from custom_autocomplete import CustomAutoCompleteWidget
from gluon.validators import Validator, ValidationError

class IS_GREATER_THAN_ZERO(Validator):
    def __init__(self, error_message=' Must Be An Integer Greater Than 0'):
        self.error_message = error_message
    
    def validate(self, value):
        try:
            if int(value) > 0:
                return value
            else:
                raise ValueError(self.error_message) 
        except Exception:
            raise ValidationError(self.error_message)
            
# Saved lambda representation of dollar amounts.
format_dollar = lambda val, row: '$%0.2f' % (val, ) if val is not None else ""

def get_file(field_ref, val, row):
    """
    A function that is passed to any upload field. This allows us to fetch the relevant files associated with the filename in the database

    We are returning a string of HTML that is essentially a link to the file location
    """
    return A(
        field_ref.retrieve(
            val, nameonly=True)
            [0], _href=URL('download', args=val, user_signature=True), _target="_blank") if val is not None else 'No File Attached'

################################# \/ Databases \/ #################################

db.define_table('department',
    Field('name', 'string'),
    format = '%(name)s'
)

db.define_table('vendor',
    Field('name', "string", label = "Vendor", requires=IS_NOT_EMPTY()),
    Field('phone_num', "string"),
    Field('account_num', "integer"),
    Field('contact_person', 'string'),
    format = '%(name)s, Account #%(account_num)s'
)

db.define_table('po_status_reference',
    Field('status', 'string', writable=False, readable=False),
    format='%(status)s'
)

db.define_table('po_form',
        Field('status', 'reference po_status_reference', default=1, writable=False, readable=True),
        Field('submitter', 'reference auth_user', default=auth.user, writable=False, readable=False,
            represent=lambda user, row: db.auth_user.format(user)
        ),
        Field('department', 
             'reference department', 
             requires=IS_IN_DB(
             db(get_department_permission_query()),
             'department.id',
             '%(name)s',
             distinct=True,
             zero="Choose a department"
             )
        ),
        Field('vendor', 'string', length=128, requires=IS_NOT_EMPTY(), 
            widget=CustomAutoCompleteWidget(request=request, field=db.vendor.name, at_beginning=False, orderby=db.vendor.name, min_length=1),
            # vendor_autocomplete_widget
            ),
        Field('phone_num', "string", length=32),
        Field('account_num', "string"),
        Field('contact_person', 'string', length=128),
        Field('order_confirmation_num', "string", length=128, readable=False, writable=False),
        Field('tracking_number','string', length=256, readable=False, writable=False),
        # TODO Put together a shipping list?
        Field('shipping_company','string', length=128, readable=False, writable=False),
        Field('project', "string", length=128),
        Field('last_modified', 'datetime', default=request.now, writable=False, readable=True),
        Field('po_approval_date', 'datetime', writable=False, readable=False),
        Field('subtotal', 'double', default=0.0, represent=format_dollar, writable=False, readable=True),
        Field('freight', 'double', label="Freight (Not Taxed)", writable=True, readable=True, represent=format_dollar, default=0.0, requires=IS_NOT_EMPTY()),
        Field('tax_percent', 'double', default=0.0, writable=True, readable=True, requires=IS_NOT_EMPTY()),
        Field('tax', 'double', default=0.0, represent=format_dollar, writable=False, readable=True),
        Field('total', 'double', default=0.0, represent=format_dollar, writable=False, readable=True),
        Field('po_entry_date', 'datetime', label="Entry Date", default=request.now, writable=False, readable=True),
        Field('on_vendor_approval_list', 'boolean'),
        Field('sole_source_purchase', 'boolean', default=False),
        format='PO # %(id)i',
    )

db.po_form.id.label = 'PO #'


db.define_table('po_note',
    Field('po_form', 'reference po_form', readable = True, writable = False, label = "PO #", notnull=True, required=True),
    Field('made_by', 'reference auth_user',
        notnull=True, default=auth.user_id, writable=False, readable=False,
        represent=lambda user_id, row: "" if user_id is None else (f'{db(db.auth_user.id == user_id).select("first_name").first().first_name} {db (db.auth_user.id == user_id).select("last_name").first().last_name} ')
         ),
    Field('made_time', 'datetime', default=request.now, notnull=True),
    Field('comment', 'text'),
    Field('po_approval', 'boolean', default=False, writable=False),
)

db.define_table('po_item',
    Field('po_form', 'reference po_form', readable = True, writable = False, label = "PO #", notnull=True, required=True),
    Field('catalog', 'string', label = "Catalog / UPC"),
    Field('description', 'string', label = "Product / Service / Description", requires=IS_NOT_EMPTY()),
    Field('quantity', 'integer', required=True, requires=IS_GREATER_THAN_ZERO()),
    Field('price_per_unit', 'double', required=True, represent=format_dollar, requires=IS_NOT_EMPTY()),
    Field('extended_price', 'double', label="Item Total", represent=format_dollar, compute=lambda r: r.quantity * r.price_per_unit),
    format='PO # %(id)i',
)

db.define_table('po_rule',
    Field('department', 'reference department', comment="Department this rule falls under, used with min_subtotal and max_subtotal to determine rule applicability"),
    Field('min_subtotal', 'double', comment="Lower limit of subtotal for PO items this ruleset evaluates against within the dept (0 == any)"),
    Field('max_subtotal', 'double', comment="Lower limit of subtotal for PO items this ruleset evaluates against within the dept (0 == any)"),
    Field('supervisor_approvals', 'integer', default=0, comment="Number of supervisor approvals, before getting any department head approvals"),
    Field('department_head_approvals', 'integer', default=0, comment="Number of department head approvals, before getting any lab operations manager approvals"),
    Field('superapprover_approvals', 'integer', default=0, comment="Number of superuser approvals"),
    Field('invoice', 'boolean', default=True, comment="Invoice is required"),
    Field('bids_recommended', 'integer', default=1, comment="Recommended number of bids (not currently enforced)"),
    Field('contract_recommended', 'boolean', comment="Contract required unless finance review is performed (outside the po system)"),
    Field('contract_required', 'boolean', comment="Contract required regardless of finance review"),
    format=lambda r: '%s PO between %0.2f and $%0.2f' % (r.department.name, r.min_subtotal, r.max_subtotal),
)

db.define_table('role_heirarchy',
    Field('name', 'string', length=128),
    Field('level', 'integer')
)

db.define_table('po_rule_map',
    Field('po_form', 'integer', required=True),
    Field('po_rule', 'reference po_rule'),
)

db.define_table('po_form_attachments',
    Field('po_form', 'reference po_form', readable=True, writable=True),
    Field('upload_date', 'datetime', default=request.now, readable=True, writable=True),
    Field('upload_type', 'string', length=128, readable=True, writable=True),
    Field('file', 'upload', length=512, readable=True, writable=True, autodelete=True)
)

db.po_form_attachments.file.represent = lambda val, row: get_file(db.po_form_attachments.file, val, row)
################################# /\ Databases /\ #################################

# We append a list of departments for the user to auth so we have access to it later instead of having to continuously call a function
auth.departments = [dept_name.name for dept_name in db(db.department).select(db.department.name, orderby=db.department.name) if auth.has_permission('submitter', dept_name.name)]

# This is the auto approver that is used for POs under the minimum threshold
db.auth_user.update_or_insert(id=0, first_name="Auto", last_name="Approver")

db.auth_user.format = lambda row: f'{row.first_name} {row.last_name}' if row.id is not None else ''
db.po_form.submitter.requires.options = lambda zero=True: [(row.id, db.auth_user.format(row)) for row in db(db.auth_user.id >= 0).select(orderby=db.auth_user.first_name)]
