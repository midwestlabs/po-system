import datetime

if configuration.get('scheduler.enabled'):
    from gluon.scheduler import Scheduler
    scheduler = Scheduler(db, heartbeat=configuration.get('scheduler.heartbeat'))

# TODO remove the preset po_number and remove the unused_param_deprecated
def review_po(po_number=0, notify_approvers=None, escalation_level=0):
    po = db(db.po_form.id == po_number).select().first()
    if not po:
        raise ValueError(f'PO #{po_number} does not exist')

    po_status = db(db.po_status_reference.id == po.status).select().first().status
    if po_status in ['Rejected',]:
        _review_rejected_po(po_number)
    if po_status in ['Submitted', 'Approved',]:
        _review_submitted_po(po_number, escalation_level)

def _review_submitted_po(po_number, escalation_level):
    rule_check = get_failing_po_rules(po_number)
    failed_rules = rule_check['failed_rules']
    approver_counts = rule_check['approver_counts']
    po = db(db.po_form.id == po_number).select().first()
    if len(failed_rules) == 0:
        po.update_record(
            status = db.po_status_reference(status='Approved'),
            last_modified=request.now,
            po_approval_date=request.now
        )
        _notify_submitter(po_number, *_generate_po_approval_notification(po_number))
        if db(db.vendor.name == po.vendor).isempty():
            _notify_accounting_new_vendor(po)
    else:
        _process_review_submitted_po(po_number, failed_rules, approver_counts, escalation_level)

def _process_review_submitted_po(po_number, rules, approver_counts, escalation_level):
    _stop_scheduled_review_po_task(po_number)

    for rule in rules:
        if escalation_level > 0:
            # This should be able to handle the dynamic approval levels per department
            # We should also codify this in such a way that it handles the dynamic roles
            if escalation_level == 1:
                rule.department_head_approvals += 1
            if escalation_level >= 2:
                rule.department_head_approvals += 1
                rule.superapprover_approvals += 1


        notify_levels = _generate_notify_levels_for_po(po_number, approver_counts, rule.id)

        if not len(notify_levels):
            continue
        _notify_approvers(po_number, notify_levels.pop(0))
        # send_approval_emails(notify_levels.pop(0), po)
        hour_delay = _get_scheduler_delay()
        for level in notify_levels:
            if len(level) == 0:
                continue
            _schedule_review(po_number, escalation_level, hour_delay)
            escalation_level += 1
            hour_delay += _get_scheduler_delay(hour_delay)

def _generate_notify_levels_for_po(po_number, approver_counts, rule_id):
    po = db(db.po_form.id == po_number).select().first()
    rule = db(db.po_rule.id == rule_id).select().first()
    submitter_set = set([po.submitter])
    notify_levels = []
    # Get all approvers with each permission level (even if in higher permissioned groups)
    # TODO This doesn't handle if there are no supervisors for the department and we need a supervisor to sign off on the PO....
    supervisors = set(getUsersByLevelForDepartment('Supervisor', po.department))
    department_heads = set(getUsersByLevelForDepartment('Department Head', po.department))
    superapprovers = set(getUsersByLevelForDepartment('superapprover', po.department))

    # Remove already approved people from notification sets

    supervisors -= approver_counts['supervisor']
    department_heads -= approver_counts['department_head']
    superapprovers -= approver_counts['superapprover']

    # Remove self from all approval sets
    supervisors -= submitter_set
    department_heads -= submitter_set
    superapprovers -= submitter_set

    unique_department_heads = approver_counts['department_head'] - (approver_counts['department_head'] & approver_counts['superapprover'])
    unique_supervisors = approver_counts['supervisor'] - (approver_counts['supervisor'] & unique_department_heads)

    if rule.supervisor_approvals > len(approver_counts['supervisor']):
        notify_levels.append(supervisors)
    elif rule.supervisor_approvals > len(unique_supervisors):
        for unique_department_head in unique_department_heads:
            unique_supervisors.add(unique_department_head)
            unique_department_heads.remove(unique_department_head)
            approver_counts['department_head'].remove(unique_department_head)
            if rule.supervisor_approvals <= len(unique_supervisors):
                break
        if rule.supervisor_approvals <= len(unique_supervisors):
            for superapprover in approver_counts['superapprover']:
                unique_supervisors.add(superapprover)
                approver_counts['superapprover'].remove(superapprover)
                if rule.supervisor_approvals <= len(unique_supervisors):
                    break

    if rule.department_head_approvals > len(approver_counts['department_head']):
        notify_levels.append(department_heads)
    elif rule.department_head_approvals > len(unique_department_heads):
        for superapprover in approver_counts['superapprover']:
            unique_department_heads.add(superapprover)
            approver_counts['superapprover'].remove(superapprover)
            if rule.department_head_approvals <= len(unique_department_heads):
                break
    if rule.superapprover_approvals > len(approver_counts['superapprover']):
        notify_levels.append(superapprovers)

    return notify_levels

def _review_rejected_po(po_number):
    rejection_subject, rejection_message = _generate_po_rejection_notification(po_number)
    _notify_submitter(po_number, rejection_subject, rejection_message)

def _generate_po_approval_notification(po_number):
    subject = f'PO #{po_number} was approved'
    message = subject

    return subject, message

def _generate_po_rejection_notification(po_number):
    subject = f'PO #{po_number} was rejected'
    message = subject

    return subject, message

def _generate_accounting_new_vendor_notification(po):
    po_submitter = db(
        (db.po_form.id == po.id) 
        & (db.auth_user.id == db.po_form.submitter)
    ).select(db.auth_user.first_name, db.auth_user.last_name).first()
    subject = f'{po_submitter.first_name} {po_submitter.last_name} has submitted a PO with a new vendor'
    message = f"""<html>
        {po_submitter.first_name} {po_submitter.last_name} has submitted PO #{A(po.id, _href=URL(c='manageUserPO', f='index', vars=dict(po_number=po.id), scheme=True, host=True))} with a new vendor: {po.vendor}"""
    return subject, message

def _generate_approver_notification(po_number):
    subject = f'Please Review PO #{po_number}'
    message = f"<html>PO Number {(A(po_number, _href=URL(c='manageUserPO', f='index', vars=dict(po_number=po_number), scheme=True, host=True)))} requires your approval, please take time to review the PO</html>"

    return subject, message

def _notify_accounting_new_vendor(po):
    for accounting_user in get_accounting_users():
        subject, message = _generate_accounting_new_vendor_notification(po)
        email_addr = db(db.auth_user.id == accounting_user.id).select().first().email
        _send_notification(subject, message, email_addr)

def _notify_submitter(po_number, subject, message):
    po_submitter = db(
        (db.po_form.id == po_number)
        & (db.po_form.submitter == db.auth_user.id)
    ).select(db.auth_user.email).first().email
    _send_notification(subject, message, po_submitter)

def _notify_approvers(po_number, approvers):
    for approver_id in approvers:
        approver = db(db.auth_user.id == approver_id).select(db.auth_user.email).first()
        if approver == None:
            print(f'Cannot find email for user_id f{approver_id}')
            continue
        else:
            approver = approver.email
        subject, message = _generate_approver_notification(po_number)
        _send_notification(subject, message, approver)

def _send_notification(subject, message, recipient):
    # This should eventually support a true notification system. As it is, we handle notifications via email
    try:
        mail.send(to=recipient, subject=subject, message=message )
    except:
        # Switch to logging
        print(f'Failed to send email. Recipient: {recipient}     Subject: {subject}     Message: {message}')

def _get_scheduler_delay(current_delay_in_days=0):
    """
    Figures out how many hours to delay notification of users (in the event that the next day is a weekend)
    current_delay_in_days is in number of days
    """
    # We should probably target 8am on weekdays
    endOfWeek = 4
    startOfWeek = 0
    trueStartOfWeek = 7
    interval = 24
    today = datetime.datetime.now().weekday()
    delay = today + current_delay_in_days
    if startOfWeek <= delay < endOfWeek:
        return interval
    return (trueStartOfWeek - delay) * interval 
        

def _schedule_review(po_number, escalation_level, delay=0):
    if scheduler:
        scheduler.queue_task(
            'review_po',
            task_name=f'po_review_{po_number}',
            pargs=[po_number, escalation_level],
            start_time=request.now + datetime.timedelta(hours=delay),
            retry_failed=3,
            group_name='po_review_reminder'
        )
    else:
        # Convert to logging
        print('Unable to schedule task as scheduler is not enabled')

def _stop_scheduled_review_po_task(po_number):
    for row in db(db.scheduler_task.task_name == f"po_review_{po_number}").select():
        scheduler.stop_task(row.id)