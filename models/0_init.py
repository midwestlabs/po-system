# -*- coding: utf-8 -*-
from gluon.contrib.appconfig import AppConfig
from gluon.tools import Mail
from gluon.storage import Storage
from gluon.tools import Auth
from gluon.contrib.login_methods.ldap_auth import ldap_auth
from gluon.custom_import import track_changes
import sys

# -------------------------------------------------------------------------
# AppConfig configuration made easy. Look inside private/appconfig.ini
# Auth is for authenticaiton and access control
# -------------------------------------------------------------------------
# -------------------------------------------------------------------------
# once in production, remove reload=True to gain full speed
# -------------------------------------------------------------------------
# configuration = AppConfig(reload=True)
configuration = AppConfig()

def setup_logging():
    import logging
    logger = logging.getLogger()
    # class CustomLoggingFilter(logging.Filter):
    #     def filter(self, record):
    #         return not 'Rocket' in record.name

    # rootlogger = logging.getLogger()
    # rootlogger.setLevel(logging.DEBUG)
    # fh = logging.FileHandler('/tmp/web2py.log')
    # formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(filename)s:%(lineno)s - %(funcName)s - PID:%(process)s - TID:%(thread)s -- %(message)s')
    # fh.setFormatter(formatter)
    # fh.addFilter(CustomLoggingFilter())
    # rootlogger.addHandler(fh)


track_changes(True)
cache.ram('root_logger_config_function_call', setup_logging, time_expire=sys.maxsize)

if request.global_settings.web2py_version < "2.15.5":
    raise HTTP(500, "Requires web2py 2.15.5 or newer")

# -------------------------------------------------------------------------
# if SSL/HTTPS is properly configured and you want all HTTP requests to
# be redirected to HTTPS, uncomment the line below:
# -------------------------------------------------------------------------
# request.requires_https()
# -------------------------------------------------------------------------
# by default give a view/generic.extension to all actions from localhost
# none otherwise. a pattern can be 'controller/function.extension'
# -------------------------------------------------------------------------
response.generic_patterns = [] 
if request.is_local and not configuration.get('app.production'):
    response.generic_patterns.append('*')

# -------------------------------------------------------------------------
# choose a style for forms
# -------------------------------------------------------------------------
response.formstyle = 'bootstrap4_inline'
response.form_label_separator = ''

# -------------------------------------------------------------------------
# (optional) optimize handling of static files
# -------------------------------------------------------------------------
# response.optimize_css = 'concat,minify,inline'
# response.optimize_js = 'concat,minify,inline'

# -------------------------------------------------------------------------
# (optional) static assets folder versioning
# -------------------------------------------------------------------------
# response.static_version = '0.0.0'

# -------------------------------------------------------------------------  
# read more at http://dev.w3.org/html5/markup/meta.name.html               
# -------------------------------------------------------------------------
response.meta.author = configuration.get('app.author')
response.meta.description = configuration.get('app.description')
response.meta.keywords = configuration.get('app.keywords')
response.meta.generator = configuration.get('app.generator')
response.show_toolbar = configuration.get('app.toolbar')

# -------------------------------------------------------------------------
# your http://google.com/analytics id                                      
# -------------------------------------------------------------------------
# response.google_analytics_id = configuration.get('google.analytics_id')
# 

dal_vars = dict(
    uri=configuration.get('db.uri'),
    check_reserved=['postgres'],
)
if configuration.get('db.migrate'):
    dal_vars['migrate'] = configuration.get('db.migrate')

if configuration.get('db.fake_migrate'):
    dal_vars['fake_migrate'] = configuration.get('db.fake_migrate')

db = DAL(**dal_vars)
# # Setup for postgres
# if configuration.get('app.testing'):
#     db = DAL('sqlite://testing.sqlite')
# elif (configuration.get('app.production')):
# # Prod Server
#     db = DAL("postgres://purchase_order_system:epkDh73^6*kvdnQGx$&T@10.10.0.213:5432/purchase_order_system", check_reserved=['postgres'])
# # Its likely worth trying to create a copy script that will copy from our staging backup, to an SQLite server so we can manipulate "current"
# # data
# else:
# # Test Server
#     db = DAL("postgres://purchase_order_system:G3uo&3X*EHrF$6dL86fP3uN@localhost:5432/purchase_order_system", check_reserved=['postgres'])
session.connect(request, response, db)

####################\/ Mail stuff \/####################

mail = Mail()
mail.settings.server = "mwlapp01.mwlad.midwestlabs.com:25"
#mail.settings.server = "logging"
mail.settings.tls = False
mail.settings.sender = "po-delivery@midwestlabs.com"

####################/\ Mail stuff /\#####################


####################\/ Authentication \/#################

auth = Auth(db, host_names=configuration.get('host.names'))
# -------------------------------------------------------------------------
# create all tables needed by auth, maybe add a list of extra fields
# -------------------------------------------------------------------------

# -------------------------------------------------------------------------
# configure auth policy
# -------------------------------------------------------------------------
auth.settings.client_side = True
auth.settings.actions_disabled = ['register', 'change_password', 'request_reset_password',
                                  'retrieve_password', 'retrieve_username', 'profile']
# you don't have to remember me
auth.settings.remember_me_form = False
auth.settings.create_user_groups = None
# ldap authentication and not save password on web2py
if not configuration.get('app.testing'):
    auth.settings.login_methods = [
        ldap_auth(
            mode='ad',
            db=db,
            server='mwlad.midwestlabs.com',
            base_dn='dc=mwlad,dc=midwestlabs,dc=com',
            bind_dn='mps', bind_pw='m*vng^yt',
            manage_user=True,
            user_firstname_attrib='cn:1',
            user_lastname_attrib='cn:2',
            user_mail_attrib='mail',
        ),
    ]

def on_login_fail(form):
    for k in ('username', 'email', '_formkey', '_formname', auth.settings.password_field):
        if k in request.post_vars:
            del request.post_vars[k]
    redirect(URL(args=request.args, vars=request.post_vars), client_side=True)

def on_login_success(form):
    if form.vars.username and '@' in form.vars.username:
        form.errors.username = 'Please enter your purple page username, not your email address'
        return

    if 'test' in form.vars.username and configuration.get('app.production'):
        session.flash = 'Invalid login'
        on_login_fail(form)

auth.settings.login_onfail = [on_login_fail]

auth.settings.login_onvalidation = [on_login_success]

auth.settings.extra_fields['auth_group'] = [
    Field('role_level', 'integer', writable=False, readable=False),
]

auth.define_tables(username=True, signature=True)

####################/\ Authentication /\#################

####################\/ Menu \/#################

response.menu = []
if auth.is_logged_in():
    # TODO migrate to role instead of permission
    if auth.has_permission("submitter", "any"):
        response.menu.append((T('Create New PO Form'), False, URL(c="createPO", f="index", user_signature=True), []))

    response.menu.append((T('View Your PO Forms'), False, URL(c="viewPOForms", f='index', user_signature=True, vars=dict(filter="user"))))

    if auth.has_permission("supervisor", "any") or auth.has_permission('accounting', 'any'):
        if auth.has_permission('accounting', 'any'):
            response.menu.append((T('View Actionable POs'), False, URL(c='viewPOForms', f='index', user_signature=True, vars=dict(filter="accounting", keywords='po_form.status="6"'))))
        elif auth.has_permission('supervisor', 'any'):
            response.menu.append((T('View Actionable POs'), False, URL(c='viewPOForms', f='index', user_signature=True, vars=dict(filter="supervisor"))))
        response.menu.append((T('View All POs'), False, URL(c="viewPOForms", f='index', user_signature=True, vars=dict(filter="all"))))
        if auth.has_permission('supervisor', 'any'):
            response.menu.append((T('User Admin'), False, URL(c='userAdmin', f='index', user_signature=True)))
    if auth.has_permission('impersonate', "auth_user", auth.user.id):
        response.menu.append((
            T('Impersonate User'), False, '#', [
                (
                    f'{user.first_name} {user.last_name}', 
                    False, 
                    URL(c='default', f='user/impersonate', args=[user.id])
                ) 
                for user in db(db.auth_user.id >= 1).select(db.auth_user.id, db.auth_user.first_name,
                                                                db.auth_user.last_name, orderby=db.auth_user.first_name)
                if user.first_name != None and user.first_name != '' and not user.first_name.isspace()
            ]))
    if auth.is_impersonating():
        response.menu.append((
            T('Remove Impersonation'), False, URL(c='default', f='user/impersonate', args=[0])))
    if not db(db.auth_membership.user_id == auth.user.id).isempty():
        response.menu.append((
            SPAN(
                FORM(
                    INPUT(_placeholder='PO Lookup', _name='po_number', _type='number', _class='no-spinners'),
                    INPUT(_type='submit', _style='position: absolute; left: -100px; width: 1px; height: 1px;', _class='nomargins'),
                        _action=URL(c="manageUserPO", f="index", user_signature=True),
                        _style="margin-top: 5px; margin-left: 10px",
                        _method="get")
            )
        ))

####################/\ Menu /\#################

####################\/ PO Rule Checking \/ ##############

def get_failing_po_rules(po_num):
    approver_counts = getApproversByPO(po_num)
    # TODO: Get default rule matching working

    has_contract = db((db.po_form_attachments.po_form == po_num) & (db.po_form_attachments.upload_type == 'contract')).isempty()
    failed_rules = (
        # Joining Tables
        ((db.po_form.id == po_num) &
        (db.po_rule_map.po_form == db.po_form.id) &
        (db.po_rule.id == db.po_rule_map.po_rule)) & 
        # Contract Check
        (((db.po_rule.contract_required == True) & has_contract) |
        # Approval Check
            (
                ((db.po_rule.supervisor_approvals + db.po_rule.department_head_approvals + db.po_rule.superapprover_approvals) > len(approver_counts['supervisor'])) |
                ((db.po_rule.department_head_approvals + db.po_rule.superapprover_approvals) > len(approver_counts['department_head'])) | 
                ((db.po_rule.superapprover_approvals) > len(approver_counts['superapprover'])) 
    )
        )
    )

    # Returns a list of rules that this failed, so evaluates to false if didn't fail any (empty list)
    return dict(failed_rules=db(failed_rules).select(db.po_rule.ALL), approver_counts=approver_counts)

def getUsersByLevelForDepartment(role, departmentId):

    department = db(db.department.id == departmentId).select().first().name
    if role not in ['superapprover',]:
        role = f'{department} {role}'

    query = (
        (db.auth_user.id == db.auth_membership.user_id) & 
        (db.auth_membership.group_id == db.auth_group.id) &
        (db.auth_group.id == db.auth_permission.group_id) &
        (db.auth_group.role != "superuser") &
        (db.auth_group.role != 'accounting') &
        (db.auth_group.role == role) &
        (db.department.name == db.auth_permission.table_name) &
        (db.department.name == department)
    )

    return [row.id for row in db(query).select(db.auth_user.id)]

def getApproversByPO(poNumber):
    approver_levels = [
        r.name 
        for r in db(
            (db.role_heirarchy.name != 'superuser') & (db.role_heirarchy.name != 'accounting') & (db.role_heirarchy.level != 0)).select(db.role_heirarchy.name, orderby=~db.role_heirarchy.level)
        ]
    approver_counts = dict([(level, set()) for level in approver_levels])
    # Gets a list of all the approvers that have already approved this po.
    approval_po_notes = db((db.po_note.po_form == poNumber) & (db.po_note.po_approval == True)).select(db.po_note.made_by)
    po_form_row = db.po_form(poNumber)
    submitter = int(po_form_row.submitter)
    department = db(db.department.id == po_form_row.department).select(db.department.name).first().name
    
    for approver in [approver.made_by for approver in approval_po_notes]:
        # I don't care if you've somehow approved your own stuff
        if approver == submitter:
            continue
        for permission in approver_levels:
            if permission == 'superapprover':
                table = 'any'
            else:
                table = department
            if auth.has_permission(permission, table, user_id=approver):
                approver_counts[permission].add(approver)

    return approver_counts

####################/\ PO Rule Checking /\ ##############

####################\/ Misc Functions \/#################

def get_department_permission_query(permission=None, user_id=auth.user_id):
    '''
    Takes a permission to search for (submitter, approver, etc.) and a user ID
    (defaulting to current user)
    Joins the user to _any_ permission in departments, if the "permission" arg
    is passed as None (the default).
    Returns a query set/segment that can be included to join that user up to the
    db.department table for departments they have the given permission in.
    '''
    query = (db.department.name == db.auth_permission.table_name)
    if permission is not None:
        query &= (db.auth_permission.name == permission)
    query &= (db.auth_permission.is_active == True)
    query &= (db.auth_permission.group_id == db.auth_membership.group_id)
    query &= (db.auth_membership.is_active == True)
    query &= (db.auth_membership.user_id == user_id)
    return query

# This should probably be moved to a controller but its fine for now
def add_department(department):
    # Deprecated
    # How to add a department
    # add department to department table
    # copy ruleset of another department, updating it to be our department.
    # update the rules to match the required rules for the new department
    # create submitter role for department
    # create department head role for department
    # copy department head permission to superapprover
    # copy department head permission to superuser

    db.department.update_or_insert(name=department)
    groupId = auth.add_group(department + " Submitter", "Submitter Group For " + department)
    auth.add_permission(groupId, "submitter", department)
    auth.add_permission(groupId, "submitter", "any")
    groupId = auth.add_group(department + " Supervisor", "Supervisor Group For " + department)
    auth.add_permission(groupId, "supervisor", department)
    auth.add_permission(groupId, "supervisor", 'any')
    copyPermissions(department + " Submitter", department + " Supervisor", "supervisor", department)
    groupId = auth.add_group(department + " Department Head", "Department Head Group For " + department)
    auth.add_permission(groupId, "department_head", department)
    auth.add_permission(groupId, "department_head", 'any')
    copyPermissions(department + ' Supervisor', department + ' Department Head', 'department_head', department)
    copyPermissions(department + ' Department Head', 'superapprover', 'superapprover', department)
    copyPermissions(department + ' Department Head', 'superuser')

    departmentId = db(db.department.name == department).select("id").first().id
    db.po_rule.update_or_insert(department=departmentId, min_subtotal=0, max_subtotal=100, supervisor_approvals=0, department_head_approvals=0, superapprover_approvals=0, invoice=1, bids_recommended=0, contract_recommended=0, contract_required=0)
    db.po_rule.update_or_insert(department=departmentId, min_subtotal=100, max_subtotal=5000, supervisor_approvals=1, department_head_approvals=0, superapprover_approvals=0, invoice=1, bids_recommended=0, contract_recommended=0, contract_required=0)
    db.po_rule.update_or_insert(department=departmentId, min_subtotal=5000, max_subtotal=50000, supervisor_approvals=1, department_head_approvals=1, superapprover_approvals=0, invoice=1, bids_recommended=3, contract_recommended=1, contract_required=0)
    db.po_rule.update_or_insert(department=departmentId, min_subtotal=50000, max_subtotal=0, supervisor_approvals=1, department_head_approvals=1, superapprover_approvals=1, invoice=1, bids_recommended=3, contract_recommended=0, contract_required=1)

def copyPermissions(givingGroupName, receivingGroupName, new_permission=None, department=None):
    target_group_id = auth.id_group(receivingGroupName)
    for permission in db(db.auth_permission.group_id == auth.id_group(givingGroupName)).select("name", "table_name"):
        auth.add_permission(target_group_id, name=permission.name, table_name=permission.table_name)
    if new_permission is not None and department is not None:
        auth.add_permission(target_group_id, new_permission, department)
        auth.add_permission(target_group_id, new_permission, 'any')

def get_accounting_users():
    iau = db.auth_user.with_alias('iau')
    iam = db.auth_membership.with_alias('iam')
    iag = db.auth_group.with_alias('iag')

    inner_query = (iag.role == 'superuser') 
    inner_query &= (iam.group_id == iag.id)
    inner_query &= ((iau.id == iam.user_id) & (iau.is_active == True))

    query = (
        (db.auth_group.role == 'accounting') &
        (db.auth_membership.group_id == db.auth_group.id) &
        ((db.auth_membership.user_id == db.auth_user.id) & (db.auth_user.is_active == True)) &
        (~db.auth_user.id.belongs(db(inner_query).nested_select(iau.id)))
    )
    return [user for user in db(query).select(db.auth_user.id)]

if auth.user and (
    (not configuration.get('app.production') or configuration.get('testing'))
):
    if auth.id_group('impersonate') is None:
        gid = auth.add_group('impersonate', 'Impersonation user')
        auth.add_permission(gid, 'impersonate', db.auth_user)
    gid = auth.id_group('impersonate')
    auth.add_membership(gid, auth.user_id)