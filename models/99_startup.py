# Generic "startup" script. Pending implementation and documentation

# if auth.user is not None and db(db.auth_user.id > 0).count() == 1:
#     if db(db.auth_group.role == "superuser").isempty():
#         groupId = auth.add_group("superuser")
#         auth.add_permission(groupId, "superuser", 'any')
#     groupId = auth.id_group(role="superuser")
#     if not db((db.auth_membership.user_id == auth.user.id) & (db.auth_membership.group_id == groupId)).select().first():
#         auth.add_membership(groupId, auth.user_id)

# Updates for Sprint 05-11-2020
# \/
# if db(db.auth_group.role == 'packing_slip_uploader').isempty():
#     id = auth.add_group('packing_slip_uploader')
#     db(db.auth_group.id == id).select().first().update_record(role_level=0)
#     if db(db.role_heirarchy.name == 'packing_slip_uploader').isempty():
#         db.role_heirarchy.insert(name='packing_slip_uploader', level=0)

# if db(db.auth_permission.name == 'upload_packing_slip_ordered').isempty():
#     group_id = db(db.auth_group.role == 'packing_slip_uploader').select().first().id
#     auth.add_permission(group_id, 'upload_packing_slip_ordered', 'any')

# if not db((db.auth_group.role.contains('Submitter')) & (db.auth_group.role_level == 1)).isempty():
#     db((db.auth_group.role.contains('Submitter')) & (db.auth_group.role_level == 1)).update(role_level = 0)

# if not db((db.auth_group.role.contains('Supervisor')) & (db.auth_group.role_level == 2)).isempty():
#     db((db.auth_group.role.contains('Supervisor')) & (db.auth_group.role_level == 2)).update(role_level = 1)

# if not db((db.auth_group.role.contains('Department Head')) & (db.auth_group.role_level == 3)).isempty():
#     db((db.auth_group.role.contains('Department Head')) & (db.auth_group.role_level == 3)).update(role_level = 2)
# if(not db((db.auth_group.role == 'accounting') & (db.auth_group.role_level != 3)).isempty()):
#     db(db.auth_group.role == 'accounting').select().first().update_record(role_level=3)
# /\

if auth.user and not auth.user.email:
    auth.user.email = f'{auth.user.username}@midwestlabs.com'
    db(db.auth_user.id == auth.user.id).update(email=auth.user.email)
    db.commit()

def migrate_attachments():
    try:
        db.define_table('po_form_attachments',
            Field('po_form', 'reference po_form', readable=True, writable=True),
            Field('upload_date', 'datetime', default=request.now, readable=True, writable=True),
            Field('upload_type', 'string', length=128, readable=True, writable=True),
            Field('file', 'upload', length=512, readable=True, writable=True, autodelete=True)
        )
    except:
        pass

    for row in db(db.po_form.id >= 0).select():
    # Population section
        if row.quote1:
            filename, stream = db.po_form.quote1.retrieve(row.quote1)
            db.po_form_attachments.insert(
                po_form=row.id, 
                upload_date=row.last_modified, 
                upload_type='quote', 
                file=db.po_form_attachments.file.store(stream, filename)
            )
            stream.close()
        if row.quote2:
            filename, stream = db.po_form.quote2.retrieve(row.quote2)
            db.po_form_attachments.insert(
                po_form=row.id, 
                upload_date=row.last_modified, 
                upload_type='quote', 
                file=db.po_form_attachments.file.store(stream, filename)
            )
            stream.close()
        if row.quote3:
            filename, stream = db.po_form.quote3.retrieve(row.quote3)
            db.po_form_attachments.insert(
                po_form=row.id, 
                upload_date=row.last_modified, 
                upload_type='quote', 
                file=db.po_form_attachments.file.store(stream, filename)
            )
            stream.close()
        if row.contract:
            filename, stream = db.po_form.contract.retrieve(row.contract)
            db.po_form_attachments.insert(
                po_form=row.id, 
                upload_date=row.last_modified, 
                upload_type='contract', 
                file=db.po_form_attachments.file.store(stream, filename)
            )
            stream.close()
        if row.packing_slip:
            filename, stream = db.po_form.packing_slip.retrieve(row.packing_slip)
            db.po_form_attachments.insert(
                po_form=row.id, 
                upload_date=row.last_modified, 
                upload_type='packing_slip',
                file=db.po_form_attachments.file.store(stream, filename)
            )
            stream.close()
        if row.invoice:
            filename, stream = db.po_form.invoice.retrieve(row.invoice)
            db.po_form_attachments.insert(
                po_form=row.id,
                upload_date=row.last_modified,
                upload_type='invoice',
                file=db.po_form_attachments.file.store(stream, filename)
            )
            stream.close()
        db.po_form[row.id] = dict(
            quote1=None,
            quote2=None,
            quote3=None,
            contract=None,
            invoice=None,
            packing_slip=None
        )
def create_upload_permissions():
    upload_types = ["quote", "contract", "packing_slip", "invoice", "receipt"] 
    departments = [row.name for row in db(db.department.id >= 0).select()]
    states = [row.status for row in db((db.po_status_reference.id >= 0) & (db.po_status_reference.status != 'Closed')).select()]
    superapprover_group_id = db(db.auth_group.role == 'superapprover').select().first().id
    superuser_group_id = db(db.auth_group.role == 'superuser').select().first().id
    accounting_group_id = db(db.auth_group.role == 'accounting').select().first().id
    packing_slip_uploader_id = db(db.auth_group.role == 'packing_slip_uploader').select().first().id

    for type in upload_types:
        for state in states:
            permission = f"upload_{type}_{state.replace(' ', '_').lower()}"
            auth.add_permission(superapprover_group_id, permission, 'any')
            auth.add_permission(superuser_group_id, permission, 'any')
            auth.add_permission(accounting_group_id, permission, 'any')
            if type in('packing_slip', 'invoice'):
                if type == 'packing_slip':
                    auth.add_permission(packing_slip_uploader_id, permission, 'any')

                for department in departments:
                    submitter_group_id = db(db.auth_group.role == f'{department} Submitter').select().first().id
                    supervisor_group_id = db(db.auth_group.role == f'{department} Supervisor').select().first().id
                    department_head_group_id = db(db.auth_group.role == f'{department} Department Head').select().first().id
                    _d = department.replace(' ', '_').lower()
                    auth.add_permission(submitter_group_id, permission, _d)
                    auth.add_permission(supervisor_group_id, permission, _d)
                    auth.add_permission(department_head_group_id, permission, _d)

# create_upload_permissions()
# migrate_attachments()

# for po in db(db.po_form.id >= 0).select():
# if auth.user is not None and auth.user.username == 'mli6' and not auth.has_permission('impersonate', db.auth_user, auth.user.id):
#     auth.add_permission('impersonate', db.auth_user)

# This should correct the issue with all null groups
#db(db.auth_permission.group_id == None).delete()
# This should delete all extra permissions for accounting
#db((db.auth_permission.id == 311) & ((db.auth_permission.name != 'accounting') & (db.auth_permission.table_name != 'any'))).delete()
# Run this once in production to correct all the issues with superuser having all roles
#db((db.auth_membership.user_id.belongs([4, 7, 8, 9, 6])) & (db.auth_membership.group_id != 309)).delete()

# Its not pretty and not sql, but this does the proper mapping on a per group basis. This should be moved
# to the nukeOrPopulate script to be run on creating departments probably
# missing_roles = db(db.auth_group.role_level == None).select(db.auth_group.id, db.auth_group.role, db.auth_group.role_level)
# if missing_roles and len(missing_roles) > 0:
#     for missing_role in missing_roles:
#         for role in db(db.role_heirarchy).select(db.role_heirarchy.id, db.role_heirarchy.name):
#             if role.name.lower() in missing_role.role.lower().replace(' ','_'):
#                 db(db.auth_group.id == missing_role.id).update(role_level=role.id)

# We need to correct the accounting permissions????
