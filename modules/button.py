from gluon.html import STYLE, SPAN, INPUT

def BUTTON(text, icon=None, style=STYLE("margin: 0px; padding: 3px;"), *args, **kwargs):
    """
    A Helper method that creates HTML "BUTTON" objects

    :usage BUTTON(text, icon (optional), style (optional)):

    :requires None:

    :params text: The string you want to appear on the button
    :params icon (optional): The image icon you want to appear on the button. NOTE, currently this is unused
    :params style (optional): The css style you want the button to have

    :returns string: Returns an HTML compatible string form of the button and its objects
    """
    # icon isnt used currently, but if we want to be able to put an icon on the button, we should be able to supply that
    if kwargs.get('_class'):
        kwargs['_class'] = ('btn btn-secondary ' + kwargs['_class'])
    else:
        kwargs['_class'] = 'btn btn-secondary'
    if not kwargs.get('_value'):
        kwargs['_value'] = text
    # return SPAN(Probably going to put the icon here, INPUT(_type="submit", *args, **kwargs), _style=style)
    return SPAN(INPUT(_type="submit", *args, **kwargs), _style=style)