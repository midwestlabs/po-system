from gluon import CAT, SPAN, DIV, INPUT, SELECT, TEXTAREA, P, LABEL, UL

def custom_bootstrap4_inline(col_label_size=3, submit_button_text='Submit'):
    '''
    All we are doing different is overriding the "submit" button text via CSS

    For more details, reference gluon.sqlhtml.formstyle_boostrap4_inline_factory
    '''
    add_class = lambda a, b: a + ' ' + b if a else b
    def _inner(form, fields):
        label_col_class = "col-sm-%d" % col_label_size
        col_class = "col-sm-%d" % (12 - col_label_size)
        offset_class = "col-sm-offset-%d" % col_label_size
        parent = CAT()
        for id, label, controls, help in fields:
            # wrappers
            _help = SPAN(help, _class='help-block')
            # embed _help into _controls
            _controls = DIV(controls, _help, _class="%s" % (col_class))
            if isinstance(controls, INPUT):
                if controls['_type'] == 'submit':
                    controls['_value'] = submit_button_text
                    controls.add_class('btn btn-primary')
                    _controls = DIV(controls, _class="%s %s" % (col_class, offset_class))
                if controls['_type'] == 'button':
                    controls.add_class('btn btn-secondary')
                elif controls['_type'] == 'file':
                    controls.add_class('input-file')
                elif controls['_type'] in ('text', 'password'):
                    controls.add_class('form-control')
                elif controls['_type'] == 'checkbox' or controls['_type'] == 'radio':
                    controls.add_class('form-check-input')
                    label.add_class('form-check-label')
                    label.insert(0, controls)
                    #label.insert(0, ' ')
                    _controls = DIV(DIV(label, _help, _class="form-check"), _class="%s" % col_class)
                    label = DIV(_class="sm-hidden %s" % label_col_class)
                elif isinstance(controls, (SELECT, TEXTAREA)):
                    controls.add_class('form-control')

            elif isinstance(controls, SPAN):
                _controls = P(controls.components,
                              _class="form-control-plaintext %s" % col_class)
            elif isinstance(controls, UL):
                for e in controls.elements("input"):
                    e.add_class('form-control')
            elif isinstance(controls, CAT) and isinstance(controls[0], INPUT):
                    controls[0].add_class('form-control')
            if isinstance(label, LABEL):
                label['_class'] = add_class(label.get('_class'), 'form-control-label %s' % label_col_class)

            parent.append(DIV(label, _controls, _class='form-group row', _id=id))
        return parent
    return _inner
