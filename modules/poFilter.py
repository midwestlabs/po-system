
# I hate this, but it should work
def UserLevelFilter(db, userId):
    fields = [
            db.po_form.id,
            db.po_form.department,
            db.po_form.po_entry_date,
            db.po_form.vendor,
            db.po_form.total,
            db.po_form.status,
        ]

    query =  (db.po_form.submitter == userId)
    query &= (db.po_form.status != (db(db.po_status_reference.status == "Closed").select().first().id))


    return query, fields

def SupervisorLevelFilter(db, userId):
    db.po_form.submitter.readable = True
    db.po_form.last_modified.readable = True

    fields = [
        db.po_form.id,
        db.po_form.department,
        db.po_form.submitter,
        db.po_form.vendor,
        db.po_form.po_entry_date,
        db.po_form.total,
    ]
    

    inner_query = (
        (db.po_form.department == db.department.id)
        & ((db.po_status_reference.id == db.po_form.status) & (db.po_status_reference.status == 'Submitted'))
        & (db.po_rule_map.po_form == db.po_form.id)
        & (db.po_rule_map.po_rule == db.po_rule.id)
        & (
                ((db.role_heirarchy.name == 'supervisor') & (db.po_rule.supervisor_approvals > 0)) 
                | ((db.role_heirarchy.name == 'department_head') & (db.po_rule.department_head_approvals > 0)) 
                | ((db.role_heirarchy.name == 'superapprover') & (db.po_rule.superapprover_approvals > 0))
        )
        & ((db.auth_group.role.lower().startswith(db.department.name.lower())) | (db.auth_group.role == 'superapprover'))
        & (db.auth_membership.group_id == db.auth_group.id)
        & (db.role_heirarchy.level == db.auth_group.role_level)
        & (db.auth_membership.user_id == db.auth_user.id)
        & (db.auth_user.id == userId)
        & (db.po_form.submitter != userId)
    )

    notes_query_1 = (
        (db.po_note.po_form == db.po_form.id) &
        (db.po_note.made_by == db.auth_user.id) &
        (db.auth_user.id == userId)
    )

    notes_query_2 = (
        (db.po_note.po_form == db.po_form.id) &
        (db.po_note.made_by == db.auth_user.id) &
        (db.auth_user.id == userId) &
        (db.po_note.po_approval == 'T')
    )

    query = (
        (db.po_form.id.belongs(db(inner_query).nested_select(db.po_form.id))) 
        & (
            (~db.po_form.id.belongs(db(notes_query_2).nested_select(db.po_form.id))) |
            (~db.po_form.id.belongs(db(notes_query_1).nested_select(db.po_form.id)))
        )
    )


    return query, fields

def CustomFilter(db):
    # I am not exactly sure how we can do this. Might be worth checking out parsing a dictionary (JSON) to a query, but that isn't the best idea.
    pass

# Figure out if we need this to be all POs in your departments or all POs that you can act on
def MaxLevelFilter(db, userId):

    db.po_form.submitter.readable = True
    db.po_form.last_modified.readable = True

    # All PO's the user _can_ see, with the exception of closed POs
    fields = [
        db.po_form.id,
        db.po_form.department,
        db.po_form.submitter,
        db.po_form.vendor,
        db.po_form.po_entry_date,
        db.po_form.last_modified,
        db.po_form.status,
        db.po_form.total,
    ]

    query = (db.department.name == db.auth_permission.table_name)
    query &= (db.auth_permission.is_active == True)
    query &= (db.auth_permission.group_id == db.auth_membership.group_id)
    query &= (db.auth_membership.is_active == True)
    query &= (db.auth_membership.user_id == userId)
    query &= (db.po_form.department == db.department.id)
    query &= (db.po_status_reference.id == db.po_form.status) & (db.po_status_reference.status != "Closed")

    query = (db.po_form.id.belongs(db(query).nested_select(db.po_form.id)))
    return query, fields

def MaxLevelFilter_WithClosedPOs(db, userId):

    db.po_form.submitter.readable = True
    db.po_form.last_modified.readable = True

    # All PO's the user _can_ see, with the exception of closed POs
    fields = [
        db.po_form.id,
        db.po_form.department,
        db.po_form.submitter,
        db.po_form.vendor,
        db.po_form.po_entry_date,
        db.po_form.last_modified,
        db.po_form.status,
        db.po_form.total,
    ]

    query = (
        ((db.auth_user.id == userId) & (db.auth_user.is_active == True)) &
        (db.po_form.status == db.po_status_reference.id)
    )

    return query, fields

def MaxLevelFilter_2(db, userId):
    # Deprecated?
    #Currently unused. If the current MaxLevelFilter displays too much information, you can use this one instead. 
    #Its differences are, it doesn't include the user's POs in the "All" POs.
    db.po_form.submitter.readable = True
    db.po_form.last_modified.readable = True
    
    # All PO's the user _can_ see, with the exception of closed POs and the user's own POs
    fields = [
        db.po_form.id,
        db.po_form.department,
        db.po_form.submitter,
        db.po_form.po_entry_date,
        db.po_form.last_modified,
        db.po_form.status,
        db.po_form.total,
    ]
    
    departmentQuery = (db.department.name == db.auth_permission.table_name)
    departmentQuery &= (db.auth_permission.is_active == True)
    departmentQuery &= (db.auth_permission.group_id == db.auth_membership.group_id)
    departmentQuery &= (db.auth_membership.is_active == True)
    departmentQuery &= (db.auth_membership.user_id == userId)
    departmentQuery = db(departmentQuery)._select(db.department.id)

    query = (db.po_form.submitter != userId)
    query &= (db.auth_membership.user_id == userId) & (db.auth_membership.is_active == True)
    query &= (db.po_status_reference.id == db.po_form.status) & (db.po_status_reference.status != "Closed")
    query &= (db.po_form.department.belongs(departmentQuery))
    query = (db.po_form.id.belongs(db(query).nested_select(db.po_form.id)))

    return query, fields

def AccountingFilter(db, userId):
    
    db.po_form.submitter.readable = True
    db.po_form.status.readable = True
    db.po_form.last_modified.readable = True
    fields = [
        db.po_form.id,
        db.po_form.department,
        db.po_form.submitter,
        db.po_form.vendor,
        db.po_form.po_entry_date,
        db.po_form.last_modified,
        db.po_form.status,
        db.po_form.total,
    ]

    query = (
        ((db.auth_user.id == userId) & (db.auth_user.is_active == True)) &
        (db.po_form.status == db.po_status_reference.id) &
        (
            (db.po_status_reference.status == 'Ordered') | 
            (db.po_status_reference.status == 'Received') | 
            (db.po_status_reference.status == 'Closed')
        ) 
    )

    return query, fields