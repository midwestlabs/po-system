# Basically ripped from sqlhtml.AutocompleteWidget
import datetime
import re
import copy

import os
from gluon._compat import StringIO,unichr, urllib_quote, iteritems, basestring, long, integer_types, unicodeT, to_native, to_unicode, urlencode
from gluon.http import HTTP, redirect
from gluon.html import XmlComponent, truncate_string
from gluon.html import XML, SPAN, TAG, A, DIV, CAT, UL, LI, TEXTAREA, BR, IMG
from gluon.html import FORM, INPUT, LABEL, OPTION, SELECT, COL, COLGROUP
from gluon.html import TABLE, THEAD, TBODY, TR, TD, TH, STYLE, SCRIPT
from gluon.html import URL, FIELDSET, P, DEFAULT_PASSWORD_DISPLAY
from pydal.base import DEFAULT
from pydal.objects import Table, Row, Expression, Field, Set, Rows
from pydal.adapters.base import CALLABLETYPES
from pydal.helpers.methods import smart_query, bar_encode, _repr_ref, merge_tablemaps
from pydal.helpers.classes import Reference, SQLCustomType
from pydal.default_validators import default_validators
from gluon.storage import Storage
from gluon.utils import md5_hash
from gluon.validators import IS_EMPTY_OR, IS_NOT_EMPTY, IS_LIST_OF, IS_DATE
from gluon.validators import IS_DATETIME, IS_INT_IN_RANGE, IS_FLOAT_IN_RANGE
from gluon.validators import IS_STRONG
from gluon.sqlhtml import StringWidget

import gluon.serializers as serializers
from gluon.globals import current
from functools import reduce

try:
    import gluon.settings as settings
except ImportError:
    settings = {}

class CustomAutoCompleteWidget:
    # To utilize this (vs just the generic sqlhtml.AutocompleteWidget), you should set the 
    # 'oncomplete' attribute for whatever input box utilizes this widget. It is recommended
    # that you set the 'oncomplete' attribute to be a function call of some kind as you
    # can do logic after the autocomplete has been finished.
    # The autocomplete widget will call the 'oncomplete' after it has finished filling in
    # the input box with whatever value it is autocompleting.
    # 
    # Example Usage is as follows
    '''
    //getting the input object
    let autocompleteWidget = document.getElementById('whateverYourAutocompleteWidgetIdIs');
    autocompleteWidget.oncomplete = 'logicAfterAutoComplete(autocompleteWidget.innerHTML)';
    //or if you prefer jquery
    $('#whateverYourAutocompleteWidgetIdIs').attr('oncomplete', 'logicAfterAutoComplete($("#'#whateverYourAutocompleteWidgetIdIs'").val())');

    function logicAfterAutoComplete(autocompletedValue){
        // do your logic here
    }

    For more details, reference gluon.sqlhtml.AutocompleteWidget
    '''

    _class = 'string'

    def __init__(self, 
            request, 
            field, 
            id_field=None, 
            db=None,
            orderby=None, 
            limitby=(0, 10), 
            distinct=False,
            keyword='_autocomplete_%(tablename)s_%(fieldname)s',
            min_length=2,
            help_fields=None, 
            help_string=None,
            at_beginning=True, 
            default_var='ac', 
            user_signature=True,
            hash_vars=False):

        self.help_fields = help_fields or []
        self.help_string = help_string
        if self.help_fields and not self.help_string:
            self.help_string = ' '.join('%%(%s)s' % f.name for f in self.help_fields)

        self.request = request
        self.keyword = keyword % dict(tablename=field.tablename,
                                      fieldname=field.name)
        self.db = db or field._db
        self.orderby = orderby
        self.limitby = limitby
        self.distinct = distinct
        self.min_length = min_length
        self.at_beginning = at_beginning
        self.fields = [field]
        if id_field:
            self.is_reference = True
            self.fields.append(id_field)
        else:
            self.is_reference = False
        if hasattr(request, 'application'):
            urlvars = copy.copy(request.vars)
            urlvars[default_var] = 1
            self.url = URL(args=request.args, vars=urlvars,
                           user_signature=user_signature, hash_vars=hash_vars)
            self.run_callback = True
        else:
            self.url = request
            self.run_callback = False

    def callback(self):
        if self.keyword in self.request.vars:
            field = self.fields[0]
            kword = self.request.vars[self.keyword]
            if isinstance(field, Field.Virtual):
                records = []
                table_rows = self.db(self.db[field.tablename]).select(orderby=self.orderby)
                count = self.limitby[1] if self.limitby else -1
                for row in table_rows:
                    if self.at_beginning:
                        if row[field.name].lower().startswith(kword):
                            count -= 1
                            records.append(row)
                    else:
                        if kword in row[field.name].lower():
                            count -= 1
                            records.append(row)
                    if count == 0:
                        break
                if self.limitby and self.limitby[0]:
                    records = records[self.limitby[0]:]
                rows = Rows(self.db, records, table_rows.colnames,
                            compact=table_rows.compact)
            elif settings and settings.global_settings.web2py_runtime_gae:
                rows = self.db(field.__ge__(kword) &
                               field.__lt__(kword + '\ufffd')
                               ).select(orderby=self.orderby,
                                        limitby=self.limitby,
                                        *(self.fields + self.help_fields))
            elif self.at_beginning:
                rows = self.db(field.like(kword + '%', case_sensitive=False)
                               ).select(orderby=self.orderby,
                                        limitby=self.limitby,
                                        distinct=self.distinct,
                                        *(self.fields + self.help_fields))
            else:
                rows = self.db(field.contains(kword, case_sensitive=False)
                               ).select(orderby=self.orderby,
                                        limitby=self.limitby,
                                        distinct=self.distinct,
                                        *(self.fields + self.help_fields))
            if rows:
                if self.is_reference:
                    id_field = self.fields[1]
                    if self.help_fields:
                        options = [
                            OPTION(
                                self.help_string % dict(
                                    [(h.name, s[h.name]) for h
                                     in self.fields[:1] + self.help_fields]),
                                                    _value=s[id_field.name],
                                                    _selected=(k == 0))
                                            for k, s in enumerate(rows)
                                    ]
                    else:
                        options = [OPTION(
                            s[field.name], _value=s[id_field.name],
                            _selected=(k == 0)) for k, s in enumerate(rows)]
                    raise HTTP(
                        200, SELECT(_id=self.keyword, _class='autocomplete',
                                    _size=len(rows), _multiple=(len(rows) == 1),
                                    *options).xml())
                else:
                    raise HTTP(
                        200, SELECT(_id=self.keyword, _class='autocomplete',
                                    _size=len(rows), _multiple=(len(rows) == 1),
                                    *[OPTION(s[field.name],
                                             _selected=(k == 0))
                                      for k, s in enumerate(rows)]).xml())
            else:
                raise HTTP(200, '')

    def __call__(self, field, value, **attributes):
        if self.run_callback:
            self.callback()
        default = dict(
            _type='text',
            value=(value is not None and str(value)) or '',
        )
        attr = StringWidget._attributes(field, default, **attributes)
        div_id = self.keyword + '_div'
        attr['_autocomplete'] = 'off'
        if self.is_reference:
            key2 = self.keyword + '_aux'
            key3 = self.keyword + '_auto'
            attr['_class'] = 'string'
            name = attr['_name']
            if 'requires' in attr:
                del attr['requires']
            attr['_name'] = key2
            value = attr['value']
            if isinstance(self.fields[0], Field.Virtual):
                record = None
                table_rows = self.db(self.db[self.fields[0].tablename]).select(orderby=self.orderby)
                for row in table_rows:
                    if row.id == value:
                        record = row
                        break
            else:
                record = self.db(
                    self.fields[0] == value).select(self.fields[0]).first()
            attr['value'] = record and record[self.fields[0].name]
            attr['_onblur'] = "jQuery('#%(div_id)s').delay(500).fadeOut('slow');" % \
                dict(div_id=div_id, u='F' + self.keyword)
            js = """
            (function($) {
                function doit(e_) {
                    $('#%(key3)s').val('');
                    var e=e_.which?e_.which:e_.keyCode;
                    function %(u)s(){
                        $('#%(id)s').val($('#%(key)s option:selected').text());
                        $('#%(key3)s').val($('#%(key)s option:selected').text());
                        if($('#%(id)s').attr('oncomplete')){
                            setTimeout($('#%(id)s').attr('oncomplete'), 0);
                        }
                    };
                    if(e==39) %(u)s();
                    else if(e==40) {
                        if($('#%(key)s option:selected').next().length)
                        $('#%(key)s option:selected').attr('selected',null).next().attr('selected','selected');
                        %(u)s();
                    }
                    else if(e==38) {
                    if($('#%(key)s option:selected').prev().length)
                        $('#%(key)s option:selected').attr('selected',null).prev().attr('selected','selected');
                        %(u)s();
                    }
                    else if($('#%(id)s').val().length>=%(min_length)s)
                        $.get('%(url)s&%(key)s='+encodeURIComponent($('#%(id)s').val()),
                            function(data){
                                if(data=='')$('#%(key3)s').val('');
                                else{
                                    $('#%(id)s').next('.error').hide();
                                    $('#%(div_id)s').html(data).show().focus();
                                    $('#%(div_id)s select').css('width',$('#%(id)s').css('width'));
                                    $('#%(key3)s').val($('#%(key)s option:selected').val());
                                    $('#%(key)s').change(%(u)s).click(%(u)s);
                                };
                            });
                    else $('#%(div_id)s').fadeOut('slow');
                }
            var tmr = null;
            $("#%(id)s").on('keyup focus',function(e) {
                if (tmr) clearTimeout(tmr);
                if($('#%(id)s').val().length>=%(min_length)s) {
                    tmr = setTimeout(function() { tmr = null; doit(e); }, 300);
                }
            });
            })(jQuery)""".replace('\n', '').replace(' ' * 4, '') % \
                dict(url=self.url, min_length=self.min_length,
                     key=self.keyword, id=attr['_id'], key2=key2, key3=key3,
                     name=name, div_id=div_id, u='F' + self.keyword)
            return CAT(INPUT(**attr),
                       INPUT(_type='hidden', _id=key3, _value=value,
                             _name=name, requires=field.requires),
                       SCRIPT(js),
                       DIV(_id=div_id, _style='position:absolute;'))
        else:
            attr['_name'] = field.name
            attr['_onblur'] = "jQuery('#%(div_id)s').delay(500).fadeOut('slow');" % \
                dict(div_id=div_id, u='F' + self.keyword)
            js = """
            (function($) {
            function doit(e_) {
                var e=e_.which?e_.which:e_.keyCode;
                function %(u)s(){
                    $('#%(id)s').val($('#%(key)s option:selected').val());
                    if($('#%(id)s').attr('oncomplete')){
                        setTimeout($('#%(id)s').attr('oncomplete'), 0);
                    }
                };
                if(e==39) %(u)s();
                else if(e==40) {
                    if($('#%(key)s option:selected').next().length)
                    $('#%(key)s option:selected').attr('selected',null).next().attr('selected','selected');
                    %(u)s();
                } else if(e==38) {
                if($('#%(key)s option:selected').prev().length)
                $('#%(key)s option:selected').attr('selected',null).prev().attr('selected','selected');
                %(u)s();
                } else if($('#%(id)s').val().length>=%(min_length)s)
                        $.get('%(url)s&%(key)s='+encodeURIComponent($('#%(id)s').val()),
                            function(data){
                                $('#%(id)s').next('.error').hide();
                                $('#%(div_id)s').html(data).show().focus();
                                $('#%(div_id)s select').css('width',$('#%(id)s').css('width'));
                                $('#%(key)s').change(%(u)s).click(%(u)s);
                            }
                        );
                else $('#%(div_id)s').fadeOut('slow');
            }
            var tmr = null;
            $("#%(id)s").on('keyup focus',function(e) {
                if (tmr) clearTimeout(tmr);
                if($('#%(id)s').val().length>=%(min_length)s) {
                    tmr = setTimeout(function() { tmr = null; doit(e); }, 300);
                }
            });
            })(jQuery)""".replace('\n', '').replace(' ' * 4, '') % \
                dict(url=self.url, min_length=self.min_length,
                     key=self.keyword, id=attr['_id'], div_id=div_id, u='F' + self.keyword)
            return CAT(INPUT(**attr), SCRIPT(js),
                       DIV(_id=div_id, _style='position:absolute;'))
