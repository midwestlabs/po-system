from gluon import SQLFORM
from gluon.storage import Storage

class InjectableSQLFORM(SQLFORM):
    def __init__(self, *args, **kwargs):
        """
        Creates a new SQLFORM object. In addition, allows you to specify certain fields to "monitor" for changes.
        This is relevant for the changed() method, as we will check these fields to see if any of them changed on the form submission. 

        See also http://web2py.com/books/default/chapter/29/07/forms-and-validators#SQLFORM

        :usage: form = InjectableSQLFORM(required_sqlform_params)

        :param monitor_fields (optional): A list of fields that we are to monitor instead of the default "all fields on the base form". If provided we will instead only check these fields for changes. By default we will check all the fields on the base form provided. Injected fields are not checked since we do not have a point of origin for them

        :return InjectableSQLFORM:
        """
        self.injections = []
        if "monitor_fields" in kwargs.keys():
            self.monitor_fields = kwargs['monitor_fields']
            del(kwargs['monitor_fields'])
        else:
            self.monitor_fields = None
        super().__init__(*args, **kwargs)

    def inject_items(self, injection_points):
        """
        Injects any provided components into a provided SQLForm

        :usage inject_items(injection_points):

        :requires: None

        :params injection_points: 2 Layer dictionary where the first key is the attach-point on the form (where you want the item to be injected at) and the value is a dictionary formatted as follows
        dict(
            location,
            label,
            widget,
            key
        ).
        Valid values for the inside dictionary are 
        location: "before" or "after". This is on which side of the defined injection point you want the injection to occur
        label: An HTML compatible string to be used as the label
        widget: An HTML compatible string to be used as the widget
        key: The name of the new point on the form we just created.

        :returns None:

        :notes:
            You can chain injection points together. Meaning the following injection points dictionary is valid
            dict("some existing place on a form"=dict(
                    location: "after",
                    label:"<h2>Some Label 1</h2>",
                    widget:'',
                    key:"a new place"
                ),
                "a new place"=dict(
                    location: "after",
                    label:"<h2>Some Label 2</h2>",
                    widget:'',
                    key:"another new place"
                )
            )
            Notice how "another new place" is chained after "a new place" even though "a new place" doesn't exist when this is injected into the form.
        """

        widgets = Storage()
        labels = Storage()

        temp_widget_dict = self.custom.widget
        while len(temp_widget_dict):
            key = next(iter(temp_widget_dict))
            label = temp_widget_dict[key]
            
            injection = None
            if key in injection_points.keys():
                injection = injection_points[key]
            widgets[key] = label

            if injection and injection['location'] == 'after':
                try:
                    temp_widget_dict[injection['key']] = injection['widget']
                except KeyError:
                    temp_widget_dict[injection['key']] = ''

            if key not in self.fields:
                self.fields.append(key)
                if key not in self.injections:
                    self.injections.append(key)

            del(temp_widget_dict[key])
        
        temp_labels_dict = self.custom.label
        while len(temp_labels_dict):
            key = next(iter(temp_labels_dict))
            label = temp_labels_dict[key]
            
            injection = None
            if key in injection_points.keys():
                injection = injection_points[key]
            labels[key] = label

            if injection and injection['location'] == 'after':
                try:
                    temp_labels_dict[injection['key']] = injection['label']
                except KeyError:
                    temp_labels_dict[injection['key']] = ''
            del(temp_labels_dict[key])

        self.custom.widget = widgets
        self.custom.label = labels

    def process(self, *args, **kwargs):
        """
        More information here http://web2py.com/books/default/chapter/29/07/forms-and-validators#The-process-and-validate-methods

        :note: This will extract the injected fields from the form before passing it to the parent
        in order to be "processed". Once that has been complete, we will reinject the methods

        """
        self.previous_record = self.record
        injected_info = []
        for injected_item in self.injections:
            if injected_item in self.fields:
                injected_info.append(dict(item=injected_item, index=self.fields.index(injected_item)))
                del(self.fields[self.fields.index(injected_item)])

        result = super().process(*args, **kwargs)

        for injected_item in injected_info:
            self.fields.insert(injected_item['index'], injected_item['item'])

        return result

    def get_injected_info(self, request):
        """
        Extracts the relevant information from request.post_vars, related to the injected fields in the form

        :usage get_injected_info(request):

        :param request: The valid request provided by web2py. Most times, simply providing the variable "request" is good enough here

        :requires None:

        :returns: This will return a dictionary formatted with each provided injection point as a key, and the value being whatever is in the request.post_vars, or None if there is nothing in the request.post_vars for that value

        """
        injected_info = {key: None for key in self.injections}
        for key, value in request.post_vars.items():
            if key in self.injections:
                injected_info[key] = value
        
        return injected_info

    def changed(self, request):
        """
        NOTE! DO NOT PUT INJECTED FIELDS IN THE MONITOR_FIELDS! YOU WILL NOT GET DESIRED RESULTS
        """
        try:
            # If self.previous_record does not exist, there is no way anything has changed yet
            self.previous_record
        except:
            return False
        
        if self.monitor_fields is not None:
            return self._changed_based_on_monitor_fields(request)
        else:
            return self._changed_based_on_default_fields(request)

    def _changed_based_on_monitor_fields(self, request):
        for key in self.monitor_fields:
            if self.previous_record[key] == request.post_vars[key]:
                continue
            else:
                return True
        return False

    def _changed_based_on_default_fields(self, request):
        for key, value in request.post_vars.items():
            if key in self.injections or key.startswith("_"):
                continue
            comp_value = str(self.previous_record[key]) if self.previous_record[key] != None else ''
            if value == 'on':
                value = 'True'
            if value == 'off':
                value = 'False'
            if (self.monitor_fields is not None and key in self.monitor_fields) or comp_value == value:
                continue
            else:
                return True
        return False