from gluon import DIV, INPUT, LABEL, BUTTON, DAL, SPAN, A


UPLOAD_TYPES = ["quote", "contract", "packing_slip", "invoice"] 
# TODO(Mike): Receipt is for upcoming credit card reconciliation. For now it will remain commented out.
# Its also worth considering moving these to a database table instead of a "raw" list like this
# So the types are truly dynamic
# UPLOAD_TYPES = ["quote", "contract", "packing_slip", "invoice", "receipt"]

def get_editable_views(po_number, attachments=None, editable_fields=None):
    """
    If provided, only the fields in editable_fields will be editable. All else will not
    """
    return _get_views(po_number, True, attachments, editable_fields)

def get_uneditable_views(po_number, attachments=None):
    return _get_views(po_number, False, attachments)

def _get_views(po_number, editable, attachments=None, editable_fields=None):
    views = dict()
    for upload_type in UPLOAD_TYPES:
        label = upload_type.title().replace("_", " ")
        container_name = upload_type+"_container"
        if editable_fields == None or upload_type in editable_fields:
            views[upload_type] = _get_upload_container(label, upload_type, container_name, editable, attachments)
        else:
            views[upload_type] = _get_upload_container(label, upload_type, container_name, False, attachments)


    return views

def _get_upload_container(label, upload_type, uploads_container_name, editable=True, attachments=None):
    container = DIV(_style="padding-bottom: 1em;")

    title_container = DIV(_class="form-group row", _style="padding-left: 15px")
    label = LABEL(label, _style="margin-right: -15px")
    title_container.append(label)
    
    if editable:
        add_button_container = DIV(_style="display: flex; flex-direction: column; padding-left: .75em;")
        add_button = A(u'\u271A', _class="button btn btn-default line-button", _style="margin-top: auto; margin-bottom: auto;", _onclick=f"event.stopPropagation(); $('#{upload_type}').click(); return false;")
        add_button_container.append(add_button)
        title_container.append(add_button_container)

    uploads_container = DIV(_id=uploads_container_name)
    if attachments:
        for attachment in attachments[upload_type]:
            uploads_container.append(_get_attachment_view(attachment[0], upload_type, editable))

    container.append(title_container)
    container.append(uploads_container)

    return container

def _get_attachment_view(attachment, attachment_type, editable=True):
    container = DIV(_class="file-attachment")
    hidden_input = INPUT(_name=attachment_type, _style="display: none;", _type="file", _files=str(attachment))
    container.append(hidden_input)
    container.append(attachment)
    if editable:
        remove_button = BUTTON(
            u'\u2716', 
            _onclick=\
                "event.stopPropagation();"+
                "$(this).parent().remove();"+
                # f"$('#{container_id}').remove();"+
                "return false;",
            _class="btn line-button"
        )
        container.append(remove_button)
    return container

def get_attachments(db: DAL, po_number, file_repr, editable_types=None):
    return stringify_po_attachments(
                db,
                sort_po_attachments(
                    [attachment for attachment in db(get_po_attachments_query(db, po_number)).select()]
                ), 
                file_repr,
                editable_types
            )

def get_po_attachments_query(db, po_number):
    # po_attachments should be a query (Set object). We should take this query and use that to create the view
    query = (
        (db.po_form_attachments.po_form == po_number)
    )

    return query

def sort_po_attachments(po_attachments):
    attachment_dict = dict(
        invoice=[],
        quote=[],
        packing_slip=[],
        contract=[],
        receipt=[]
    )
    for attachment in po_attachments:
        if attachment.upload_type == "quote":
            attachment_dict['quote'].append(attachment)
        elif attachment.upload_type == "invoice":
            attachment_dict['invoice'].append(attachment)
        elif attachment.upload_type == "packing_slip":
            attachment_dict['packing_slip'].append(attachment)
        elif attachment.upload_type == "contract":
            attachment_dict['contract'].append(attachment)
        elif attachment.upload_type == "receipt":
            attachment_dict['receipt'].append(attachment)

    return attachment_dict

def stringify_po_attachments(db, po_attachments, file_repr, editable_types=None):
    attachment_dict = dict()

    for attachment_type, attachments in po_attachments.items():
        attachment_dict[attachment_type] = []
        for attachment in attachments:
            if editable_types and attachment_type not in editable_types:
                attachment_dict[attachment_type].append((file_repr(db.po_form_attachments.file, attachment.file, None), False))
            else:
                attachment_dict[attachment_type].append((file_repr(db.po_form_attachments.file, attachment.file, None), True))
    return attachment_dict
