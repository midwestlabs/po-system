#!/usr/bin/python

import sys
import os.path

GLUON_PATH = '/opt/web2py'
sys.path.insert(1, os.path.realpath(GLUON_PATH))

from gluon.dal import DAL
from gluon.shell import exec_environment, execfile
from gluon.globals import Request, Response, Session
from gluon import current
from gluon.storage import Storage
from gluon.http import HTTP

original_globals = dict(globals())

import unittest

TEST_DB = 'sqlite://testing.sqlite'

class Web2PyTest(unittest.TestCase):
    # Parent Class
    """
        All tests run against the PO system should inherit this web2pytest class
    """
    CLEAR_TABLES = None
    CONTROLLER = None

    def setUp(self):
        self.setup_web2py()
        self.exec_controller()

    def setup_web2py(self):
        if db._uri != TEST_DB:
            raise Exception('Not using testing database')
        if self.CONTROLLER is None:
            raise NotImplementedError('CONTROLLER must be defined by a subclass of Web2PyTest')
        self.globals = original_globals.copy()
        self.clear_tables()

    def exec_controller(self):
        execfile(self.CONTROLLER, self.globals)

    def tearDown(self):
        self.clear_tables()
        self.web2py_logout()

    def clear_tables(self):
        # Dropping everything in the CLEAR_TABLES array
        if self.CLEAR_TABLES is None:
            raise NotImplementedError('CLEAR_TABLES must be defined by a subclass of Web2PyTest')
        db = self.globals['db']
        db.rollback()
        for table in self.CLEAR_TABLES:
            db[table].truncate()
        db.commit()
    
    def web2py_register(self, username, password):
        auth = self.globals['auth']
        test_user = dict(username=username, password=password)
        auth.register_bare(**test_user)

    def web2py_login(self, username, password):
        auth = self.globals['auth']
        test_user = dict(username=username, password=password)
        auth.login_bare(**test_user)

    def web2py_set_role(self, username, role_name, department=None):
        auth = self.globals['auth']
        db = self.globals['db']
        if department:
            role = f'{department} {role_name}'
        else:
            role = role_name
        auth.add_membership(
            group_id=auth.id_group(role), 
            user_id=db(db.auth_user.username == username).select().first().id)

    def web2py_remove_role(self, username, role_name):
        auth = self.globals['auth']
        db = self.globals['db']
        auth.del_membership(
            group_id=auth.id_group(role_name), 
            user_id=db(db.auth_user.username == username).select().first().id)

    def web2py_logout(self):
        auth = self.globals['auth']
        auth.logout(next=None)

class CreatePOControllerTest(Web2PyTest):
    CLEAR_TABLES = [
        'auth_user',
        'auth_membership',
        'po_form',
        'po_note',
        'po_item',
        'po_rule_map',
    ]

    CONTROLLER = 'applications/po/controllers/createPO.py'
    
    USER = None
    PASSWORD = None
    ROLE = None
    DEPARTMENT = None

    def setUp(self):
        self.setup_web2py()

        if self.USER:
            user = dict(username=self.USER, password=self.PASSWORD)

            self.web2py_register(**user)
            self.web2py_login(**user)
            if self.ROLE:
                self.web2py_set_role(user['username'], self.ROLE, self.DEPARTMENT)

        self.exec_controller()

    def _helper_create_po_with_note(self):
        exec('po_number=createNewPo()', self.globals)
        return self.globals['po_number']

    def _helper_create_po_without_note(self):
        exec('po_number=createNewPo(addCreationNote=False)', self.globals)
        return self.globals['po_number']

    def _helper_clone_po(self, po_number):
        exec(f'cloned_po=cloneExistingPo({po_number})', self.globals)
        return self.globals['cloned_po']

    def createpo_index(self):
        exec('index()', self.globals)

    def createpo_with_note(self):
        po_number = self._helper_create_po_with_note()
        db = self.globals['db']

        submitter = self.globals['auth'].user.id
        created_status = db(db.po_status_reference.status == 'Created').select().first().id
        po_count = db(db.po_form.id >= 0).count()
        note_count = db(db.po_note.po_form == po_number).count()
        notes_created_comment = 'Created PO'

        po = db(db.po_form.id == po_number).select().first()
        notes = db(db.po_note.po_form == po_number).select()

        self.assertEqual(po_count, 1)
        self.assertEqual(po.status, created_status)
        self.assertEqual(note_count, 1)
        for note in notes:
            self.assertEqual(note.made_by, submitter)
            self.assertEqual(note.comment, notes_created_comment)
            self.assertFalse(note.po_approval)

    def createpo_without_note(self):
        po_number = self._helper_create_po_without_note()
        db = self.globals['db']

        submitter = self.globals['auth'].user.id
        created_status = db(db.po_status_reference.status == 'Created').select().first().id
        po_count = db(db.po_form.id >= 0).count()
        note_count = db(db.po_note.po_form == po_number).count()
        po = db(db.po_form.id == po_number).select().first()

        self.assertEqual(po_count, 1)
        self.assertEqual(po.submitter, submitter)
        self.assertEqual(po.status, created_status)
        self.assertEqual(note_count, 0)


class SetupSubmitterTest(CreatePOControllerTest):
    USER = 'submitter'
    PASSWORD = 'password'
    ROLE = 'Submitter'
    DEPARTMENT = 'Feeds/Pet Foods'

class SetupSupervisorTest(CreatePOControllerTest):
    USER = 'supervisor'
    PASSWORD = 'password'
    ROLE = 'Supervisor'
    DEPARTMENT = 'Feeds/Pet Foods'

class SetupDepartmentHeadTest(CreatePOControllerTest):
    USER = 'department_head'
    PASSWORD = 'password'
    ROLE = 'Department Head'
    DEPARTMENT = 'Feeds/Pet Foods'

class SetupSuperapproverTest(CreatePOControllerTest):
    USER = 'superapprover'
    PASSWORD = 'password'
    ROLE = 'superapprover'

class SetupSuperuserTest(CreatePOControllerTest):
    USER = 'superuser'
    PASSWORD = 'password'
    ROLE = 'superuser'

class SetupAccountingTest(CreatePOControllerTest):
    USER = 'accounting'
    PASSWORD = 'password'
    ROLE = 'accounting'

class SetupInvalidUserPermissionTest(CreatePOControllerTest):
    USER = 'invalid_user'
    PASSWORD = 'password'

class SetupNoUserTest(CreatePOControllerTest):
    pass


class TestSubmitterCreatePOWithNote(SetupSubmitterTest):
    def test_submitter_create_po_with_note(self):
        self.createpo_with_note()

class TestSubmitterCreatePOWithoutNote(SetupSubmitterTest):
    def test_submitter_createpo_without_note(self):
        self.createpo_without_note()

class TestSubmitterIndex(SetupSubmitterTest):
    def test_submitter_index(self):
        try:
            self.createpo_index()
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertTrue(exception.headers['Location'].startswith('/po/editPO/index'))

class TestSupervisorIndex(SetupSupervisorTest):
    def test_supervisor_index(self):
        try:
            self.createpo_index()
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertTrue(exception.headers['Location'].startswith('/po/editPO/index'))

class TestSupervisorCreatePOWithNote(SetupSupervisorTest):
    def test_supervisor_createpo_with_note(self):
        self.createpo_with_note()

class TestSupervisorCreatePOWithoutNote(SetupSupervisorTest):
    def test_supervisor_createpo_without_note(self):
        self.createpo_without_note()

class TestDepartmentHeadIndex(SetupDepartmentHeadTest):
    def test_department_head_index(self):
        try:
            self.createpo_index()
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertTrue(exception.headers['Location'].startswith('/po/editPO/index'))

class TestDepartmentHeadCreatePOWithNote(SetupDepartmentHeadTest):
    def test_department_head_createpo_with_note(self):
        self.createpo_with_note()

class TestDepartmentHeadCreatePOWithoutNote(SetupDepartmentHeadTest):
    def test_department_head_createpo_without_note(self):
        self.createpo_without_note()

class TestSuperapproverIndex(SetupSuperapproverTest):
    def test_superapprover_index(self):
        try:
            self.createpo_index()
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertTrue(exception.headers['Location'].startswith('/po/editPO/index'))

class TestSuperapproverCreatePOWithNote(SetupSuperapproverTest):
    def test_superapprover_createpo_with_note(self):
        self.createpo_with_note()

class TestSuperapproverCreatePOWithoutNote(SetupSuperapproverTest):
    def test_superapprover_createpo_without_note(self):
        self.createpo_without_note()

class TestSuperuserIndex(SetupSuperuserTest):
    def test_superuser_index(self):
        try:
            self.createpo_index()
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertTrue(exception.headers['Location'].startswith('/po/editPO/index'))

class TestSuperuserCreatePOWithNote(SetupSuperuserTest):
    def test_superuser_createpo_with_note(self):
        self.createpo_with_note()

class TestSuperuserCreatePOWithoutNote(SetupSuperuserTest):
    def test_superuser_createpo_without_note(self):
        self.createpo_without_note()

class TestAccountingUserIndex(SetupAccountingTest):
    def test_accounting_index(self):
        try:
            self.createpo_index()
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertTrue(exception.headers['Location'].endswith('not_authorized'))

class TestAccountingUserCreatePOWithNote(SetupAccountingTest):
    def test_accounting_createpo_with_note(self):
        try:
            self.createpo_with_note()
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertTrue(exception.headers['Location'].endswith('not_authorized'))

class TestAccountingUserCreatePOWithoutNote(SetupAccountingTest):
    def test_accounting_without_note(self):
        try:
            self.createpo_without_note()
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertTrue(exception.headers['Location'].endswith('not_authorized'))

class TestInvalidPermissionUserIndex(SetupInvalidUserPermissionTest):
    def test_invalid_permission_index(self):
        try:
            self.createpo_index()
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertTrue(exception.headers['Location'].endswith('not_authorized'))

class TestInvalidPermissionUserCreatePOWithNote(SetupInvalidUserPermissionTest):
    def test_invalid_permission_createpo_with_note(self):
        try:
            self.createpo_with_note()
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertTrue(exception.headers['Location'].endswith('not_authorized'))

class TestInvalidPermissionUserCreatePOWithoutNote(SetupInvalidUserPermissionTest):
    def test_invalid_permission_without_note(self):
        try:
            self.createpo_without_note()
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertTrue(exception.headers['Location'].endswith('not_authorized'))

class TestNoUserIndex(SetupNoUserTest):
    def test_no_user_index(self):
        try:
            self.createpo_index()
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertTrue('login' in exception.headers['Location'])

class TestNoUserCreatePOWithNote(SetupNoUserTest):
    def test_no_user_create_po_with_note(self):
        try:
            self.createpo_with_note()
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertTrue('login' in exception.headers['Location'])

class TestNoUserCreatePOWithoutNote(SetupNoUserTest):
    def test_no_user_create_po_without_note(self):
        try:
            self.createpo_without_note()
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertTrue('login' in exception.headers['Location'])

class TestClonePO(SetupSubmitterTest):
    
    def test_clone_po(self):
        db = self.globals['db']

        origin_po = self._helper_create_po_with_note()
        db.po_item.insert(
            po_form=origin_po,
            catalog="some item 1",
            description="some item 1",
            quantity=1,
            price_per_unit=10,
            extended_price=10
        )

        self.web2py_logout()
        self.web2py_register('new user', 'password')
        self.web2py_set_role('new user', self.ROLE, self.DEPARTMENT)
        self.web2py_login('new user', 'password')

        department = db(db.department.name == self.DEPARTMENT).select().first().id

        db(db.po_form.id == origin_po).update(
            department=department,
            subtotal=10,
            vendor="some vendor",
            phone_num='18000000000',
            account_num='1',
            contact_person='some person',
            project="some project",
            on_vendor_approval_list=True,
            sole_source_purchase=True,
            total=10,
            tax=0,
            status=db(db.po_status_reference.status == 'Ordered').select().first().id,
            order_confirmation_num='1234567890',
            tracking_number='0987654321a',
            shipping_company="awesome shipping llc",
            invoice='some invoice',
            contract='some contract',
            quote1='quote1',
            quote2='quote2',
            quote3='quote3',
            packing_slip='packing slip'
        )

        cloned_po = self._helper_clone_po(origin_po)
        self.assertEqual(db.po_form(cloned_po).department, db.po_form(origin_po).department)
        self.assertEqual(db.po_form(cloned_po).subtotal, db.po_form(origin_po).subtotal)
        self.assertEqual(db.po_form(cloned_po).vendor, db.po_form(origin_po).vendor)
        self.assertEqual(db.po_form(cloned_po).phone_num, db.po_form(origin_po).phone_num)
        self.assertEqual(db.po_form(cloned_po).account_num, db.po_form(origin_po).account_num)
        self.assertEqual(db.po_form(cloned_po).on_vendor_approval_list, db.po_form(origin_po).on_vendor_approval_list)
        self.assertEqual(db.po_form(cloned_po).sole_source_purchase, db.po_form(origin_po).sole_source_purchase)
        self.assertEqual(db.po_form(cloned_po).total, db.po_form(origin_po).total)
        self.assertEqual(db.po_form(cloned_po).tax, db.po_form(origin_po).tax)
        self.assertEqual(db.po_form(cloned_po).status, db(db.po_status_reference.status == 'Created').select().first().id)
        self.assertEqual(db.po_form(cloned_po).order_confirmation_num, None)
        self.assertEqual(db.po_form(cloned_po).shipping_company, None)

tests = [
    TestSubmitterCreatePOWithNote('test_submitter_create_po_with_note'),
    TestSubmitterCreatePOWithoutNote('test_submitter_createpo_without_note'),
    TestSubmitterIndex('test_submitter_index'),
    TestSupervisorIndex('test_supervisor_index'),
    TestSupervisorCreatePOWithNote('test_supervisor_createpo_with_note'),
    TestSupervisorCreatePOWithoutNote('test_supervisor_createpo_without_note'),
    TestDepartmentHeadIndex('test_department_head_index'),
    TestDepartmentHeadCreatePOWithNote('test_department_head_createpo_with_note'),
    TestDepartmentHeadCreatePOWithoutNote('test_department_head_createpo_without_note'),
    TestSuperapproverIndex('test_superapprover_index'),
    TestSuperapproverCreatePOWithNote('test_superapprover_createpo_with_note'),
    TestSuperapproverCreatePOWithoutNote('test_superapprover_createpo_without_note'),
    TestSuperuserIndex('test_superuser_index'),
    TestSuperuserCreatePOWithNote('test_superuser_createpo_with_note'),
    TestSuperuserCreatePOWithoutNote('test_superuser_createpo_without_note'),
    TestAccountingUserIndex('test_accounting_index'),
    TestAccountingUserCreatePOWithNote('test_accounting_createpo_with_note'),
    TestAccountingUserCreatePOWithoutNote('test_accounting_without_note'),
    TestInvalidPermissionUserIndex('test_invalid_permission_index'),
    TestInvalidPermissionUserCreatePOWithNote('test_invalid_permission_createpo_with_note'),
    TestInvalidPermissionUserCreatePOWithoutNote('test_invalid_permission_without_note'),
    TestNoUserIndex('test_no_user_index'),
    TestNoUserCreatePOWithNote('test_no_user_create_po_with_note'),
    TestNoUserCreatePOWithoutNote('test_no_user_create_po_without_note'),
    TestClonePO('test_clone_po'),
]

suite = unittest.TestSuite()
[suite.addTest(test) for test in tests]

unittest.TextTestRunner(verbosity=2).run(suite)