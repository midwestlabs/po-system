#!/usr/bin/env python

import sys
import os.path

GLUON_PATH = '/opt/web2py'
sys.path.insert(1, os.path.realpath(GLUON_PATH))

from gluon.dal import DAL
from gluon.shell import exec_environment, execfile
from gluon.globals import Request, Response, Session
from gluon import current
from gluon.storage import Storage
from gluon.http import HTTP

original_globals = dict(globals())

import unittest
import random
import datetime
# import unittest.mock as mock
import freezegun
from freezegun import freeze_time

TEST_DB = 'sqlite://testing.sqlite'

class Web2PyTest(unittest.TestCase):
    # Parent Class
    """
        All tests run against the PO system should inherit this web2pytest class
    """
    CLEAR_TABLES = None
    CONTROLLER = None

    def setUp(self):
        # Probably should have something setup to copy data from one database to another, only if needed
        self.setup_web2py()
        self.exec_controller()

    def setup_web2py(self):
        if db._uri != TEST_DB:
            raise Exception('Not using testing database')
        self.globals = original_globals.copy()
        self.clear_tables()

    def exec_controller(self):
        if self.CONTROLLER:
            execfile(self.CONTROLLER, self.globals)

    def tearDown(self):
        self.clear_tables()
        self.web2py_logout()
        if self.globals['request']['_vars'] is not None:
            self.globals['request']['_vars'].clear()

    def clear_tables(self):
        # Dropping everything in the CLEAR_TABLES array
        if self.CLEAR_TABLES is None:
            raise NotImplementedError('CLEAR_TABLES must be defined by a subclass of Web2PyTest')
        db = self.globals['db']
        db.rollback()
        for table in self.CLEAR_TABLES:
            db[table].truncate()
        db.commit()

    def web2py_register(self, username, password):
        auth = self.globals['auth']
        test_user = dict(username=username, password=password)
        auth.register_bare(**test_user)
        return self.globals['db'](self.globals['db'].auth_user.username == username).select().first().id

    def web2py_login(self, username, password):
        auth = self.globals['auth']
        test_user = dict(username=username, password=password)
        auth.login_bare(**test_user)

    def web2py_set_role(self, username, role_name, department=None):
        auth = self.globals['auth']
        db = self.globals['db']
        if department:
            role = f'{department} {role_name}'
        else:
            role = role_name
        auth.add_membership(
            group_id=auth.id_group(role), 
            user_id=db(db.auth_user.username == username).select().first().id)

    def web2py_remove_role(self, username, role_name, department=None):
        auth = self.globals['auth']
        db = self.globals['db']
        if department:
            role = f'{department} {role_name}'
        else:
            role = role_name
        auth.del_membership(
            group_id=auth.id_group(role), 
            user_id=db(db.auth_user.username == username).select().first().id)

    def web2py_logout(self):
        auth = self.globals['auth']
        auth.logout(next=None)

class Test_Scheduler(Web2PyTest):
    USER = ''
    PASSWORD = ''
    ROLE = ''
    DEPARTMENT = ''
    CLEAR_TABLES = [
        'auth_user',
        'auth_membership',
        'po_form',
        'po_note',
        'po_item',
        'scheduler_run',
        'scheduler_task',
        'scheduler_task_deps',
        'scheduler_worker'
    ]

    def setUp(self):
        self.setup_web2py()
        if self.USER:
            self.web2py_register(self.USER, self.PASSWORD)
            self.web2py_login(self.USER, self.PASSWORD)
        if self.ROLE:
            if self.DEPARTMENT:
                self.web2py_set_role(self.USER, self.ROLE, self.DEPARTMENT)
            else:
                self.web2py_set_role(self.USER, self.ROLE)

    def _helper_createpo(self):
        copy_globals = original_globals.copy()
        _controller = 'applications/po/controllers/createPO.py'
        execfile(_controller, copy_globals)
        exec('po_number=createNewPo()', copy_globals)
        po_number = copy_globals['po_number']
        return po_number

    def _helper_set_po_department(self, po_number, department_name):
        db = self.globals['db']
        department_id = db(db.department.name == department_name).select().first().id
        db(db.po_form.id == po_number).update(
            department = department_id
        )

    def _helper_set_subtotal(self, po_number, subtotal):
        db = self.globals['db']
        db(db.po_form.id == po_number).update(subtotal=subtotal)

    def _helper_mark_approval(self, po_number, approver):
        db = self.globals['db']
        db.po_note.insert(
            po_form = po_number,
            made_by = approver,
            comment = 'Approved',
            po_approval = True
        )

    def _helper_add_contract_to_po(self, po_number, contract):
        db = self.globals['db']
        db(db.po_form.id == po_number).update(contract=contract)

    def _helper_set_po_status(self, po_number, status):
        db = self.globals['db']
        status_id = db(db.po_status_reference.status == status).select().first().id
        db(db.po_form.id == po_number).update(
            status = status_id
        )

    def _helper_map_po_to_rule(self, po_number, rule):
        db = self.globals['db']
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule
        )

    def _helper_get_failing_rules(self, po_number):
        exec(f'failing_rules = get_failing_po_rules({po_number})', self.globals)
        return self.globals['failing_rules']

    def scheduler_generate_notify_levels_for_po(self, po_number, approver_counts, failed_rule):
        exec(f'notify_levels = _generate_notify_levels_for_po({po_number}, {approver_counts}, {failed_rule})', self.globals)
        return self.globals['notify_levels']

    def scheduler_get_scheduler_delay(self, delay):
        exec(f'scheduler_delay = _get_scheduler_delay({delay})', self.globals)
        return self.globals['scheduler_delay']

class Test_Get_Notify_Levels_For_Department_With_All_Approval_Levels(Test_Scheduler):
    SUBMITTER = dict(
        username="submitter",
        password="password",
        role="Submitter",
        department="FCC",
        id=None
    )
    SUPERVISOR = dict(
        username="supervisor",
        password="password",
        role="Supervisor",
        department="FCC",
        id=None
    )
    DEPARTMENT_HEAD = dict(
        username="department head",
        password="password",
        role="Department Head",
        department="FCC",
        id=None
    )
    SUPERAPPROVER = dict(
        username="superapprover",
        password="password",
        role="superapprover",
        id=None
    )
    def setUp(self):
        super().setUp()
        self.SUBMITTER['id'] =       self.web2py_register(self.SUBMITTER['username'],        self.SUBMITTER['password'])
        self.SUPERVISOR['id'] =      self.web2py_register(self.SUPERVISOR['username'],       self.SUPERVISOR['password'])
        self.DEPARTMENT_HEAD['id'] = self.web2py_register(self.DEPARTMENT_HEAD['username'],  self.DEPARTMENT_HEAD['password'])
        self.SUPERAPPROVER['id'] =   self.web2py_register(self.SUPERAPPROVER['username'],    self.SUPERAPPROVER['password'])

        self.web2py_set_role(self.SUBMITTER['username'],       self.SUBMITTER['role'],       self.SUBMITTER['department'])
        self.web2py_set_role(self.SUPERVISOR['username'],      self.SUPERVISOR['role'],      self.SUPERVISOR['department'])
        self.web2py_set_role(self.DEPARTMENT_HEAD['username'], self.DEPARTMENT_HEAD['role'], self.DEPARTMENT_HEAD['department'])
        self.web2py_set_role(self.SUPERAPPROVER['username'],   self.SUPERAPPROVER['role'])

        self.web2py_login(self.SUBMITTER['username'], self.SUBMITTER['password'])

    def test_notifies_supervisors(self):
        db = self.globals['db']
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.SUBMITTER['department'])
        self._helper_set_po_status(po_number, 'Submitted')
        self._helper_set_subtotal(po_number, 100.01)
        
        rule = db(
            (db.po_form.id == po_number) &
            (db.po_form.department == db.po_rule.department) &
            (db.po_rule.min_subtotal <= db.po_form.subtotal) &
            ((db.po_rule.max_subtotal >= db.po_form.subtotal) | (db.po_rule.max_subtotal == 0))
        ).select(db.po_rule.id, db.po_rule.min_subtotal).first()
        self._helper_map_po_to_rule(po_number, rule.id)

        failed_rules = self._helper_get_failing_rules(po_number)
        notify_levels = self.scheduler_generate_notify_levels_for_po(po_number, failed_rules['approver_counts'], failed_rules['failed_rules'].first().id)
        self.assertEqual(len(notify_levels), 1)
        self.assertEqual(len(notify_levels[0]), 1)
        self.assertEqual(list(notify_levels[0])[0], self.SUPERVISOR['id'])

    def test_notifies_department_heads(self):
        db = self.globals['db']
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.SUBMITTER['department'])
        self._helper_set_po_status(po_number, 'Submitted')
        self._helper_set_subtotal(po_number, 5000.01)
        self._helper_mark_approval(po_number, self.SUPERVISOR['id'])
        
        rule = db(
            (db.po_form.id == po_number) &
            (db.po_form.department == db.po_rule.department) &
            (db.po_rule.min_subtotal <= db.po_form.subtotal) &
            ((db.po_rule.max_subtotal >= db.po_form.subtotal) | (db.po_rule.max_subtotal == 0))
        ).select(db.po_rule.id, db.po_rule.min_subtotal).first()
        self._helper_map_po_to_rule(po_number, rule.id)

        failed_rules = self._helper_get_failing_rules(po_number)
        notify_levels = self.scheduler_generate_notify_levels_for_po(po_number, failed_rules['approver_counts'], failed_rules['failed_rules'].first().id)
        self.assertEqual(len(notify_levels), 1)
        self.assertEqual(len(notify_levels[0]), 1)
        self.assertEqual(list(notify_levels[0])[0], self.DEPARTMENT_HEAD['id'])

    def test_notifies_superapprovers(self):
        db = self.globals['db']
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.SUBMITTER['department'])
        self._helper_set_po_status(po_number, 'Submitted')
        self._helper_set_subtotal(po_number, 50000.01)
        self._helper_mark_approval(po_number, self.SUPERVISOR['id'])
        self._helper_mark_approval(po_number, self.DEPARTMENT_HEAD['id'])
        
        rule = db(
            (db.po_form.id == po_number) &
            (db.po_form.department == db.po_rule.department) &
            (db.po_rule.min_subtotal <= db.po_form.subtotal) &
            ((db.po_rule.max_subtotal >= db.po_form.subtotal) | (db.po_rule.max_subtotal == 0))
        ).select(db.po_rule.id, db.po_rule.min_subtotal).first()
        self._helper_map_po_to_rule(po_number, rule.id)

        failed_rules = self._helper_get_failing_rules(po_number)
        notify_levels = self.scheduler_generate_notify_levels_for_po(po_number, failed_rules['approver_counts'], failed_rules['failed_rules'].first().id)
        self.assertEqual(len(notify_levels), 1)
        self.assertEqual(len(notify_levels[0]), 1)
        self.assertEqual(list(notify_levels[0])[0], self.SUPERAPPROVER['id'])

    def test_notifies_department_heads_on_lone_supervisor_submission(self):
        # TODO
        pass

class Test_Get_Notify_Levels_For_Department_Without_Supervisor(Test_Scheduler):
    SUBMITTER = dict(
        username="submitter",
        password="password",
        role="Submitter",
        department="IT"
    )
    DEPARTMENT_HEAD = dict(
        username="department head",
        password="password",
        role="Department Head",
        department="IT"
    )
    SUPERAPPROVER = dict(
        username="superapprover",
        password="password",
        role="superapprover"
    )

    def setUp(self):
        super().setUp()
        self.SUBMITTER['id'] =       self.web2py_register(self.SUBMITTER['username'],        self.SUBMITTER['password'])
        self.DEPARTMENT_HEAD['id'] = self.web2py_register(self.DEPARTMENT_HEAD['username'],  self.DEPARTMENT_HEAD['password'])
        self.SUPERAPPROVER['id'] =   self.web2py_register(self.SUPERAPPROVER['username'],    self.SUPERAPPROVER['password'])

        self.web2py_set_role(self.SUBMITTER['username'],       self.SUBMITTER['role'],       self.SUBMITTER['department'])
        self.web2py_set_role(self.DEPARTMENT_HEAD['username'], self.DEPARTMENT_HEAD['role'], self.DEPARTMENT_HEAD['department'])
        self.web2py_set_role(self.SUPERAPPROVER['username'],   self.SUPERAPPROVER['role'])

        self.web2py_login(self.SUBMITTER['username'], self.SUBMITTER['password'])

    def test_notifies_department_heads(self):
        db = self.globals['db']
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.SUBMITTER['department'])
        self._helper_set_po_status(po_number, 'Submitted')
        self._helper_set_subtotal(po_number, 5000.01)
        
        rule = db(
            (db.po_form.id == po_number) &
            (db.po_form.department == db.po_rule.department) &
            (db.po_rule.min_subtotal <= db.po_form.subtotal) &
            ((db.po_rule.max_subtotal >= db.po_form.subtotal) | (db.po_rule.max_subtotal == 0))
        ).select(db.po_rule.id, db.po_rule.min_subtotal).first()
        self._helper_map_po_to_rule(po_number, rule.id)

        failed_rules = self._helper_get_failing_rules(po_number)
        notify_levels = self.scheduler_generate_notify_levels_for_po(po_number, failed_rules['approver_counts'], failed_rules['failed_rules'].first().id)
        self.assertEqual(len(notify_levels), 1)
        self.assertEqual(len(notify_levels[0]), 1)
        self.assertEqual(list(notify_levels[0])[0], self.DEPARTMENT_HEAD['id'])

    def test_notifies_superapprovers(self):
        db = self.globals['db']
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.SUBMITTER['department'])
        self._helper_set_po_status(po_number, 'Submitted')
        self._helper_set_subtotal(po_number, 50000.01)
        self._helper_mark_approval(po_number, self.DEPARTMENT_HEAD['id'])
        
        rule = db(
            (db.po_form.id == po_number) &
            (db.po_form.department == db.po_rule.department) &
            (db.po_rule.min_subtotal <= db.po_form.subtotal) &
            ((db.po_rule.max_subtotal >= db.po_form.subtotal) | (db.po_rule.max_subtotal == 0))
        ).select(db.po_rule.id, db.po_rule.min_subtotal).first()
        self._helper_map_po_to_rule(po_number, rule.id)

        failed_rules = self._helper_get_failing_rules(po_number)
        notify_levels = self.scheduler_generate_notify_levels_for_po(po_number, failed_rules['approver_counts'], failed_rules['failed_rules'].first().id)
        self.assertEqual(len(notify_levels), 1)
        self.assertEqual(len(notify_levels[0]), 1)
        self.assertEqual(list(notify_levels[0])[0], self.SUPERAPPROVER['id'])

    def test_notifies_superapprovers_on_lone_department_head_submission(self):
        # TODO
        pass

class Test_Get_Notify_Levels_For_Department_Without_Supervisor_And_Department_Head(Test_Scheduler):
    SUBMITTER = dict(
        username="submitter",
        password="password",
        role="Submitter",
        department="Construction"
    )
    SUPERAPPROVER = dict(
        username="superapprover",
        password="password",
        role="superapprover"
    )

    def setUp(self):
        super().setUp()
        self.SUBMITTER['id'] =       self.web2py_register(self.SUBMITTER['username'],        self.SUBMITTER['password'])
        self.SUPERAPPROVER['id'] =   self.web2py_register(self.SUPERAPPROVER['username'],    self.SUPERAPPROVER['password'])

        self.web2py_set_role(self.SUBMITTER['username'],       self.SUBMITTER['role'],       self.SUBMITTER['department'])
        self.web2py_set_role(self.SUPERAPPROVER['username'],   self.SUPERAPPROVER['role'])

        self.web2py_login(self.SUBMITTER['username'], self.SUBMITTER['password'])

    def test_notifies_superapprovers(self):
        db = self.globals['db']
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.SUBMITTER['department'])
        self._helper_set_po_status(po_number, 'Submitted')
        self._helper_set_subtotal(po_number, 50000.01)
        
        rule = db(
            (db.po_form.id == po_number) &
            (db.po_form.department == db.po_rule.department) &
            (db.po_rule.min_subtotal <= db.po_form.subtotal) &
            ((db.po_rule.max_subtotal >= db.po_form.subtotal) | (db.po_rule.max_subtotal == 0))
        ).select(db.po_rule.id, db.po_rule.min_subtotal).first()
        self._helper_map_po_to_rule(po_number, rule.id)

        failed_rules = self._helper_get_failing_rules(po_number)
        notify_levels = self.scheduler_generate_notify_levels_for_po(po_number, failed_rules['approver_counts'], failed_rules['failed_rules'].first().id)
        self.assertEqual(len(notify_levels), 1)
        self.assertEqual(len(notify_levels[0]), 1)
        self.assertEqual(list(notify_levels[0])[0], self.SUPERAPPROVER['id'])

class Test_Get_Scheduler_Delay(Test_Scheduler):

    def test_today_is_thursday(self):
        freezer = freeze_time('2020-05-07')
        freezer.start()
        delay = self.scheduler_get_scheduler_delay(0)
        freezer.stop()
        self.assertEqual(delay, 24)

    def test_today_is_friday(self):
        freezer = freeze_time('2020-05-08')
        freezer.start()
        delay = self.scheduler_get_scheduler_delay(0)
        freezer.stop()
        self.assertEqual(delay, 72)

    def test_today_is_saturday(self):
        freezer = freeze_time('2020-05-09')
        freezer.start()
        delay = self.scheduler_get_scheduler_delay(0)
        freezer.stop()
        self.assertEqual(delay, 48)

    def test_today_is_sunday(self):
        freezer = freeze_time('2020-05-10')
        freezer.start()
        delay = self.scheduler_get_scheduler_delay(0)
        freezer.stop()
        self.assertEqual(delay, 24)

tests = [
    Test_Get_Notify_Levels_For_Department_With_All_Approval_Levels('test_notifies_supervisors'),
    Test_Get_Notify_Levels_For_Department_With_All_Approval_Levels('test_notifies_department_heads'),
    Test_Get_Notify_Levels_For_Department_With_All_Approval_Levels('test_notifies_superapprovers'),
    Test_Get_Notify_Levels_For_Department_Without_Supervisor('test_notifies_department_heads'),
    Test_Get_Notify_Levels_For_Department_Without_Supervisor('test_notifies_superapprovers'),
    Test_Get_Notify_Levels_For_Department_Without_Supervisor_And_Department_Head('test_notifies_superapprovers'),
    Test_Get_Scheduler_Delay('test_today_is_thursday'),
    Test_Get_Scheduler_Delay('test_today_is_friday'),
    Test_Get_Scheduler_Delay('test_today_is_saturday'),
    Test_Get_Scheduler_Delay('test_today_is_sunday'),
]

suite = unittest.TestSuite()
[suite.addTest(test) for test in tests]

unittest.TextTestRunner(verbosity=2).run(suite)