#!/usr/bin/env python

import sys
import os.path

GLUON_PATH = '/opt/web2py'
sys.path.insert(1, os.path.realpath(GLUON_PATH))

from gluon.dal import DAL
from gluon.shell import exec_environment, execfile
from gluon.globals import Request, Response, Session
from gluon import current
from gluon.storage import Storage
from gluon.http import HTTP

original_globals = dict(globals())

import unittest
# import Web2PyTest
import random
import datetime

TEST_DB = 'sqlite://testing.sqlite'

class Web2PyTest(unittest.TestCase):
    # Parent Class
    """
        All tests run against the PO system should inherit this web2pytest class
    """
    CLEAR_TABLES = None
    CONTROLLER = None

    def setUp(self):
        # Probably should have something setup to copy data from one database to another, only if needed
        self.setup_web2py()
        self.exec_controller()

    def setup_web2py(self):
        if db._uri != TEST_DB:
            raise Exception('Not using testing database')
        self.globals = original_globals.copy()
        self.clear_tables()

    def exec_controller(self):
        if self.CONTROLLER:
            execfile(self.CONTROLLER, self.globals)

    def tearDown(self):
        self.clear_tables()
        self.web2py_logout()
        if self.globals['request']['_vars'] is not None:
            self.globals['request']['_vars'].clear()

    def clear_tables(self):
        # Dropping everything in the CLEAR_TABLES array
        if self.CLEAR_TABLES is None:
            raise NotImplementedError('CLEAR_TABLES must be defined by a subclass of Web2PyTest')
        db = self.globals['db']
        db.rollback()
        for table in self.CLEAR_TABLES:
            db[table].truncate()
        db.commit()

    def web2py_register(self, username, password):
        auth = self.globals['auth']
        test_user = dict(username=username, password=password)
        auth.register_bare(**test_user)
        return self.globals['db'](self.globals['db'].auth_user.username == username).select().first().id

    def web2py_login(self, username, password):
        auth = self.globals['auth']
        test_user = dict(username=username, password=password)
        auth.login_bare(**test_user)

    def web2py_set_role(self, username, role_name, department=None):
        auth = self.globals['auth']
        db = self.globals['db']
        if department:
            role = f'{department} {role_name}'
        else:
            role = role_name
        auth.add_membership(
            group_id=auth.id_group(role), 
            user_id=db(db.auth_user.username == username).select().first().id)

    def web2py_remove_role(self, username, role_name, department=None):
        auth = self.globals['auth']
        db = self.globals['db']
        if department:
            role = f'{department} {role_name}'
        else:
            role = role_name
        auth.del_membership(
            group_id=auth.id_group(role), 
            user_id=db(db.auth_user.username == username).select().first().id)

    def web2py_logout(self):
        auth = self.globals['auth']
        auth.logout(next=None)

class InitModelTest(Web2PyTest):
    USER = ''
    PASSWORD = ''
    DEPARTMENT = ''
    ROLE = ''
    CLEAR_TABLES = [
        'auth_user',
        'auth_membership',
        'po_form',
        'po_note',
        'po_item',
    ]

    def setUp(self):
        self.setup_web2py()
        if self.USER:
            self.web2py_register(self.USER, self.PASSWORD)
            self.web2py_login(self.USER, self.PASSWORD)
        if self.ROLE:
            if self.DEPARTMENT:
                self.web2py_set_role(self.USER, self.ROLE, self.DEPARTMENT)
            else:
                self.web2py_set_role(self.USER, self.ROLE)

    def _helper_createpo(self):
        copy_globals = original_globals.copy()
        _controller = 'applications/po/controllers/createPO.py'
        execfile(_controller, copy_globals)
        exec('po_number=createNewPo()', copy_globals)
        po_number = copy_globals['po_number']
        return po_number

    def _helper_set_po_department(self, po_number, department_name):
        db = self.globals['db']
        department_id = db(db.department.name == department_name).select().first().id
        db(db.po_form.id == po_number).update(
            department = department_id
        )

    def _helper_set_subtotal(self, po_number, subtotal):
        db = self.globals['db']
        db(db.po_form.id == po_number).update(subtotal=subtotal)

    def _helper_mark_approval(self, po_number, approver):
        db = self.globals['db']
        db.po_note.insert(
            po_form = po_number,
            made_by = approver,
            comment = 'Approved',
            po_approval = True
        )
    
    def _helper_add_contract_to_po(self, po_number, contract):
        db = self.globals['db']
        db.po_form_attachments.insert(
            po_form = po_number,
            upload_type = "contract",
            file=contract,
            upload_date=datetime.datetime.now()
        )

    def init_get_failing_rules(self, po_number):
        exec(f"failed_rules = get_failing_po_rules({po_number})", self.globals)
        return self.globals['failed_rules']

    def init_get_users_by_level(self, role, department_id):
        exec(f"users = getUsersByLevelForDepartment('{role}', {department_id})", self.globals)
        return self.globals['users']

    def init_get_approvers_for_po(self, po_number):
        exec(f'approvers = getApproversByPO({po_number})', self.globals)
        return self.globals['approvers']

class Test_Get_Failing_PO_Rules_For_Department_With_All_Approval_Levels(InitModelTest):
    USER = 'submitter'
    PASSWORD = 'password'
    ROLE = 'Submitter'
    DEPARTMENT = 'FCC'

    def test_first_approval_rule(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)

        db = self.globals['db']
        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 0) 
            & (db.po_rule.department_head_approvals == 0) 
            & (db.po_rule.superapprover_approvals == 0) 
            & (db.po_rule.department == department_id)
        ).select(orderby=db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 0)

    def test_second_rule_rule_with_no_approvals(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)

        db = self.globals['db']
        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 1) 
            & (db.po_rule.department_head_approvals == 0) 
            & (db.po_rule.superapprover_approvals == 0) 
            & (db.po_rule.department == department_id)
        ).select(orderby=db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 1)
        self.assertEqual(failing_rules['failed_rules'].first().id, rule.id)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 0)

    def test_second_rule_rule_with_supervisor_approval(self):
        db = self.globals['db']
        supervisor = dict(
            username  ='supervisor',
            password  =self.PASSWORD,
            role      ='Supervisor',
            department=self.DEPARTMENT
        )
        self.web2py_register(supervisor['username'], supervisor['password'])
        self.web2py_set_role(supervisor['username'], supervisor['role'], supervisor['department'])
        
        supervisor_id = db(db.auth_user.username == supervisor['username']).select().first().id

        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_mark_approval(po_number, supervisor_id)

        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 1) 
            & (db.po_rule.department_head_approvals == 0) 
            & (db.po_rule.superapprover_approvals == 0) 
            & (db.po_rule.department == department_id)
        ).select(orderby=db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 0)

    def test_second_rule_rule_with_department_head_approval(self):
        db = self.globals['db']
        department_head = dict(
            username  ='department head',
            password  =self.PASSWORD,
            role      ='Department Head',
            department=self.DEPARTMENT
        )
        self.web2py_register(department_head['username'], department_head['password'])
        self.web2py_set_role(department_head['username'], department_head['role'], department_head['department'])
        department_head_id = db(db.auth_user.username == department_head['username']).select().first().id

        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_mark_approval(po_number, department_head_id)

        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 1) 
            & (db.po_rule.department_head_approvals == 0) 
            & (db.po_rule.superapprover_approvals == 0) 
            & (db.po_rule.department == department_id)
        ).select(orderby=db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 0)

    def test_second_rule_rule_with_superapprover_approval(self):
        db = self.globals['db']
        superapprover = dict(
            username  ='superapprover',
            password  =self.PASSWORD,
            role      ='superapprover'
        )
        self.web2py_register(superapprover['username'], superapprover['password'])
        self.web2py_set_role(superapprover['username'], superapprover['role'])
        
        superapprover_id = db(db.auth_user.username == superapprover['username']).select().first().id

        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_mark_approval(po_number, superapprover_id)

        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 1) 
            & (db.po_rule.department_head_approvals == 0) 
            & (db.po_rule.superapprover_approvals == 0) 
            & (db.po_rule.department == department_id)
        ).select(orderby=db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 1)

    def test_third_rule_rule_with_no_approvals(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)

        db = self.globals['db']
        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 1) 
            & (db.po_rule.department_head_approvals == 1) 
            & (db.po_rule.superapprover_approvals == 0) 
            & (db.po_rule.department == department_id)
        ).select(orderby=db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 1)
        self.assertEqual(failing_rules['failed_rules'].first().id, rule.id)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 0)

    def test_third_rule_rule_with_supervisor_approval(self):
        db = self.globals['db']
        supervisor = dict(
            username  ='supervisor',
            password  =self.PASSWORD,
            role      ='Supervisor',
            department=self.DEPARTMENT
        )
        self.web2py_register(supervisor['username'], supervisor['password'])
        self.web2py_set_role(supervisor['username'], supervisor['role'], supervisor['department'])
        
        supervisor_id = db(db.auth_user.username == supervisor['username']).select().first().id

        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_mark_approval(po_number, supervisor_id)

        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 1) 
            & (db.po_rule.department_head_approvals == 1) 
            & (db.po_rule.superapprover_approvals == 0) 
            & (db.po_rule.department == department_id)
        ).select(orderby=db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 1)
        self.assertEqual(failing_rules['failed_rules'].first().id, rule.id)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 0)

    def test_third_rule_rule_with_department_head_approval(self):
        db = self.globals['db']
        department_head = dict(
            username  ='department head',
            password  =self.PASSWORD,
            role      ='Department Head',
            department=self.DEPARTMENT
        )
        self.web2py_register(department_head['username'], department_head['password'])
        self.web2py_set_role(department_head['username'], department_head['role'], department_head['department'])

        department_head_id = db(db.auth_user.username == department_head['username']).select().first().id

        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_mark_approval(po_number, department_head_id)

        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 1) 
            & (db.po_rule.department_head_approvals == 1) 
            & (db.po_rule.superapprover_approvals == 0) 
            & (db.po_rule.department == department_id)
        ).select(orderby=db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 1)
        self.assertEqual(failing_rules['failed_rules'].first().id, rule.id)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 0)

    def test_third_rule_rule_with_superapprover_approval(self):
        db = self.globals['db']
        superapprover = dict(
            username  ='superapprover',
            password  =self.PASSWORD,
            role      ='superapprover'
        )
        self.web2py_register(superapprover['username'], superapprover['password'])
        self.web2py_set_role(superapprover['username'], superapprover['role'])
        superapprover_id = db(db.auth_user.username == superapprover['username']).select().first().id

        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_mark_approval(po_number, superapprover_id)

        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 1) 
            & (db.po_rule.department_head_approvals == 1) 
            & (db.po_rule.superapprover_approvals == 0) 
            & (db.po_rule.department == department_id)
        ).select(orderby=db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 1)
        self.assertEqual(failing_rules['failed_rules'].first().id, rule.id)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 1)

    def test_third_rule_with_supervisor_and_department_head_approval(self):
        db = self.globals['db']
        supervisor = dict(
            username  ='supervisor',
            password  =self.PASSWORD,
            role      ='Supervisor',
            department=self.DEPARTMENT
        )
        department_head = dict(
            username  ='department head',
            password  =self.PASSWORD,
            role      ='Department Head',
            department=self.DEPARTMENT
        )

        self.web2py_register(supervisor['username'], supervisor['password'])
        self.web2py_register(department_head['username'], department_head['password'])
        self.web2py_set_role(supervisor['username'], supervisor['role'], supervisor['department'])
        self.web2py_set_role(department_head['username'], department_head['role'], department_head['department'])
        
        supervisor_id = db(db.auth_user.username == supervisor['username']).select().first().id
        department_head_id = db(db.auth_user.username == department_head['username']).select().first().id


        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_mark_approval(po_number, supervisor_id)
        self._helper_mark_approval(po_number, department_head_id)

        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 1) 
            & (db.po_rule.department_head_approvals == 1) 
            & (db.po_rule.superapprover_approvals == 0) 
            & (db.po_rule.department == department_id)
        ).select(orderby=db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 2)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 0)

    def test_third_rule_with_department_head_and_superapprover_approval(self):
        db = self.globals['db']
        department_head = dict(
            username  ='department head',
            password  =self.PASSWORD,
            role      ='Department Head',
            department=self.DEPARTMENT
        )
        superapprover = dict(
            username  ='superapprover',
            password  =self.PASSWORD,
            role      ='superapprover'
        )

        self.web2py_register(department_head['username'], department_head['password'])
        self.web2py_register(superapprover['username'], superapprover['password'])
        self.web2py_set_role(department_head['username'], department_head['role'], department_head['department'])
        self.web2py_set_role(superapprover['username'], superapprover['role'])
        
        department_head_id = db(db.auth_user.username == department_head['username']).select().first().id
        superapprover_id = db(db.auth_user.username == superapprover['username']).select().first().id


        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_mark_approval(po_number, department_head_id)
        self._helper_mark_approval(po_number, superapprover_id)

        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 1) 
            & (db.po_rule.department_head_approvals == 1) 
            & (db.po_rule.superapprover_approvals == 0) 
            & (db.po_rule.department == department_id)
        ).select(orderby=db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 2)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 2)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 1)

    def test_third_rule_with_supervisor_and_superapprover_approval(self):
        db = self.globals['db']
        supervisor = dict(
            username  ='supervisor',
            password  =self.PASSWORD,
            role      ='Supervisor',
            department=self.DEPARTMENT
        )
        superapprover = dict(
            username  ='superapprover',
            password  =self.PASSWORD,
            role      ='superapprover'
        )

        self.web2py_register(supervisor['username'], supervisor['password'])
        self.web2py_register(superapprover['username'], superapprover['password'])
        self.web2py_set_role(supervisor['username'], supervisor['role'], supervisor['department'])
        self.web2py_set_role(superapprover['username'], superapprover['role'])
        
        supervisor_id = db(db.auth_user.username == supervisor['username']).select().first().id
        superapprover_id = db(db.auth_user.username == superapprover['username']).select().first().id


        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_mark_approval(po_number, supervisor_id)
        self._helper_mark_approval(po_number, superapprover_id)

        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 1) 
            & (db.po_rule.department_head_approvals == 1) 
            & (db.po_rule.superapprover_approvals == 0) 
            & (db.po_rule.department == department_id)
        ).select(orderby=db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 2)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 1)

    def test_fourth_rule_no_contract_with_no_approvals(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)

        db = self.globals['db']
        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 1) 
            & (db.po_rule.department_head_approvals == 1) 
            & (db.po_rule.superapprover_approvals == 1) 
            & (db.po_rule.department == department_id)
        ).select(orderby=db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 1)
        self.assertEqual(failing_rules['failed_rules'].first().id, rule.id)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 0)

    def test_fourth_rule_no_contract_with_supervisor_approval(self):
        db = self.globals['db']
        supervisor = dict(
            username  ='supervisor',
            password  =self.PASSWORD,
            role      ='Supervisor',
            department=self.DEPARTMENT
        )
        self.web2py_register(supervisor['username'], supervisor['password'])
        self.web2py_set_role(supervisor['username'], supervisor['role'], supervisor['department'])
        
        supervisor_id = db(db.auth_user.username == supervisor['username']).select().first().id

        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_mark_approval(po_number, supervisor_id)

        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 1) 
            & (db.po_rule.department_head_approvals == 1) 
            & (db.po_rule.superapprover_approvals == 1) 
            & (db.po_rule.department == department_id)
        ).select(orderby=db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 1)
        self.assertEqual(failing_rules['failed_rules'].first().id, rule.id)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 0)

    def test_fourth_rule_no_contract_with_department_head_approval(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)

        db = self.globals['db']
        department_head = dict(
            username  ='department head',
            password  =self.PASSWORD,
            role      ='Department Head',
            department=self.DEPARTMENT
        )
        self.web2py_register(department_head['username'], department_head['password'])
        self.web2py_set_role(department_head['username'], department_head['role'], department_head['department'])
        
        department_head_id = db(db.auth_user.username == department_head['username']).select().first().id

        self._helper_mark_approval(po_number, department_head_id)

        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 1) 
            & (db.po_rule.department_head_approvals == 1) 
            & (db.po_rule.superapprover_approvals == 1) 
            & (db.po_rule.department == department_id)
        ).select(orderby=db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 1)
        self.assertEqual(failing_rules['failed_rules'].first().id, rule.id)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 0)

    def test_fourth_rule_no_contract_with_superapprover_approval(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)

        db = self.globals['db']
        superapprover = dict(
            username  ='superapprover',
            password  =self.PASSWORD,
            role      ='superapprover'
        )
        self.web2py_register(superapprover['username'], superapprover['password'])
        self.web2py_set_role(superapprover['username'], superapprover['role'])
        
        superapprover_id = db(db.auth_user.username == superapprover['username']).select().first().id

        self._helper_mark_approval(po_number, superapprover_id)

        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 1) 
            & (db.po_rule.department_head_approvals == 1) 
            & (db.po_rule.superapprover_approvals == 1) 
            & (db.po_rule.department == department_id)
        ).select(orderby=db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 1)
        self.assertEqual(failing_rules['failed_rules'].first().id, rule.id)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 1)

    def test_fourth_rule_no_contract_with_department_head_approval_and_supervisor_approval(self):
        db = self.globals['db']
        supervisor = dict(
            username  ='supervisor',
            password  =self.PASSWORD,
            role      ='Supervisor',
            department=self.DEPARTMENT
        )
        department_head = dict(
            username  ='department head',
            password  =self.PASSWORD,
            role      ='Department Head',
            department=self.DEPARTMENT
        )

        self.web2py_register(supervisor['username'], supervisor['password'])
        self.web2py_register(department_head['username'], department_head['password'])
        self.web2py_set_role(supervisor['username'], supervisor['role'], supervisor['department'])
        self.web2py_set_role(department_head['username'], department_head['role'], department_head['department'])

        supervisor_id = db(db.auth_user.username == supervisor['username']).select().first().id
        department_head_id = db(db.auth_user.username == department_head['username']).select().first().id


        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_mark_approval(po_number, supervisor_id)
        self._helper_mark_approval(po_number, department_head_id)

        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 1) 
            & (db.po_rule.department_head_approvals == 1) 
            & (db.po_rule.superapprover_approvals == 1) 
            & (db.po_rule.department == department_id)
        ).select(orderby=db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 1)
        self.assertEqual(failing_rules['failed_rules'].first().id, rule.id)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 2)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 0)

    def test_fourth_rule_no_contract_with_superapprover_approval_and_department_head_approval(self):
        db = self.globals['db']
        department_head = dict(
            username  ='department head',
            password  =self.PASSWORD,
            role      ='Department Head',
            department=self.DEPARTMENT
        )
        superapprover = dict(
            username  ='superapprover',
            password  =self.PASSWORD,
            role      ='superapprover'
        )

        self.web2py_register(department_head['username'], department_head['password'])
        self.web2py_register(superapprover['username'], superapprover['password'])
        self.web2py_set_role(department_head['username'], department_head['role'], department_head['department'])
        self.web2py_set_role(superapprover['username'], superapprover['role'])
        
        department_head_id = db(db.auth_user.username == department_head['username']).select().first().id
        superapprover_id = db(db.auth_user.username == superapprover['username']).select().first().id


        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_mark_approval(po_number, department_head_id)
        self._helper_mark_approval(po_number, superapprover_id)

        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 1) 
            & (db.po_rule.department_head_approvals == 1) 
            & (db.po_rule.superapprover_approvals == 1) 
            & (db.po_rule.department == department_id)
        ).select(orderby=db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 1)
        self.assertEqual(failing_rules['failed_rules'].first().id, rule.id)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 2)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 2)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 1)

    def test_fourth_rule_no_contract_with_superapprover_approval_and_supervisor_approval(self):
        db = self.globals['db']
        supervisor = dict(
            username  ='supervisor',
            password  =self.PASSWORD,
            role      ='Supervisor',
            department=self.DEPARTMENT
        )
        superapprover = dict(
            username  ='superapprover',
            password  =self.PASSWORD,
            role      ='superapprover'
        )

        self.web2py_register(supervisor['username'], supervisor['password'])
        self.web2py_register(superapprover['username'], superapprover['password'])
        self.web2py_set_role(supervisor['username'], supervisor['role'], supervisor['department'])
        self.web2py_set_role(superapprover['username'], superapprover['role'])
        
        supervisor_id = db(db.auth_user.username == supervisor['username']).select().first().id
        superapprover_id = db(db.auth_user.username == superapprover['username']).select().first().id

        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_mark_approval(po_number, supervisor_id)
        self._helper_mark_approval(po_number, superapprover_id)

        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 1) 
            & (db.po_rule.department_head_approvals == 1) 
            & (db.po_rule.superapprover_approvals == 1) 
            & (db.po_rule.department == department_id)
        ).select(orderby=db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 1)
        self.assertEqual(failing_rules['failed_rules'].first().id, rule.id)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 2)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 1)

    def test_fourth_rule_no_contract_with_superapprover_approval_and_department_head_approval_supervisor_approval(self):
        db = self.globals['db']

        supervisor = dict(
            username  ='supervisor',
            password  =self.PASSWORD,
            role      ='Supervisor',
            department=self.DEPARTMENT
        )
        self.web2py_register(supervisor['username'], supervisor['password'])
        self.web2py_set_role(supervisor['username'], supervisor['role'], supervisor['department'])
        department_head = dict(
            username  ='department head',
            password  =self.PASSWORD,
            role      ='Department Head',
            department=self.DEPARTMENT
        )
        self.web2py_register(department_head['username'], department_head['password'])
        self.web2py_set_role(department_head['username'], department_head['role'], department_head['department'])
        superapprover = dict(
            username  ='superapprover',
            password  =self.PASSWORD,
            role      ='superapprover'
        )
        self.web2py_register(superapprover['username'], superapprover['password'])
        self.web2py_set_role(superapprover['username'], superapprover['role'])

        supervisor_id = db(db.auth_user.username == supervisor['username']).select().first().id
        department_head_id = db(db.auth_user.username == department_head['username']).select().first().id
        superapprover_id = db(db.auth_user.username == superapprover['username']).select().first().id

        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_mark_approval(po_number, supervisor_id)
        self._helper_mark_approval(po_number, department_head_id)
        self._helper_mark_approval(po_number, superapprover_id)

        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 1) 
            & (db.po_rule.department_head_approvals == 1) 
            & (db.po_rule.superapprover_approvals == 1) 
            & (db.po_rule.department == department_id)
        ).select(orderby=db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 1)
        self.assertEqual(failing_rules['failed_rules'].first().id, rule.id)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 3)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 2)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 1)


    def test_fourth_rule_with_contract_with_no_approvals(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_add_contract_to_po(po_number, 'some awesome contract')

        db = self.globals['db']
        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 1) 
            & (db.po_rule.department_head_approvals == 1) 
            & (db.po_rule.superapprover_approvals == 1) 
            & (db.po_rule.department == department_id)
        ).select(orderby=db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 1)
        self.assertEqual(failing_rules['failed_rules'].first().id, rule.id)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 0)

    def test_fourth_rule_with_contract_with_supervisor_approval(self):
        db = self.globals['db']

        supervisor = dict(
            username  ='supervisor',
            password  =self.PASSWORD,
            role      ='Supervisor',
            department=self.DEPARTMENT
        )
        self.web2py_register(supervisor['username'], supervisor['password'])
        self.web2py_set_role(supervisor['username'], supervisor['role'], supervisor['department'])
        supervisor_id = db(db.auth_user.username == supervisor['username']).select().first().id

        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_add_contract_to_po(po_number, 'some awesome contract')
        self._helper_mark_approval(po_number, supervisor_id)

        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 1) 
            & (db.po_rule.department_head_approvals == 1) 
            & (db.po_rule.superapprover_approvals == 1) 
            & (db.po_rule.department == department_id)
        ).select(orderby=db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 1)
        self.assertEqual(failing_rules['failed_rules'].first().id, rule.id)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 0)

    def test_fourth_rule_with_contract_with_department_head_approval(self):
        db = self.globals['db']
        department_head = dict(
            username  ='department head',
            password  =self.PASSWORD,
            role      ='Department Head',
            department=self.DEPARTMENT
        )
        self.web2py_register(department_head['username'], department_head['password'])
        self.web2py_set_role(department_head['username'], department_head['role'], department_head['department'])
        department_head_id = db(db.auth_user.username == department_head['username']).select().first().id

        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_add_contract_to_po(po_number, 'some awesome contract')
        self._helper_mark_approval(po_number, department_head_id)

        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 1) 
            & (db.po_rule.department_head_approvals == 1) 
            & (db.po_rule.superapprover_approvals == 1) 
            & (db.po_rule.department == department_id)
        ).select(orderby=db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 1)
        self.assertEqual(failing_rules['failed_rules'].first().id, rule.id)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 0)

    def test_fourth_rule_with_contract_with_department_head_approval_and_supervisor_approval(self):
        db = self.globals['db']
        supervisor = dict(
            username  ='supervisor',
            password  =self.PASSWORD,
            role      ='Supervisor',
            department=self.DEPARTMENT
        )
        self.web2py_register(supervisor['username'], supervisor['password'])
        self.web2py_set_role(supervisor['username'], supervisor['role'], supervisor['department'])
        department_head = dict(
            username  ='department head',
            password  =self.PASSWORD,
            role      ='Department Head',
            department=self.DEPARTMENT
        )
        self.web2py_register(department_head['username'], department_head['password'])
        self.web2py_set_role(department_head['username'], department_head['role'], department_head['department'])

        supervisor_id = db(db.auth_user.username == supervisor['username']).select().first().id
        department_head_id = db(db.auth_user.username == department_head['username']).select().first().id

        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_add_contract_to_po(po_number, 'some awesome contract')
        self._helper_mark_approval(po_number, supervisor_id)
        self._helper_mark_approval(po_number, department_head_id)

        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 1) 
            & (db.po_rule.department_head_approvals == 1) 
            & (db.po_rule.superapprover_approvals == 1) 
            & (db.po_rule.department == department_id)
        ).select(orderby=db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 1)
        self.assertEqual(failing_rules['failed_rules'].first().id, rule.id)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 2)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 0)

    def test_fourth_rule_with_contract_with_superapprover_approval(self):
        db = self.globals['db']
        superapprover = dict(
            username  ='superapprover',
            password  =self.PASSWORD,
            role      ='superapprover'
        )
        self.web2py_register(superapprover['username'], superapprover['password'])
        self.web2py_set_role(superapprover['username'], superapprover['role'])

        superapprover_id = db(db.auth_user.username == superapprover['username']).select().first().id

        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_add_contract_to_po(po_number, 'some awesome contract')
        self._helper_mark_approval(po_number, superapprover_id)

        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 1) 
            & (db.po_rule.department_head_approvals == 1) 
            & (db.po_rule.superapprover_approvals == 1) 
            & (db.po_rule.department == department_id)
        ).select(orderby=db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 1)
        self.assertEqual(failing_rules['failed_rules'].first().id, rule.id)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 1)

    def test_fourth_rule_with_contract_with_superapprover_approval_and_department_head_approval(self):
        db = self.globals['db']
        department_head = dict(
            username  ='department head',
            password  =self.PASSWORD,
            role      ='Department Head',
            department=self.DEPARTMENT
        )
        self.web2py_register(department_head['username'], department_head['password'])
        self.web2py_set_role(department_head['username'], department_head['role'], department_head['department'])
        superapprover = dict(
            username  ='superapprover',
            password  =self.PASSWORD,
            role      ='superapprover'
        )
        self.web2py_register(superapprover['username'], superapprover['password'])
        self.web2py_set_role(superapprover['username'], superapprover['role'])

        superapprover_id = db(db.auth_user.username == superapprover['username']).select().first().id
        department_head_id = db(db.auth_user.username == department_head['username']).select().first().id

        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_add_contract_to_po(po_number, 'some awesome contract')
        self._helper_mark_approval(po_number, superapprover_id)
        self._helper_mark_approval(po_number, department_head_id)

        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 1) 
            & (db.po_rule.department_head_approvals == 1) 
            & (db.po_rule.superapprover_approvals == 1) 
            & (db.po_rule.department == department_id)
        ).select(orderby=db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 1)
        self.assertEqual(failing_rules['failed_rules'].first().id, rule.id)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 2)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 2)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 1)

    def test_fourth_rule_with_contract_with_superapprover_approval_and_supervisor_approval(self):
        db = self.globals['db']
        supervisor = dict(
            username  ='supervisor',
            password  =self.PASSWORD,
            role      ='Supervisor',
            department=self.DEPARTMENT
        )
        self.web2py_register(supervisor['username'], supervisor['password'])
        self.web2py_set_role(supervisor['username'], supervisor['role'], supervisor['department'])
        superapprover = dict(
            username  ='superapprover',
            password  =self.PASSWORD,
            role      ='superapprover'
        )
        self.web2py_register(superapprover['username'], superapprover['password'])
        self.web2py_set_role(superapprover['username'], superapprover['role'])

        superapprover_id = db(db.auth_user.username == superapprover['username']).select().first().id
        supervisor_id = db(db.auth_user.username == supervisor['username']).select().first().id

        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_add_contract_to_po(po_number, 'some awesome contract')
        self._helper_mark_approval(po_number, superapprover_id)
        self._helper_mark_approval(po_number, supervisor_id)

        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 1) 
            & (db.po_rule.supervisor_approvals == 1) 
            & (db.po_rule.superapprover_approvals == 1) 
            & (db.po_rule.department == department_id)
        ).select(orderby=db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 1)
        self.assertEqual(failing_rules['failed_rules'].first().id, rule.id)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 2)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 1)

    def test_fourth_rule_with_contract_with_superapprover_approval_and_department_head_approval_supervisor_approval(self):
        db = self.globals['db']
        supervisor = dict(
            username  ='supervisor',
            password  =self.PASSWORD,
            role      ='Supervisor',
            department=self.DEPARTMENT
        )
        self.web2py_register(supervisor['username'], supervisor['password'])
        self.web2py_set_role(supervisor['username'], supervisor['role'], supervisor['department'])
        department_head = dict(
            username  ='department head',
            password  =self.PASSWORD,
            role      ='Department Head',
            department=self.DEPARTMENT
        )
        self.web2py_register(department_head['username'], department_head['password'])
        self.web2py_set_role(department_head['username'], department_head['role'], department_head['department'])
        superapprover = dict(
            username  ='superapprover',
            password  =self.PASSWORD,
            role      ='superapprover'
        )
        self.web2py_register(superapprover['username'], superapprover['password'])
        self.web2py_set_role(superapprover['username'], superapprover['role'])

        superapprover_id = db(db.auth_user.username == superapprover['username']).select().first().id
        department_head_id = db(db.auth_user.username == department_head['username']).select().first().id
        supervisor_id = db(db.auth_user.username == supervisor['username']).select().first().id

        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_add_contract_to_po(po_number, 'some awesome contract')

        self._helper_mark_approval(po_number, superapprover_id)
        self._helper_mark_approval(po_number, department_head_id)
        self._helper_mark_approval(po_number, supervisor_id)

        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 1) 
            & (db.po_rule.supervisor_approvals == 1) 
            & (db.po_rule.superapprover_approvals == 1) 
            & (db.po_rule.department == department_id)
        ).select(orderby=db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 3)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 2)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 1)

class Test_Get_Failing_PO_Rules_For_Department_Without_Supervisor(InitModelTest):
    USER = 'submitter'
    PASSWORD = 'password'
    ROLE = 'Submitter'
    DEPARTMENT = 'IT'

    def test_first_approval_rule(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)

        db = self.globals['db']
        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 0) 
            & (db.po_rule.department_head_approvals == 0) 
            & (db.po_rule.superapprover_approvals == 0) 
            & (db.po_rule.department == department_id)
        ).select(orderby=~db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 0)

    def test_second_rule_rule_with_no_approvals(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)

        db = self.globals['db']
        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 0) 
            & (db.po_rule.department_head_approvals == 1) 
            & (db.po_rule.superapprover_approvals == 0) 
            & (db.po_rule.department == department_id)
        ).select(orderby=~db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 1)
        self.assertEqual(failing_rules['failed_rules'].first().id, rule.id)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 0)

    def test_second_rule_rule_with_department_head_approval(self):
        db = self.globals['db']
        department_head = dict(
            username  ='department head',
            password  =self.PASSWORD,
            role      ='Department Head',
            department=self.DEPARTMENT
        )
        self.web2py_register(department_head['username'], department_head['password'])
        self.web2py_set_role(department_head['username'], department_head['role'], department_head['department'])
        department_head_id = db(db.auth_user.username == department_head['username']).select().first().id

        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_mark_approval(po_number, department_head_id)

        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 0) 
            & (db.po_rule.department_head_approvals == 1) 
            & (db.po_rule.superapprover_approvals == 0) 
            & (db.po_rule.department == department_id)
        ).select(orderby=~db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 0)

    def test_second_rule_rule_with_superapprover_approval(self):
        db = self.globals['db']
        superapprover = dict(
            username  ='superapprover',
            password  =self.PASSWORD,
            role      ='superapprover'
        )
        self.web2py_register(superapprover['username'], superapprover['password'])
        self.web2py_set_role(superapprover['username'], superapprover['role'])
        
        superapprover_id = db(db.auth_user.username == superapprover['username']).select().first().id

        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_mark_approval(po_number, superapprover_id)

        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 0) 
            & (db.po_rule.department_head_approvals == 1) 
            & (db.po_rule.superapprover_approvals == 0) 
            & (db.po_rule.department == department_id)
        ).select(orderby=~db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 1)

    def test_third_rule_rule_with_no_approvals(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)

        db = self.globals['db']
        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 0) 
            & (db.po_rule.department_head_approvals == 1) 
            & (db.po_rule.superapprover_approvals == 0) 
            & (db.po_rule.department == department_id)
        ).select(orderby=~db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 1)
        self.assertEqual(failing_rules['failed_rules'].first().id, rule.id)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 0)

    def test_third_rule_rule_with_department_head_approval(self):
        db = self.globals['db']
        department_head = dict(
            username  ='department head',
            password  =self.PASSWORD,
            role      ='Department Head',
            department=self.DEPARTMENT
        )
        self.web2py_register(department_head['username'], department_head['password'])
        self.web2py_set_role(department_head['username'], department_head['role'], department_head['department'])

        department_head_id = db(db.auth_user.username == department_head['username']).select().first().id

        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_mark_approval(po_number, department_head_id)

        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 0) 
            & (db.po_rule.department_head_approvals == 1) 
            & (db.po_rule.superapprover_approvals == 0) 
            & (db.po_rule.department == department_id)
        ).select(orderby=~db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 0)

    def test_third_rule_rule_with_superapprover_approval(self):
        db = self.globals['db']
        superapprover = dict(
            username  ='superapprover',
            password  =self.PASSWORD,
            role      ='superapprover'
        )
        self.web2py_register(superapprover['username'], superapprover['password'])
        self.web2py_set_role(superapprover['username'], superapprover['role'])
        superapprover_id = db(db.auth_user.username == superapprover['username']).select().first().id

        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_mark_approval(po_number, superapprover_id)

        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 0) 
            & (db.po_rule.department_head_approvals == 1) 
            & (db.po_rule.superapprover_approvals == 0) 
            & (db.po_rule.department == department_id)
        ).select(orderby=~db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 1)

    def test_fourth_rule_no_contract_with_no_approvals(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)

        db = self.globals['db']
        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 0) 
            & (db.po_rule.department_head_approvals == 1) 
            & (db.po_rule.superapprover_approvals == 1) 
            & (db.po_rule.department == department_id)
        ).select(orderby=~db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 1)
        self.assertEqual(failing_rules['failed_rules'].first().id, rule.id)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 0)

    def test_fourth_rule_no_contract_with_department_head_approval(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)

        db = self.globals['db']
        department_head = dict(
            username  ='department head',
            password  =self.PASSWORD,
            role      ='Department Head',
            department=self.DEPARTMENT
        )
        self.web2py_register(department_head['username'], department_head['password'])
        self.web2py_set_role(department_head['username'], department_head['role'], department_head['department'])
        
        department_head_id = db(db.auth_user.username == department_head['username']).select().first().id

        self._helper_mark_approval(po_number, department_head_id)

        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 0) 
            & (db.po_rule.department_head_approvals == 1) 
            & (db.po_rule.superapprover_approvals == 1) 
            & (db.po_rule.department == department_id)
        ).select(orderby=~db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 1)
        self.assertEqual(failing_rules['failed_rules'].first().id, rule.id)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 0)

    def test_fourth_rule_no_contract_with_superapprover_approval(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)

        db = self.globals['db']
        superapprover = dict(
            username  ='superapprover',
            password  =self.PASSWORD,
            role      ='superapprover'
        )
        self.web2py_register(superapprover['username'], superapprover['password'])
        self.web2py_set_role(superapprover['username'], superapprover['role'])
        
        superapprover_id = db(db.auth_user.username == superapprover['username']).select().first().id

        self._helper_mark_approval(po_number, superapprover_id)

        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 0) 
            & (db.po_rule.department_head_approvals == 1) 
            & (db.po_rule.superapprover_approvals == 1) 
            & (db.po_rule.department == department_id)
        ).select(orderby=~db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 1)
        self.assertEqual(failing_rules['failed_rules'].first().id, rule.id)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 1)

    def test_fourth_rule_no_contract_with_superapprover_approval_and_department_head_approval(self):
        db = self.globals['db']
        department_head = dict(
            username  ='department head',
            password  =self.PASSWORD,
            role      ='Department Head',
            department=self.DEPARTMENT
        )
        superapprover = dict(
            username  ='superapprover',
            password  =self.PASSWORD,
            role      ='superapprover'
        )

        self.web2py_register(department_head['username'], department_head['password'])
        self.web2py_register(superapprover['username'], superapprover['password'])
        self.web2py_set_role(department_head['username'], department_head['role'], department_head['department'])
        self.web2py_set_role(superapprover['username'], superapprover['role'])
        
        department_head_id = db(db.auth_user.username == department_head['username']).select().first().id
        superapprover_id = db(db.auth_user.username == superapprover['username']).select().first().id


        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_mark_approval(po_number, department_head_id)
        self._helper_mark_approval(po_number, superapprover_id)

        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 0) 
            & (db.po_rule.department_head_approvals == 1) 
            & (db.po_rule.superapprover_approvals == 1) 
            & (db.po_rule.department == department_id)
        ).select(orderby=~db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 1)
        self.assertEqual(failing_rules['failed_rules'].first().id, rule.id)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 2)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 2)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 1)

    def test_fourth_rule_with_contract_with_no_approvals(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_add_contract_to_po(po_number, 'some awesome contract')

        db = self.globals['db']
        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 0) 
            & (db.po_rule.department_head_approvals == 1) 
            & (db.po_rule.superapprover_approvals == 1) 
            & (db.po_rule.department == department_id)
        ).select(orderby=~db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 1)
        self.assertEqual(failing_rules['failed_rules'].first().id, rule.id)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 0)

    def test_fourth_rule_with_contract_with_department_head_approval(self):
        db = self.globals['db']
        department_head = dict(
            username  ='department head',
            password  =self.PASSWORD,
            role      ='Department Head',
            department=self.DEPARTMENT
        )
        self.web2py_register(department_head['username'], department_head['password'])
        self.web2py_set_role(department_head['username'], department_head['role'], department_head['department'])
        department_head_id = db(db.auth_user.username == department_head['username']).select().first().id

        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_add_contract_to_po(po_number, 'some awesome contract')
        self._helper_mark_approval(po_number, department_head_id)

        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 0) 
            & (db.po_rule.department_head_approvals == 1) 
            & (db.po_rule.superapprover_approvals == 1) 
            & (db.po_rule.department == department_id)
        ).select(orderby=~db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 1)
        self.assertEqual(failing_rules['failed_rules'].first().id, rule.id)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 0)

    def test_fourth_rule_with_contract_with_superapprover_approval(self):
        db = self.globals['db']
        superapprover = dict(
            username  ='superapprover',
            password  =self.PASSWORD,
            role      ='superapprover'
        )
        self.web2py_register(superapprover['username'], superapprover['password'])
        self.web2py_set_role(superapprover['username'], superapprover['role'])

        superapprover_id = db(db.auth_user.username == superapprover['username']).select().first().id

        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_add_contract_to_po(po_number, 'some awesome contract')
        self._helper_mark_approval(po_number, superapprover_id)

        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 0) 
            & (db.po_rule.department_head_approvals == 1) 
            & (db.po_rule.superapprover_approvals == 1) 
            & (db.po_rule.department == department_id)
        ).select(orderby=~db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 1)
        self.assertEqual(failing_rules['failed_rules'].first().id, rule.id)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 1)

    def test_fourth_rule_with_contract_with_superapprover_approval_and_department_head_approval(self):
        db = self.globals['db']
        department_head = dict(
            username  ='department head',
            password  =self.PASSWORD,
            role      ='Department Head',
            department=self.DEPARTMENT
        )
        self.web2py_register(department_head['username'], department_head['password'])
        self.web2py_set_role(department_head['username'], department_head['role'], department_head['department'])
        superapprover = dict(
            username  ='superapprover',
            password  =self.PASSWORD,
            role      ='superapprover'
        )
        self.web2py_register(superapprover['username'], superapprover['password'])
        self.web2py_set_role(superapprover['username'], superapprover['role'])

        superapprover_id = db(db.auth_user.username == superapprover['username']).select().first().id
        department_head_id = db(db.auth_user.username == department_head['username']).select().first().id

        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_add_contract_to_po(po_number, 'some awesome contract')
        self._helper_mark_approval(po_number, superapprover_id)
        self._helper_mark_approval(po_number, department_head_id)

        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 0) 
            & (db.po_rule.department_head_approvals == 1) 
            & (db.po_rule.superapprover_approvals == 1) 
            & (db.po_rule.department == department_id)
        ).select(orderby=~db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 2)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 2)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 1)

class Test_Get_Failing_PO_Rules_For_Department_Without_Supervisor_And_Department_Head(InitModelTest):
    USER = 'submitter'
    PASSWORD = 'password'
    ROLE = 'Submitter'
    DEPARTMENT = 'Construction'

    def test_first_approval_rule(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)

        db = self.globals['db']
        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 0) 
            & (db.po_rule.department_head_approvals == 0) 
            & (db.po_rule.superapprover_approvals == 0) 
            & (db.po_rule.department == department_id)
        ).select(orderby=db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 0)

    def test_second_rule_rule_with_no_approvals(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)

        db = self.globals['db']
        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 0) 
            & (db.po_rule.department_head_approvals == 0) 
            & (db.po_rule.superapprover_approvals == 1) 
            & (db.po_rule.department == department_id)
        ).select(orderby=db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 1)
        self.assertEqual(failing_rules['failed_rules'].first().id, rule.id)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 0)

    def test_second_rule_rule_with_superapprover_approval(self):
        db = self.globals['db']
        superapprover = dict(
            username  ='superapprover',
            password  =self.PASSWORD,
            role      ='superapprover'
        )
        self.web2py_register(superapprover['username'], superapprover['password'])
        self.web2py_set_role(superapprover['username'], superapprover['role'])
        
        superapprover_id = db(db.auth_user.username == superapprover['username']).select().first().id

        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_mark_approval(po_number, superapprover_id)

        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 0) 
            & (db.po_rule.department_head_approvals == 0) 
            & (db.po_rule.superapprover_approvals == 1) 
            & (db.po_rule.department == department_id)
        ).select(orderby=db.po_rule.min_subtotal).first()
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 1)

    def test_third_rule_rule_with_no_approvals(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)

        db = self.globals['db']
        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 0) 
            & (db.po_rule.department_head_approvals == 0) 
            & (db.po_rule.superapprover_approvals == 1) 
            & (db.po_rule.department == department_id)
        ).select(orderby=db.po_rule.min_subtotal)[1]
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 1)
        self.assertEqual(failing_rules['failed_rules'].first().id, rule.id)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 0)

    def test_third_rule_rule_with_superapprover_approval(self):
        db = self.globals['db']
        superapprover = dict(
            username  ='superapprover',
            password  =self.PASSWORD,
            role      ='superapprover'
        )
        self.web2py_register(superapprover['username'], superapprover['password'])
        self.web2py_set_role(superapprover['username'], superapprover['role'])
        superapprover_id = db(db.auth_user.username == superapprover['username']).select().first().id

        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_mark_approval(po_number, superapprover_id)

        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 0) 
            & (db.po_rule.department_head_approvals == 0) 
            & (db.po_rule.superapprover_approvals == 1) 
            & (db.po_rule.department == department_id)
        ).select(orderby=db.po_rule.min_subtotal)[1]
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 1)

    def test_fourth_rule_no_contract_with_no_approvals(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)

        db = self.globals['db']
        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 0) 
            & (db.po_rule.department_head_approvals == 0) 
            & (db.po_rule.superapprover_approvals == 1) 
            & (db.po_rule.department == department_id)
        ).select(orderby=db.po_rule.min_subtotal)[2]
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 1)
        self.assertEqual(failing_rules['failed_rules'].first().id, rule.id)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 0)

    def test_fourth_rule_no_contract_with_superapprover_approval(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)

        db = self.globals['db']
        superapprover = dict(
            username  ='superapprover',
            password  =self.PASSWORD,
            role      ='superapprover'
        )
        self.web2py_register(superapprover['username'], superapprover['password'])
        self.web2py_set_role(superapprover['username'], superapprover['role'])
        
        superapprover_id = db(db.auth_user.username == superapprover['username']).select().first().id

        self._helper_mark_approval(po_number, superapprover_id)

        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 0) 
            & (db.po_rule.department_head_approvals == 0) 
            & (db.po_rule.superapprover_approvals == 1) 
            & (db.po_rule.department == department_id)
        ).select(orderby=db.po_rule.min_subtotal)[2]
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 1)
        self.assertEqual(failing_rules['failed_rules'].first().id, rule.id)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 1)

    def test_fourth_rule_with_contract_with_no_approvals(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_add_contract_to_po(po_number, 'some awesome contract')

        db = self.globals['db']
        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 0) 
            & (db.po_rule.department_head_approvals == 0) 
            & (db.po_rule.superapprover_approvals == 1) 
            & (db.po_rule.department == department_id)
        ).select(orderby=db.po_rule.min_subtotal)[2]
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 1)
        self.assertEqual(failing_rules['failed_rules'].first().id, rule.id)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 0)

    def test_fourth_rule_with_contract_with_superapprover_approval(self):
        db = self.globals['db']
        superapprover = dict(
            username  ='superapprover',
            password  =self.PASSWORD,
            role      ='superapprover'
        )
        self.web2py_register(superapprover['username'], superapprover['password'])
        self.web2py_set_role(superapprover['username'], superapprover['role'])

        superapprover_id = db(db.auth_user.username == superapprover['username']).select().first().id

        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_add_contract_to_po(po_number, 'some awesome contract')
        self._helper_mark_approval(po_number, superapprover_id)

        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        rule = db(
            (db.po_rule.supervisor_approvals == 0) 
            & (db.po_rule.department_head_approvals == 0) 
            & (db.po_rule.superapprover_approvals == 1) 
            & (db.po_rule.department == department_id)
        ).select(orderby=db.po_rule.min_subtotal)[2]
        db.po_rule_map.insert(
            po_form = po_number,
            po_rule = rule.id
        )
        failing_rules = self.init_get_failing_rules(po_number)
        self.assertEqual(len(failing_rules['failed_rules']), 0)
        self.assertEqual(len(failing_rules['approver_counts']['supervisor']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['department_head']), 1)
        self.assertEqual(len(failing_rules['approver_counts']['superapprover']), 1)

class Test_Get_Users_By_Level_For_Department(InitModelTest):
    USER = 'user'
    PASSWORD = 'password'

    def test_get_users_for_each_department(self):
        db = self.globals['db']
        previous_department = None
        user_id = db(db.auth_user.username == self.USER).select().first().id
        for department in db(db.department.id >= 0).select():
            if previous_department:
                self.web2py_remove_role(self.USER, 'Submitter', previous_department.name)

            self.web2py_set_role(self.USER, 'Submitter', department.name)
            users_in_department = self.init_get_users_by_level('Submitter', department.id)
            self.assertEqual(len(users_in_department), 1)
            self.assertEqual(users_in_department[0], user_id)
            
            if previous_department:
                self.web2py_remove_role(self.USER, 'Supervisor', previous_department.name)

            self.web2py_set_role(self.USER, 'Supervisor', department.name)
            users_in_department = self.init_get_users_by_level('Supervisor', department.id)
            self.assertEqual(len(users_in_department), 2)
            self.assertEqual(users_in_department[0], user_id)
            self.assertEqual(users_in_department[1], user_id)

            if previous_department:
                self.web2py_remove_role(self.USER, 'Department Head', previous_department.name)
            self.web2py_set_role(self.USER, 'Department Head', department.name)
            users_in_department = self.init_get_users_by_level('Department Head', department.id)
            self.assertEqual(len(users_in_department), 3)
            self.assertEqual(users_in_department[0], user_id)
            self.assertEqual(users_in_department[1], user_id)
            self.assertEqual(users_in_department[2], user_id)

            previous_department = department

class Test_Get_Approvers_By_PO(InitModelTest):
    DEPARTMENT = 'FCC'

    def setUp(self):
        super().setUp()
        self.SUBMITTER = dict(
            username  ='submitter',
            password  ='password',
            role      ='Submitter',
            department=self.DEPARTMENT,
            id        =None
        )
        self.SUPERVISOR = dict(
            username  ='supervisor',
            password  =self.PASSWORD,
            role      ='Supervisor',
            department=self.DEPARTMENT,
            id        =None
        )
        self.DEPARTMENT_HEAD = dict(
            username  ='department head',
            password  =self.PASSWORD,
            role      ='Department Head',
            department=self.DEPARTMENT,
            id        =None
        )
        self.SUPERAPPROVER = dict(
            username  ='superapprover',
            password  =self.PASSWORD,
            role      ='superapprover',
            id        =None
        )
        self.SUBMITTER['id'] = self.web2py_register(self.SUBMITTER['username'], self.SUBMITTER['password'])
        self.SUPERVISOR['id'] = self.web2py_register(self.SUPERVISOR['username'], self.SUPERVISOR['password'])
        self.DEPARTMENT_HEAD['id'] = self.web2py_register(self.DEPARTMENT_HEAD['username'], self.DEPARTMENT_HEAD['password'])
        self.SUPERAPPROVER['id'] = self.web2py_register(self.SUPERAPPROVER['username'], self.SUPERAPPROVER['password'])
        self.web2py_set_role(self.SUBMITTER['username'], self.SUBMITTER['role'], self.SUBMITTER['department'])
        self.web2py_set_role(self.SUPERVISOR['username'], self.SUPERVISOR['role'], self.SUPERVISOR['department'])
        self.web2py_set_role(self.DEPARTMENT_HEAD['username'], self.DEPARTMENT_HEAD['role'], self.DEPARTMENT_HEAD['department'])
        self.web2py_set_role(self.SUPERAPPROVER['username'], self.SUPERAPPROVER['role'])

        self.web2py_login(self.SUBMITTER['username'], self.SUBMITTER['password'])

    def test_po_with_no_approvals(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        approvers = self.init_get_approvers_for_po(po_number)
        self.assertEqual(len(approvers['supervisor']), 0)
        self.assertEqual(len(approvers['department_head']), 0)
        self.assertEqual(len(approvers['superapprover']), 0)

    def test_po_with_supervisor_approval(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_mark_approval(po_number, self.SUPERVISOR['id'])
        approvers = self.init_get_approvers_for_po(po_number)
        self.assertEqual(len(approvers['supervisor']), 1)
        self.assertIn(self.SUPERVISOR['id'], approvers['supervisor'])
        self.assertEqual(len(approvers['department_head']), 0)
        self.assertEqual(len(approvers['superapprover']), 0)

    def test_po_with_department_head_approval(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_mark_approval(po_number, self.DEPARTMENT_HEAD['id'])
        approvers = self.init_get_approvers_for_po(po_number)
        self.assertEqual(len(approvers['supervisor']), 1)
        self.assertIn(self.DEPARTMENT_HEAD['id'], approvers['supervisor'])
        self.assertEqual(len(approvers['department_head']), 1)
        self.assertIn(self.DEPARTMENT_HEAD['id'], approvers['department_head'])
        self.assertEqual(len(approvers['superapprover']), 0)

    def test_po_with_superapprover_approval(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_mark_approval(po_number, self.SUPERAPPROVER['id'])
        approvers = self.init_get_approvers_for_po(po_number)
        self.assertEqual(len(approvers['supervisor']), 1)
        self.assertEqual(len(approvers['department_head']), 1)
        self.assertEqual(len(approvers['superapprover']), 1)
        self.assertIn(self.SUPERAPPROVER['id'], approvers['supervisor'])
        self.assertIn(self.SUPERAPPROVER['id'], approvers['department_head'])
        self.assertIn(self.SUPERAPPROVER['id'], approvers['superapprover'])

    def test_po_with_supervisor_approval_and_department_head_approval(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_mark_approval(po_number, self.SUPERVISOR['id'])
        self._helper_mark_approval(po_number, self.DEPARTMENT_HEAD['id'])
        approvers = self.init_get_approvers_for_po(po_number)

        self.assertEqual(len(approvers['supervisor']), 2)
        self.assertEqual(len(approvers['department_head']), 1)
        self.assertEqual(len(approvers['superapprover']), 0)
        self.assertIn(self.SUPERVISOR['id'], approvers['supervisor'])
        self.assertIn(self.DEPARTMENT_HEAD['id'], approvers['supervisor'])
        self.assertIn(self.DEPARTMENT_HEAD['id'], approvers['department_head'])

    def test_po_with_department_head_approval_and_superapprover_approval(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_mark_approval(po_number, self.DEPARTMENT_HEAD['id'])
        self._helper_mark_approval(po_number, self.SUPERAPPROVER['id'])
        approvers = self.init_get_approvers_for_po(po_number)

        self.assertEqual(len(approvers['supervisor']), 2)
        self.assertEqual(len(approvers['department_head']), 2)
        self.assertEqual(len(approvers['superapprover']), 1)
        self.assertIn(self.DEPARTMENT_HEAD['id'], approvers['supervisor'])
        self.assertIn(self.DEPARTMENT_HEAD['id'], approvers['department_head'])
        self.assertIn(self.SUPERAPPROVER['id'], approvers['supervisor'])
        self.assertIn(self.SUPERAPPROVER['id'], approvers['department_head'])
        self.assertIn(self.SUPERAPPROVER['id'], approvers['superapprover'])

    def test_po_with_supervisor_approval_and_superapprover_approval(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_mark_approval(po_number, self.SUPERVISOR['id'])
        self._helper_mark_approval(po_number, self.SUPERAPPROVER['id'])
        approvers = self.init_get_approvers_for_po(po_number)

        self.assertEqual(len(approvers['supervisor']), 2)
        self.assertEqual(len(approvers['department_head']), 1)
        self.assertEqual(len(approvers['superapprover']), 1)
        self.assertIn(self.SUPERVISOR['id'], approvers['supervisor'])
        self.assertIn(self.SUPERAPPROVER['id'], approvers['supervisor'])
        self.assertIn(self.SUPERAPPROVER['id'], approvers['department_head'])
        self.assertIn(self.SUPERAPPROVER['id'], approvers['superapprover'])

    def test_po_with_supervisor_approval_and_department_head_approval_and_superapprover_approval(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_mark_approval(po_number, self.SUPERVISOR['id'])
        self._helper_mark_approval(po_number, self.DEPARTMENT_HEAD['id'])
        self._helper_mark_approval(po_number, self.SUPERAPPROVER['id'])
        approvers = self.init_get_approvers_for_po(po_number)

        self.assertEqual(len(approvers['supervisor']), 3)
        self.assertEqual(len(approvers['department_head']), 2)
        self.assertEqual(len(approvers['superapprover']), 1)
        self.assertIn(self.SUPERVISOR['id'], approvers['supervisor'])
        self.assertIn(self.DEPARTMENT_HEAD['id'], approvers['supervisor'])
        self.assertIn(self.DEPARTMENT_HEAD['id'], approvers['department_head'])
        self.assertIn(self.SUPERAPPROVER['id'], approvers['supervisor'])
        self.assertIn(self.SUPERAPPROVER['id'], approvers['department_head'])
        self.assertIn(self.SUPERAPPROVER['id'], approvers['superapprover'])

# get_failing_po_rules_all_approval_levels = unittest.TestSuite()
# get_failing_po_rules_all_approval_levels.addTests([
#     unittest.makeSuite(Test_Get_Failing_PO_Rules_For_Department_With_All_Approval_Levels)
# ])

# get_failing_po_rules_without_supervisor = unittest.TestSuite()
# get_failing_po_rules_without_supervisor.addTests([
#     unittest.makeSuite(Test_Get_Failing_PO_Rules_For_Department_Without_Supervisor)
# ])

# get_failing_po_rules_without_supervisor_and_department_head = unittest.TestSuite()
# get_failing_po_rules_without_supervisor_and_department_head.addTests([
#     unittest.makeSuite(Test_Get_Failing_PO_Rules_For_Department_Without_Supervisor_And_Department_Head)
# ])

# get_users_by_level_for_department = unittest.TestSuite()
# get_users_by_level_for_department.addTests([
#     unittest.makeSuite(Test_Get_Users_By_Level_For_Department)
# ])

# get_approvers_for_po = unittest.TestSuite()
# get_approvers_for_po.addTests([
#     unittest.makeSuite(Test_Get_Approvers_By_PO)
# ])

tests = [
    Test_Get_Failing_PO_Rules_For_Department_With_All_Approval_Levels('test_first_approval_rule'),
    Test_Get_Failing_PO_Rules_For_Department_With_All_Approval_Levels('test_second_rule_rule_with_no_approvals'),
    Test_Get_Failing_PO_Rules_For_Department_With_All_Approval_Levels('test_second_rule_rule_with_supervisor_approval'),
    Test_Get_Failing_PO_Rules_For_Department_With_All_Approval_Levels('test_second_rule_rule_with_department_head_approval'),
    Test_Get_Failing_PO_Rules_For_Department_With_All_Approval_Levels('test_second_rule_rule_with_superapprover_approval'),
    Test_Get_Failing_PO_Rules_For_Department_With_All_Approval_Levels('test_third_rule_rule_with_no_approvals'),
    Test_Get_Failing_PO_Rules_For_Department_With_All_Approval_Levels('test_third_rule_rule_with_supervisor_approval'),
    Test_Get_Failing_PO_Rules_For_Department_With_All_Approval_Levels('test_third_rule_rule_with_department_head_approval'),
    Test_Get_Failing_PO_Rules_For_Department_With_All_Approval_Levels('test_third_rule_rule_with_superapprover_approval'),
    Test_Get_Failing_PO_Rules_For_Department_With_All_Approval_Levels('test_third_rule_with_supervisor_and_department_head_approval'),
    Test_Get_Failing_PO_Rules_For_Department_With_All_Approval_Levels('test_third_rule_with_department_head_and_superapprover_approval'),
    Test_Get_Failing_PO_Rules_For_Department_With_All_Approval_Levels('test_third_rule_with_supervisor_and_superapprover_approval'),
    Test_Get_Failing_PO_Rules_For_Department_With_All_Approval_Levels('test_fourth_rule_no_contract_with_no_approvals'),
    Test_Get_Failing_PO_Rules_For_Department_With_All_Approval_Levels('test_fourth_rule_no_contract_with_supervisor_approval'),
    Test_Get_Failing_PO_Rules_For_Department_With_All_Approval_Levels('test_fourth_rule_no_contract_with_department_head_approval'),
    Test_Get_Failing_PO_Rules_For_Department_With_All_Approval_Levels('test_fourth_rule_no_contract_with_superapprover_approval'),
    Test_Get_Failing_PO_Rules_For_Department_With_All_Approval_Levels('test_fourth_rule_no_contract_with_department_head_approval_and_supervisor_approval'),
    Test_Get_Failing_PO_Rules_For_Department_With_All_Approval_Levels('test_fourth_rule_no_contract_with_superapprover_approval_and_department_head_approval'),
    Test_Get_Failing_PO_Rules_For_Department_With_All_Approval_Levels('test_fourth_rule_no_contract_with_superapprover_approval_and_supervisor_approval'),
    Test_Get_Failing_PO_Rules_For_Department_With_All_Approval_Levels('test_fourth_rule_no_contract_with_superapprover_approval_and_department_head_approval_supervisor_approval'),
    Test_Get_Failing_PO_Rules_For_Department_With_All_Approval_Levels('test_fourth_rule_with_contract_with_no_approvals'),
    Test_Get_Failing_PO_Rules_For_Department_With_All_Approval_Levels('test_fourth_rule_with_contract_with_supervisor_approval'),
    Test_Get_Failing_PO_Rules_For_Department_With_All_Approval_Levels('test_fourth_rule_with_contract_with_department_head_approval'),
    Test_Get_Failing_PO_Rules_For_Department_With_All_Approval_Levels('test_fourth_rule_with_contract_with_department_head_approval_and_supervisor_approval'),
    Test_Get_Failing_PO_Rules_For_Department_With_All_Approval_Levels('test_fourth_rule_with_contract_with_superapprover_approval'),
    Test_Get_Failing_PO_Rules_For_Department_With_All_Approval_Levels('test_fourth_rule_with_contract_with_superapprover_approval_and_department_head_approval'),
    Test_Get_Failing_PO_Rules_For_Department_With_All_Approval_Levels('test_fourth_rule_with_contract_with_superapprover_approval_and_supervisor_approval'),
    Test_Get_Failing_PO_Rules_For_Department_With_All_Approval_Levels('test_fourth_rule_with_contract_with_superapprover_approval_and_department_head_approval_supervisor_approval'),
    Test_Get_Failing_PO_Rules_For_Department_Without_Supervisor('test_first_approval_rule'),
    Test_Get_Failing_PO_Rules_For_Department_Without_Supervisor('test_second_rule_rule_with_no_approvals'),
    Test_Get_Failing_PO_Rules_For_Department_Without_Supervisor('test_second_rule_rule_with_department_head_approval'),
    Test_Get_Failing_PO_Rules_For_Department_Without_Supervisor('test_second_rule_rule_with_superapprover_approval'),
    Test_Get_Failing_PO_Rules_For_Department_Without_Supervisor('test_third_rule_rule_with_no_approvals'),
    Test_Get_Failing_PO_Rules_For_Department_Without_Supervisor('test_third_rule_rule_with_department_head_approval'),
    Test_Get_Failing_PO_Rules_For_Department_Without_Supervisor('test_third_rule_rule_with_superapprover_approval'),
    Test_Get_Failing_PO_Rules_For_Department_Without_Supervisor('test_fourth_rule_no_contract_with_no_approvals'),
    Test_Get_Failing_PO_Rules_For_Department_Without_Supervisor('test_fourth_rule_no_contract_with_department_head_approval'),
    Test_Get_Failing_PO_Rules_For_Department_Without_Supervisor('test_fourth_rule_no_contract_with_superapprover_approval'),
    Test_Get_Failing_PO_Rules_For_Department_Without_Supervisor('test_fourth_rule_no_contract_with_superapprover_approval_and_department_head_approval'),
    Test_Get_Failing_PO_Rules_For_Department_Without_Supervisor('test_fourth_rule_with_contract_with_no_approvals'),
    Test_Get_Failing_PO_Rules_For_Department_Without_Supervisor('test_fourth_rule_with_contract_with_department_head_approval'),
    Test_Get_Failing_PO_Rules_For_Department_Without_Supervisor('test_fourth_rule_with_contract_with_superapprover_approval'),
    Test_Get_Failing_PO_Rules_For_Department_Without_Supervisor('test_fourth_rule_with_contract_with_superapprover_approval_and_department_head_approval'),
    Test_Get_Failing_PO_Rules_For_Department_Without_Supervisor_And_Department_Head('test_first_approval_rule'),
    Test_Get_Failing_PO_Rules_For_Department_Without_Supervisor_And_Department_Head('test_second_rule_rule_with_no_approvals'),
    Test_Get_Failing_PO_Rules_For_Department_Without_Supervisor_And_Department_Head('test_second_rule_rule_with_superapprover_approval'),
    Test_Get_Failing_PO_Rules_For_Department_Without_Supervisor_And_Department_Head('test_third_rule_rule_with_no_approvals'),
    Test_Get_Failing_PO_Rules_For_Department_Without_Supervisor_And_Department_Head('test_third_rule_rule_with_superapprover_approval'),
    Test_Get_Failing_PO_Rules_For_Department_Without_Supervisor_And_Department_Head('test_fourth_rule_no_contract_with_no_approvals'),
    Test_Get_Failing_PO_Rules_For_Department_Without_Supervisor_And_Department_Head('test_fourth_rule_no_contract_with_superapprover_approval'),
    Test_Get_Failing_PO_Rules_For_Department_Without_Supervisor_And_Department_Head('test_fourth_rule_with_contract_with_no_approvals'),
    Test_Get_Failing_PO_Rules_For_Department_Without_Supervisor_And_Department_Head('test_fourth_rule_with_contract_with_superapprover_approval'),
    Test_Get_Users_By_Level_For_Department('test_get_users_for_each_department'),
    Test_Get_Approvers_By_PO('test_po_with_no_approvals'),
    Test_Get_Approvers_By_PO('test_po_with_supervisor_approval'),
    Test_Get_Approvers_By_PO('test_po_with_department_head_approval'),
    Test_Get_Approvers_By_PO('test_po_with_superapprover_approval'),
    Test_Get_Approvers_By_PO('test_po_with_supervisor_approval_and_department_head_approval'),
    Test_Get_Approvers_By_PO('test_po_with_department_head_approval_and_superapprover_approval'),
    Test_Get_Approvers_By_PO('test_po_with_supervisor_approval_and_superapprover_approval'),
    Test_Get_Approvers_By_PO('test_po_with_supervisor_approval_and_department_head_approval_and_superapprover_approval'),
]

suite = unittest.TestSuite()
[suite.addTest(test) for test in tests]

unittest.TextTestRunner(verbosity=2).run(suite)