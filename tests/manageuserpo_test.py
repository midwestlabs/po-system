#!/usr/bin/env python

import sys
import os.path

GLUON_PATH = '/opt/web2py'
sys.path.insert(1, os.path.realpath(GLUON_PATH))

from gluon.dal import DAL
from gluon.shell import exec_environment, execfile
from gluon.globals import Request, Response, Session
from gluon import current
from gluon.storage import Storage
from gluon.http import HTTP

original_globals = dict(globals())

import unittest
import random

TEST_DB = 'sqlite://testing.sqlite'

class Web2PyTest(unittest.TestCase):
    # Parent Class
    """
        All tests run against the PO system should inherit this web2pytest class
    """
    CLEAR_TABLES = None
    CONTROLLER = None

    def setUp(self):
        # Probably should have something setup to copy data from one database to another, only if needed
        self.setup_web2py()
        self.exec_controller()

    def setup_web2py(self):
        if db._uri != TEST_DB:
            raise Exception('Not using testing database')
        if self.CONTROLLER is None:
            raise NotImplementedError('CONTROLLER must be defined by a subclass of Web2PyTest')
        self.globals = original_globals.copy()
        self.clear_tables()

    def exec_controller(self):
        execfile(self.CONTROLLER, self.globals)

    def tearDown(self):
        self.clear_tables()
        self.web2py_logout()
        if self.globals['request']['_vars'] is not None:
            self.globals['request']['_vars'].clear()

    def clear_tables(self):
        # Dropping everything in the CLEAR_TABLES array
        if self.CLEAR_TABLES is None:
            raise NotImplementedError('CLEAR_TABLES must be defined by a subclass of Web2PyTest')
        db = self.globals['db']
        db.rollback()
        for table in self.CLEAR_TABLES:
            db[table].truncate()
        db.commit()

    def web2py_register(self, username, password):
        auth = self.globals['auth']
        test_user = dict(username=username, password=password)
        auth.register_bare(**test_user)

    def web2py_login(self, username, password):
        auth = self.globals['auth']
        test_user = dict(username=username, password=password)
        auth.login_bare(**test_user)

    def web2py_set_role(self, username, role_name, department=None):
        auth = self.globals['auth']
        db = self.globals['db']
        if department:
            role = f'{department} {role_name}'
        else:
            role = role_name
        auth.add_membership(
            group_id=auth.id_group(role), 
            user_id=db(db.auth_user.username == username).select().first().id)

    def web2py_remove_role(self, username, role_name, department=None):
        auth = self.globals['auth']
        db = self.globals['db']
        if department:
            role = f'{department} {role_name}'
        else:
            role = role_name
        auth.del_membership(
            group_id=auth.id_group(role), 
            user_id=db(db.auth_user.username == username).select().first().id)

    def web2py_logout(self):
        auth = self.globals['auth']
        auth.logout(next=None)

class ManageUserPOControllerTest(Web2PyTest):
    CLEAR_TABLES = [
        'auth_user',
        'auth_membership',
        'po_form',
        'po_note',
        'po_item',
        'po_rule_map',
        'vendor'
    ]

    CONTROLLER = 'applications/po/controllers/manageUserPO.py'

    USER = None
    PASSWORD = None
    ROLE = None
    DEPARTMENT = None
    AUTO_APPROVER_ID = None
    
    def setUp(self):
        self.setup_web2py()

        auto_approver_user = 'Auto Approver'
        auto_approver_password = 'password'
        self.web2py_register(auto_approver_user, auto_approver_password)
        
        db = self.globals['db']
        self.AUTO_APPROVER_ID = db(db.auth_user.username == auto_approver_user).select().first().id

        if self.USER:
            user = dict(username=self.USER, password=self.PASSWORD)

            self.web2py_register(**user)
            self.web2py_login(**user)
            if self.ROLE:
                self.web2py_set_role(user['username'], self.ROLE, self.DEPARTMENT)

        self.exec_controller()

    def manageuserpo_check_po_number(self, po_number, redirect_link = None):
        self.globals['request']['_vars']['po_number'] = po_number
        exec(f'checkPoNumber(redirectLinkOnError={redirect_link})', self.globals)

    def manageuserpo_is_po_editable(self, po_number):
        exec(f'editable=isPoEditable(po_number={po_number})', self.globals)
        return self.globals['editable']

    def manageuserpo_get_po_department(self, po_number):
        exec(f'department=getPoDepartment(po_number={po_number})', self.globals)
        return self.globals['department']

    def manageuserpo_process_delete_po(self, po_number):
        self.globals['request']['_vars']['po_number'] = po_number
        exec(f'processDeletePO()', self.globals)

    def manageuserpo_process_approve_po(self, po_number, approver=None):
        self.globals['request']['_vars']['po_number'] = po_number
        if approver:
            self.globals['request']['_vars']['approver'] = approver
        exec(f'processApprovePO()', self.globals)

    def manageuserpo_process_reject_po(self, po_number):
        self.globals['request']['_vars']['po_number'] = po_number
        exec(f'processRejectPO()', self.globals)

    def manageuserpo_process_order_po(self, po_number):
        self.globals['request']['_vars']['po_number'] = po_number
        exec(f'processOrderPO()', self.globals)

    def manageuserpo_process_close_po(self, po_number):
        self.globals['request']['_vars']['po_number'] = po_number
        exec(f'processClosePO()', self.globals)

    def manageuserpo_validate_submitted_po(self, po_number):
        exec(f'error_message = validateSubmittedPO({po_number})', self.globals)
        return self.globals['error_message']

    def manageuserpo_clone_po(self, po_number):
        pass

    def manageuserpo_map_submitted_po(self, po_number):
        exec(f'map_response = mapSubmittedPO({po_number})', self.globals)
        return self.globals['map_response']

    def manageuserpo_submit_po(self, po_number):
        self.globals['request']['_vars']['po_number'] = po_number
        exec('processSubmittedPO()', self.globals)

    def _helper_createpo(self):
        copy_globals = original_globals.copy()
        _controller = 'applications/po/controllers/createPO.py'
        execfile(_controller, copy_globals)
        exec('po_number=createNewPo()', copy_globals)
        po_number = copy_globals['po_number']
        return po_number
    
    def _helper_add_item(self, po_number, catalog, description, quantity, price_per_unit):
        db = self.globals['db']
        id = db.po_item.insert(
            po_form=po_number,
            catalog=catalog,
            description=description,
            quantity=quantity,
            price_per_unit=price_per_unit,
            extended_price=(quantity * price_per_unit)
        )
        return id

    def _helper_set_po_status(self, po_number, po_status):
        db = self.globals['db']
        status = db(db.po_status_reference.status == po_status).select().first().id
        db(db.po_form.id == po_number).update(status=status)

    def _helper_set_po_department(self, po_number, department):
        db = self.globals['db']
        department_id = db(db.department.name == department).select().first().id
        db(db.po_form.id == po_number).update(department=department_id)

    def _helper_add_tax(self, po_number, tax_percent):
        db = self.globals['db']
        po = db(db.po_form.id == po_number).select().first()
        po_subtotal = po.subtotal
        db(db.po_form.id == po_number).update(tax_percent=tax_percent, tax=po_subtotal * tax_percent)
    
    def _helper_add_freight(self, po_number, freight):
        db = self.globals['db']
        db(db.po_form.id == po_number).update(freight=freight)

    def _helper_add_approval_note(self, po_number, username):
        # This is simply in place to help with POs that need approvals cleansed.
        db = self.globals['db']
        user_id = db(db.auth_user.username == username).select().first().id
        db.po_note.insert(po_form=po_number, po_approval=True, comment=f'Approved By {username}', made_by=user_id)

class TestManageUserPO_CheckPONumber(ManageUserPOControllerTest):
    USER = 'submitter'
    PASSWORD = 'password'
    ROLE = 'Submitter'
    DEPARTMENT = 'Feeds/Pet Foods'

    def test_check_po_number_not_logged_in(self):
        self.web2py_logout()
        try:
            self.manageuserpo_check_po_number(None)
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertTrue('login' in exception.headers['Location'])

    def test_check_po_number_no_po(self):
        try:
            self.manageuserpo_check_po_number(None)
        except HTTP as exception:
            error_message = self.globals['session'].flash
            self.assertEqual(exception.status, 303)
            self.assertEqual(error_message, 'No PO Number Provided')
            self.assertIn('viewPOForms/index', exception.headers['Location'])

    def test_check_po_number_invalid_po(self):
        try:
            self.manageuserpo_check_po_number(po_number=random.randint(0, 1000000000))
        except HTTP as exception:
            error_message = self.globals['session'].flash
            self.assertEqual(exception.status, 303)
            self.assertTrue(error_message.startswith('Invalid PO #'))
            self.assertTrue('viewPOForms/index' in exception.headers['Location'])
        pass

    def test_check_po_number_valid_po(self):
        po_number = self._helper_createpo()
        self.manageuserpo_check_po_number(po_number=po_number)

class TestManageUserPO_GetPODepartment(ManageUserPOControllerTest):
    USER = 'submitter'
    PASSWORD = 'password'
    ROLE = 'Submitter'
    DEPARTMENT = 'Feeds/Pet Foods'

    def test_get_department_from_po_not_logged_in_unauthorized(self):
        self.web2py_logout()
        try:
            self.manageuserpo_get_po_department(None)
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertTrue('login' in exception.headers['Location'])

    def test_get_department_from_po_invalid_po(self):
        department = self.manageuserpo_get_po_department(None)
        self.assertEqual(department, None)

    def test_get_department_from_po_valid_po_no_department(self):
        po_number = self._helper_createpo()
        department = self.manageuserpo_get_po_department(po_number)
        self.assertEqual(department, None)

    def test_get_department_from_po_valid_po_with_department(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        department = self.manageuserpo_get_po_department(po_number)
        self.assertEqual(department, self.DEPARTMENT)

class TestManageUserPO_IsPOEditable(ManageUserPOControllerTest):
    USER = 'submitter'
    PASSWORD = 'password'
    ROLE = 'Submitter'
    DEPARTMENT = 'Feeds/Pet Foods'

    def test_is_po_editable_not_logged_in(self):
        self.web2py_logout()
        try:
            self.manageuserpo_is_po_editable(None)
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertTrue('login' in exception.headers['Location'])

    def test_is_po_editable_invalid_po(self):
        is_editable = self.manageuserpo_is_po_editable(None)
        self.assertFalse(is_editable)

    def test_is_po_editable_invalid_permission(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self.web2py_remove_role(self.USER, self.ROLE, self.DEPARTMENT)
        
        is_editable = self.manageuserpo_is_po_editable(po_number)
        self.assertFalse(is_editable)

    def test_is_po_editable_po_closed(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_set_po_status(po_number, 'Closed')
        
        is_editable = self.manageuserpo_is_po_editable(po_number)
        self.assertFalse(is_editable)

    def test_is_po_editable_po_ordered_user_outside_department(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self.web2py_register('department head', 'password')
        self.web2py_set_role('department head', 'Department Head', 'IT')
        self.web2py_logout()
        self.web2py_login('department head', 'password')
        
        is_editable = self.manageuserpo_is_po_editable(po_number)
        self.assertFalse(is_editable)

    def test_is_po_editable_po_state_less_than_ordered_not_origin_submitter_department(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self.web2py_register('supervisor', 'password')
        self.web2py_set_role('supervisor', 'Supervisor', 'Feeds/Pet Foods')
        self.web2py_logout()
        self.web2py_login('supervisor', 'password')
        
        is_editable = self.manageuserpo_is_po_editable(po_number)
        self.assertFalse(is_editable)

    def test_is_po_editable_po_state_ordered_submitter_in_department(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_set_po_status(po_number, 'Ordered')
        self.web2py_register('supervisor', 'password')
        self.web2py_set_role('supervisor', 'Supervisor', 'Feeds/Pet Foods')
        self.web2py_logout()
        self.web2py_login('supervisor', 'password')
        
        is_editable = self.manageuserpo_is_po_editable(po_number)
        self.assertTrue(is_editable)

    def test_is_po_editable_po_state_less_than_closed_origin_submitter(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        statuses = ['Created', 'Submitted', 'Approved', 'Rejected', 'Ordered']
        for status in statuses:
            self._helper_set_po_status(po_number, status)
            is_editable = self.manageuserpo_is_po_editable(po_number)
            self.assertTrue(is_editable)

class TestManageUserPO_DeletePO(ManageUserPOControllerTest):
    USER = 'submitter'
    PASSWORD = 'password'
    ROLE = 'Submitter'
    DEPARTMENT = 'Feeds/Pet Foods'

    def test_unable_to_delete_po_not_logged_in(self):
        self.web2py_logout()
        try:
            self.manageuserpo_process_delete_po(None)
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertTrue('login' in exception.headers['Location'])

    def test_unable_to_delete_po_invalid_permission(self):
        self.web2py_logout()
        self.web2py_register('supervisor', 'password')
        self.web2py_login('supervisor', 'password')
        self.exec_controller()

        try:
            self.manageuserpo_process_delete_po(None)
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertTrue(exception.headers['Location'].endswith('not_authorized'))

    def test_unable_to_delete_po_no_po(self):
        try:
            self.manageuserpo_process_delete_po(None)
        except HTTP as exception:
            error_message = self.globals['session'].flash
            self.assertEqual(exception.status, 303)
            self.assertEqual(error_message, 'Failed To Delete PO: Invalid PO')

    def test_unable_to_delete_po_invalid_po(self):
        try:
            import random
            po_number = random.randint(0, 1000000000)
            self.manageuserpo_check_po_number(po_number=po_number)
        except HTTP as exception:
            error_message = self.globals['session'].flash
            self.assertEqual(exception.status, 303)
            self.assertEqual(error_message, f'Invalid PO #{po_number}')
        pass

    def test_unable_to_delete_po_all_roles(self):
        po_number = self._helper_createpo()
        self._helper_set_po_status(po_number, 'Submitted')
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        roles = [
            ('Submitter', self.DEPARTMENT),
            ('Supervisor', self.DEPARTMENT),
            ('Department Head', self.DEPARTMENT),
            ("superapprover", None),
            ("accounting", None),
            ("superuser", None)
        ]

        for role_info in roles:
            previous_role = self.ROLE
            self.web2py_logout()
            self.web2py_remove_role(self.USER, previous_role)
            role = role_info[0]
            department = role_info[1]
            self.web2py_register(role, 'password')
            self.web2py_login(role, 'password')
            self.web2py_set_role(self.USER, role, department)
            self.exec_controller()
            try:
                self.manageuserpo_process_delete_po(po_number)
            except HTTP as exception:
                self.assertEqual(exception.status, 303)
                self.assertIn('not_authorized', exception.headers['Location'])

    def test_unable_to_delete_po_submitter_but_not_created_state(self):
        po_number = self._helper_createpo()
        self._helper_set_po_status(po_number, 'Submitted')
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        try:
            self.manageuserpo_process_delete_po(po_number)
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertTrue(exception.headers['Location'].endswith(f'index?po_number={po_number}'))
            self.assertEqual(session.flash, f'You Do Not Have Permission To Delete PO #{po_number}')

    def test_delete_po_user_is_submitter_and_po_created_state(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        try:
            self.manageuserpo_process_delete_po(po_number)
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertEqual(session.flash, f'Deleted PO #{po_number}')

class TestManageUserPO_AddApproval(ManageUserPOControllerTest):
    # This will test to ensure approvals are added appropriately. This does not
    # test to make sure the po is actually approved
    USER = 'submitter'
    PASSWORD = 'password'
    ROLE = 'Supervisor'
    DEPARTMENT = 'Feeds/Pet Foods'

    def test_unable_to_approve_own_po(self):
        po_number = self._helper_createpo()
        self._helper_set_po_status(po_number, 'Submitted')
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        try:
            self.manageuserpo_process_approve_po(po_number)
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertTrue(exception.headers['Location'].endswith(f'index?po_number={po_number}'))
            self.assertEqual(session.flash, f'You Do Not Have Permission To Approve PO #{po_number}')
            # TODO WE ALSO NEED TO CHECK THE PO_NOTE TABLE TO MAKE SURE NO APPROVAL NOTES WERE ADDED

    def test_unable_to_approve_po_not_logged_in(self):
        self.web2py_logout()
        try:
            self.manageuserpo_process_approve_po(None)
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertTrue('login' in exception.headers['Location'])

    def test_unable_to_approve_no_po(self):
        try:
            self.manageuserpo_process_approve_po(None)
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertTrue(exception.headers['Location'].endswith('viewPOForms/index'))
            self.assertEqual(session.flash, 'Failed To Approve PO: Invalid PO')

    def test_unable_to_approve_invalid_po(self):
        po_number = random.randint(0, 10000000)
        try:
            self.manageuserpo_process_approve_po(po_number)
        except HTTP as exception:
            db = self.globals['db']
            approvals = db((db.po_note.po_form == po_number) & (db.po_note.po_approval == True)).count()
            self.assertEqual(approvals, 0)

            self.assertEqual(exception.status, 303)
            self.assertTrue(exception.headers['Location'].endswith('viewPOForms/index'))
            self.assertEqual(session.flash, 'Failed To Approve PO: Invalid PO')

    def test_unable_to_approve_po_po_not_submitted(self):
        po_number = self._helper_createpo()
        self._helper_set_po_status(po_number, 'Created')
        self._helper_set_po_department(po_number, self.DEPARTMENT)

        self._helper_set_po_department(po_number, self.DEPARTMENT)

        self.web2py_register('supervisor', 'password')
        self.web2py_set_role('supervisor', 'Supervisor', 'Feeds/Pet Foods')
        self.web2py_logout()
        self.web2py_login('supervisor', 'password')
        self.exec_controller()

        try:
            self.manageuserpo_process_approve_po(po_number)
        except HTTP as exception:
            db = self.globals['db']
            approvals = db((db.po_note.po_form == po_number) & (db.po_note.po_approval == True)).count()
            self.assertEqual(approvals, 0)

            self.assertEqual(exception.status, 303)
            self.assertTrue(exception.headers['Location'].endswith(f'index?po_number={po_number}'))
            self.assertEqual(session.flash, f"You Do Not Have Permission To Approve PO #{po_number}")

    def test_unable_to_approve_po_invalid_permission(self):
        po_number = self._helper_createpo()

        self._helper_set_po_status(po_number, 'Submitted')
        self._helper_set_po_department(po_number, self.DEPARTMENT)

        self.web2py_register('submitter', 'password')
        self.web2py_set_role('submitter', 'Submitter', 'IT')
        self.web2py_logout()
        self.web2py_login('submitter', 'password')
        try:
            self.manageuserpo_process_approve_po(po_number)
        except HTTP as exception:
            db = self.globals['db']
            approvals = db((db.po_note.po_form == po_number) & (db.po_note.po_approval == True)).count()
            self.assertEqual(approvals, 0)

            self.assertEqual(exception.status, 303)
            self.assertTrue(exception.headers['Location'].endswith(f'index?po_number={po_number}'))
            self.assertEqual(session.flash, f'You Do Not Have Permission To Approve PO #{po_number}')

    def test_approve_po_supervisor_or_higher(self):
        po_number = self._helper_createpo()

        self._helper_set_po_status(po_number, 'Submitted')
        self._helper_set_po_department(po_number, self.DEPARTMENT)

        self.web2py_register('supervisor', 'password')
        self.web2py_set_role('supervisor', 'Supervisor', 'Feeds/Pet Foods')
        self.web2py_logout()
        self.web2py_login('supervisor', 'password')
        try:
            self.manageuserpo_process_approve_po(po_number)
        except HTTP as exception:
            db = self.globals['db']
            auth = self.globals['auth']
            approvals = db((db.po_note.po_form == po_number) & (db.po_note.po_approval == True)).select()
            self.assertEqual(len(approvals), 1)
            approver = approvals.first().made_by
            self.assertEqual(approver, auth.user.id)

            self.assertEqual(exception.status, 303)
            self.assertTrue(exception.headers['Location'].endswith(f'viewPOForms/index'))
    
    def test_approve_po_auto_approve(self):
        po_number = self._helper_createpo()

        db = self.globals['db']

        self._helper_set_po_status(po_number, 'Submitted')
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        try:
            self.manageuserpo_process_approve_po(po_number, 'Auto Approver')
        except HTTP as exception:
            db = self.globals['db']
            approvals = db((db.po_note.po_form == po_number) & (db.po_note.po_approval == True)).select()
            self.assertEqual(len(approvals), 1)
            approver = approvals.first().made_by
            self.assertEqual(approver, self.AUTO_APPROVER_ID)

            self.assertEqual(exception.status, 303)
            self.assertTrue(exception.headers['Location'].endswith(f'viewPOForms/index'))

class TestManageUserPO_RejectPO(ManageUserPOControllerTest):
    USER = 'supervisor'
    PASSWORD = 'password'
    ROLE = 'Supervisor'
    DEPARTMENT = 'Feeds/Pet Foods'

    def test_unable_to_reject_po_not_logged_in(self):
        self.web2py_logout()
        self.exec_controller()
        try:
            self.manageuserpo_process_reject_po(None)
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertTrue('login' in exception.headers['Location'])

    def test_unable_to_reject_po_no_po(self):
        try:
            self.manageuserpo_process_reject_po(None)
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertTrue(exception.headers['Location'].endswith('viewPOForms/index'))
            self.assertEqual(session.flash, 'Failed To Reject PO: Invalid PO')

    def test_unable_to_reject_invalid_po(self):
        po_number = random.randint(0, 10000000)
        try:
            self.manageuserpo_process_reject_po(po_number)
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertTrue(exception.headers['Location'].endswith('viewPOForms/index'))
            self.assertEqual(session.flash, 'Failed To Reject PO: Invalid PO')

    def test_unable_to_reject_po_invalid_permission_in_department(self):
        po_number = self._helper_createpo()
        self._helper_set_po_status(po_number, 'Submitted')
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        submitted_status = self.globals['db'].po_form(po_number).status

        self.web2py_register('submitter', 'password')
        self.web2py_set_role('submitter', 'Submitter', self.DEPARTMENT)
        self.web2py_logout()
        self.web2py_login('submitter', 'password')
        self.exec_controller()
        try:
            self.manageuserpo_process_reject_po(po_number)
        except HTTP as exception:
            db = self.globals['db']

            self.assertEqual(exception.status, 303)
            self.assertTrue(exception.headers['Location'].endswith('not_authorized'))
            self.assertEqual(db.po_form(po_number).status, submitted_status)

    def test_unable_to_reject_po_valid_permission_outside_department(self):
        po_number = self._helper_createpo()
        self._helper_set_po_status(po_number, 'Submitted')
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        
        db = self.globals['db']
        submitted_status = self.globals['db'].po_form(po_number).status

        self.web2py_register('supervisor IT', 'password')
        self.web2py_set_role('supervisor IT', 'Supervisor', 'IT')
        self.web2py_logout()
        self.web2py_login('supervisor IT', 'password')
        self.exec_controller()
        try:
            self.manageuserpo_process_reject_po(po_number)
        except HTTP as exception:
            db = self.globals['db']

            self.assertEqual(exception.status, 303)
            self.assertTrue(session.flash, f'You Do Not Have Permission To Reject PO #{po_number}')
            self.assertTrue(f'manageUserPO/index?po_number={po_number}' in exception.headers['Location'])
            self.assertEqual(db.po_form(po_number).status, submitted_status)

    def test_unable_to_reject_po_po_not_submitted(self):
        po_number = self._helper_createpo()
        self._helper_set_po_status(po_number, 'Created')
        submitted_status = self.globals['db'].po_form(po_number).status
        self._helper_set_po_department(po_number, self.DEPARTMENT)

        self.web2py_logout()
        self.web2py_register('supervisor', 'password')
        self.web2py_set_role('supervisor', 'Supervisor', self.DEPARTMENT)
        self.web2py_login('supervisor', 'password')
        self.exec_controller()
        
        try:
            self.manageuserpo_process_reject_po(po_number)
        except HTTP as exception:
            db = self.globals['db']

            self.assertEqual(exception.status, 303)
            self.assertTrue(f'manageUserPO/index?po_number={po_number}' in exception.headers['Location'])
            self.assertEqual(session.flash, f'PO #{po_number} cannot be rejected as it is not currently submitted')
            self.assertEqual(db.po_form(po_number).status, submitted_status)

    def test_reject_po_supervisor_role_in_department(self):
        po_number = self._helper_createpo()
        self._helper_set_po_status(po_number, 'Submitted')
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self.web2py_logout()

        db = self.globals['db']
        rejected_status = db(db.po_status_reference.status == 'Rejected').select().first().id

        self.web2py_register('supervisor', 'password')
        self.web2py_set_role('supervisor', 'Supervisor', self.DEPARTMENT)
        self.web2py_login('supervisor', 'password')
        self.exec_controller()

        try:
            self.manageuserpo_process_reject_po(po_number)
        except HTTP as exception:
            db = self.globals['db']

            self.assertEqual(exception.status, 303)
            self.assertTrue(exception.headers['Location'].endswith('viewPOForms/index'))
            self.assertEqual(db.po_form(po_number).status, rejected_status)

class TestManageUserPO_OrderPO(ManageUserPOControllerTest):
    USER = 'submitter'
    PASSWORD = 'password'
    ROLE = 'Submitter'
    DEPARTMENT = 'Feeds/Pet Foods'
    
    def test_unable_to_order_po_not_logged_in(self):
        self.web2py_logout()
        self.exec_controller()
        try:
            self.manageuserpo_process_order_po(None)
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertTrue('login' in exception.headers['Location'])

    def test_unable_to_order_po_invalid_permission(self):
        self.web2py_remove_role(self.USER, self.ROLE, self.DEPARTMENT)
        self.exec_controller()
        try:
            self.manageuserpo_process_order_po(None)
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertTrue(exception.headers['Location'].endswith('not_authorized'))

    def test_unable_to_order_po_no_po_number(self):
        try:
            self.manageuserpo_process_order_po(None)
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertEqual(session.flash, 'Failed To Order PO: Invalid PO')
            self.assertTrue(exception.headers['Location'].endswith(f'viewPOForms/index'))

    def test_unable_to_order_po_invalid_po(self):
        po_number = random.randint(0, 10000000)
        try:
            self.manageuserpo_process_order_po(po_number)
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertTrue(exception.headers['Location'].endswith('viewPOForms/index'))
            self.assertEqual(session.flash, 'Failed To Order PO: Invalid PO')

    def test_unable_to_order_po_po_not_approved(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_set_po_status(po_number, 'Created')
        try:
            self.manageuserpo_process_order_po(po_number)
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertTrue(f'manageUserPO/index?po_number={po_number}' in exception.headers['Location'])
            self.assertEqual(session.flash, f'PO #{po_number} cannot be ordered as it has not been approved')

    def test_unable_to_order_po_user_is_not_submitter(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_set_po_status(po_number, 'Approved')

        self.web2py_register('supervisor', 'password')
        self.web2py_set_role('supervisor', 'Supervisor', self.DEPARTMENT)
        self.web2py_login('supervisor', 'password')
        self.exec_controller()

        db = self.globals['db']
        approved_status = db.po_form(po_number).status

        try:
            self.manageuserpo_process_order_po(po_number)
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertTrue(f'manageUserPO/index?po_number={po_number}' in exception.headers['Location'])
            self.assertEqual(session.flash, f'You Do Not Have Permission To Place PO #{po_number} Into An Ordered State')
            self.assertEqual(db.po_form(po_number).status, approved_status)

    def test_order_po_user_is_submitter_po_approved(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_set_po_status(po_number, 'Approved')
        try:
            self.manageuserpo_process_order_po(po_number)
        except HTTP as exception:
            db = self.globals['db']
            ordered_status = db(db.po_status_reference.status == 'Ordered').select().first().id

            self.assertEqual(exception.status, 303)
            self.assertTrue(exception.headers['Location'].endswith('viewPOForms/index'))
            self.assertEqual(db.po_form(po_number).status, ordered_status)

class TestManageUserPO_ClosePO(ManageUserPOControllerTest):
    USER = 'submitter'
    PASSWORD = 'password'
    ROLE = 'Submitter'
    DEPARTMENT = 'Feeds/Pet Foods'
    
    def test_unable_to_close_po_not_logged_in(self):
        self.web2py_logout()
        self.exec_controller()
        try:
            self.manageuserpo_process_close_po(None)
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertTrue('login' in exception.headers['Location'])
        pass

    def test_unable_to_close_po_no_po_number(self):
        try:
            self.manageuserpo_process_close_po(None)
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertEqual(session.flash, 'Failed To Close PO: Invalid PO')
            self.assertTrue(exception.headers['Location'].endswith(f'viewPOForms/index'))

    def test_unable_to_close_po_invalid_po(self):
        po_number = random.randint(0, 10000000)
        try:
            self.manageuserpo_process_close_po(po_number)
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertTrue(exception.headers['Location'].endswith('viewPOForms/index'))
            self.assertEqual(session.flash, 'Failed To Close PO: Invalid PO')

    def test_unable_to_close_po_invalid_minimum_permission(self):
        po_number = self._helper_createpo()
        self.web2py_remove_role(self.USER, self.ROLE, self.DEPARTMENT)
        self.exec_controller()
        try:
            self.manageuserpo_process_order_po(po_number)
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertTrue(exception.headers['Location'].endswith('not_authorized'))

    def test_unable_to_close_po_supervisor_all_statuses_except_submitted(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)

        self.web2py_logout()
        self.web2py_register('supervisor', 'password')
        self.web2py_set_role('supervisor', 'Supervisor', self.DEPARTMENT)
        self.web2py_login('supervisor', 'password')

        db = self.globals['db']
        for row in db(db.po_status_reference.status != 'Submitted').select():
            status = row.status
            self._helper_set_po_status(po_number, status)
            po_status = db.po_form(po_number).status
            try:
                self.manageuserpo_process_close_po(po_number)
            except HTTP as exception:
                self.assertEqual(exception.status, 303)
                self.assertEqual(session.flash, f'You Do Not Have Permission To Close PO #{po_number}')
                self.assertTrue(f'manageUserPO/index?po_number={po_number}' in exception.headers['Location'])
                self.assertEqual(db.po_form(po_number).status, po_status)

    def test_unable_to_close_po_accounting_all_statuses_except_ordered(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)

        self.web2py_logout()
        self.web2py_register('accounting', 'password')
        self.web2py_set_role('accounting', 'accounting')
        self.web2py_login('accounting', 'password')

        db = self.globals['db']
        for row in db(db.po_status_reference.status != 'Ordered').select():
            status = row.status
            self._helper_set_po_status(po_number, status)
            po_status = db.po_form(po_number).status
            try:
                self.manageuserpo_process_close_po(po_number)
            except HTTP as exception:
                self.assertEqual(exception.status, 303)
                self.assertEqual(session.flash, f'You Do Not Have Permission To Close PO #{po_number}')
                self.assertTrue(f'manageUserPO/index?po_number={po_number}' in exception.headers['Location'])
                self.assertEqual(db.po_form(po_number).status, po_status)

    def test_unable_to_close_po_user_is_submitter(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        po_status = db.po_form(po_number).status
        try:
            self.manageuserpo_process_close_po(po_number)
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertEqual(session.flash, f'You Do Not Have Permission To Close PO #{po_number}')
            self.assertTrue(f'manageUserPO/index?po_number={po_number}' in exception.headers['Location'])
            self.assertEqual(db.po_form(po_number).status, po_status)

    def test_close_po_accounting_status_ordered(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_set_po_status(po_number, 'Ordered')

        db = self.globals['db']
        closed_status = db(db.po_status_reference.status == 'Closed').select().first().id

        self.web2py_logout()
        self.web2py_register('accounting', 'password')
        self.web2py_set_role('accounting', 'accounting')
        self.web2py_login('accounting', 'password')

        try:
            self.manageuserpo_process_close_po(po_number)
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertTrue(exception.headers['Location'].endswith('viewPOForms/index'))
            self.assertEqual(db.po_form(po_number).status, closed_status)

    def test_close_po_supervisor_status_submitted(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_set_po_status(po_number, 'Submitted')

        db = self.globals['db']
        closed_status = db(db.po_status_reference.status == 'Closed').select().first().id

        self.web2py_logout()
        self.web2py_register('supervisor', 'password')
        self.web2py_set_role('supervisor', 'Supervisor', self.DEPARTMENT)
        self.web2py_login('supervisor', 'password')

        try:
            self.manageuserpo_process_close_po(po_number)
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertTrue(exception.headers['Location'].endswith('viewPOForms/index'))
            self.assertEqual(db.po_form(po_number).status, closed_status)

    def test_close_po_superuser(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)

        db = self.globals['db']
        closed_status = db(db.po_status_reference.status == 'Closed').select().first().id

        self.web2py_logout()
        self.web2py_register('superuser', 'password')
        self.web2py_set_role('superuser', 'superuser')
        self.web2py_login('superuser', 'password')

        try:
            self.manageuserpo_process_close_po(po_number)
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertTrue(exception.headers['Location'].endswith('viewPOForms/index'))
            self.assertEqual(db.po_form(po_number).status, closed_status)

class TestManageUserPO_ValidateSubmittedPO(ManageUserPOControllerTest):
    USER = 'submitter'
    PASSWORD = 'password'
    ROLE = 'Submitter'
    DEPARTMENT = 'Feeds/Pet Foods'

    def test_invalid_submission_no_po_number(self):
        error_message = self.manageuserpo_validate_submitted_po(None)
        self.assertEqual(error_message, "Failed To Submit PO: Invalid PO")

    def test_invalid_submission_invalid_po_number(self):
        po_number = random.randint(0, 1000000)
        error_message = self.manageuserpo_validate_submitted_po(po_number)
        self.assertEqual(error_message, "Failed To Submit PO: Invalid PO")

    def test_invalid_submission_no_department_on_po(self):
        po_number = self._helper_createpo()
        error_message = self.manageuserpo_validate_submitted_po(po_number)
        self.assertEqual(error_message, 'Failed To Submit PO: No Department Selected')

    def test_invalid_submission_no_items_on_po(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        error_message = self.manageuserpo_validate_submitted_po(po_number)
        self.assertEqual(error_message, 'Failed To Submit PO: No PO Items Found')

    def test_invalid_submission_items_quantity_less_than_or_equal_to_0_po(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_add_item(po_number, 'some catalog', 'some description', -10, -10.99)
        error_message = self.manageuserpo_validate_submitted_po(po_number)
        self.assertEqual(error_message, 'Failed To Submit PO: Subtotal Needs To Be Greater Than 0')

    def test_invalid_submission_po_subtotal_less_than_or_equal_to_0(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_add_item(po_number, 'some catalog', 'some description', 10, -10.99)
        db = self.globals['db']
        db.po_form(po_number).update(subtotal = (10 * -10.99))

        error_message = self.manageuserpo_validate_submitted_po(po_number)
        self.assertEqual(error_message, 'Failed To Submit PO: Subtotal Needs To Be Greater Than 0')

    def test_invalid_submission_invalid_user_permission(self):
        po_number = self._helper_createpo()

        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_add_item(po_number, 'some catalog', 'some description', 10, 10.99)

        self.web2py_remove_role(self.USER, self.ROLE, self.DEPARTMENT)
        db = self.globals['db']
        db.po_form(po_number).update(subtotal = (10 * 10.99))

        error_message = self.manageuserpo_validate_submitted_po(po_number)
        self.assertEqual(error_message, "Failed to Submit PO: Invalid Permission For Department")


    def test_invalid_submission_not_po_owner(self):
        po_number = self._helper_createpo()

        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_add_item(po_number, 'some catalog', 'some description', 10, 10.99)

        db = self.globals['db']
        auth = self.globals['auth']
        db(db.po_form.id == po_number).update(subtotal = (10 * 10.99))

        self.web2py_logout()
        self.web2py_register('supervisor', 'password')
        self.web2py_set_role('supervisor', 'Supervisor', self.DEPARTMENT)
        self.web2py_login('supervisor', 'password')

        error_message = self.manageuserpo_validate_submitted_po(po_number)
        self.assertEqual(error_message, f"Failed to Submit PO: {auth.user.first_name} {auth.user.last_name} is not the owner of this PO")

    def test_submission_po_with_correct_details(self):
        po_number = self._helper_createpo()

        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_add_item(po_number, 'some catalog', 'some description', 10, 10.99)

        db = self.globals['db']
        db(db.po_form.id == po_number).update(subtotal = (10 * 10.99))

        self.manageuserpo_validate_submitted_po(po_number)

class TestManageUserPO_MapSubmittedPO(ManageUserPOControllerTest):
    USER = 'submitter'
    PASSWORD = 'password'
    ROLE = 'Submitter'
    DEPARTMENT = "Feeds/Pet Foods"

    def test_invalid_submission_map_no_po(self):
        try:
            self.manageuserpo_map_submitted_po(None)
        except Exception as exception:
            exception_message = exception.args[0]
            self.assertEqual(exception_message, 'No PO Provided')

    def test_invalid_submission_map_invalid_po(self):
        po_number = random.randint(0,1000000)
        try:
            self.manageuserpo_map_submitted_po(po_number)
        except Exception as exception:
            exception_message = exception.args[0]
            self.assertEqual(exception_message, 'No PO Provided')

    def test_invalid_submission_map_no_department_on_po(self):
        po_number = self._helper_createpo()
        try:
            self.manageuserpo_map_submitted_po(po_number)
        except Exception as exception:
            exception_message = exception.args[0]
            self.assertEqual(exception_message, f'No Rule Found For PO #{po_number}')

    def test_invalid_submission_map_no_subtotal_on_po(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        try:
            self.manageuserpo_map_submitted_po(po_number)
        except Exception as exception:
            exception_message = exception.args[0]
            self.assertEqual(exception_message, f'No Rule Found For PO #{po_number}')

    def test_successful_submission_map_po(self):
        po_number = self._helper_createpo()
        db = self.globals['db']
        previous_department = self.DEPARTMENT
        for department in [department for department in db(db.department.id >= 0).select()]:
            self.web2py_remove_role(self.USER, self.ROLE, previous_department)
            self._helper_set_po_department(po_number, department.name)
            self.web2py_set_role(self.USER, self.ROLE, department.name)
            previous_department = department.name
            for rule in [rule for rule in db(db.po_rule.department == department.id).select()]:
                db(db.po_form.id == po_number).update(subtotal = (rule.min_subtotal + 1))
                auto_approval = self.manageuserpo_map_submitted_po(po_number)
                if rule.min_subtotal == 0:
                    self.assertTrue(auto_approval)
                else:
                    self.assertFalse(auto_approval)
                mapped_rule = db(db.po_rule_map.po_form == po_number).select().first()
                self.assertEqual(mapped_rule.po_rule, rule.id)

class TestManageUserPO_SubmitPO(ManageUserPOControllerTest):
    USER = 'submitter'
    PASSWORD = 'password'
    ROLE = 'Submitter'
    DEPARTMENT = 'Feeds/Pet Foods'

    def test_invalid_submission_no_po_number(self):
        try:
            self.manageuserpo_submit_po(None)
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertEqual(session.flash, "Failed To Submit PO: Invalid PO")
            self.assertTrue('viewPOForms/index' in exception.headers["Location"])

    def test_invalid_submission_invalid_po_number(self):
        po_number = random.randint(0, 1000000)
        try:
            self.manageuserpo_submit_po(po_number)
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertEqual(session.flash, "Failed To Submit PO: Invalid PO")
            self.assertTrue('viewPOForms/index' in exception.headers["Location"])

    def test_invalid_submission_no_department_on_po(self):
        po_number = self._helper_createpo()
        try:
            self.manageuserpo_submit_po(po_number)
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertEqual(session.flash, 'Failed To Submit PO: No Department Selected')
            self.assertTrue(f'manageUserPO/index?po_number={po_number}' in exception.headers["Location"])

    def test_invalid_submission_no_items_on_po(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        try:
            self.manageuserpo_submit_po(po_number)
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertEqual(session.flash, 'Failed To Submit PO: No PO Items Found')
            self.assertTrue(f'manageUserPO/index?po_number={po_number}' in exception.headers["Location"])

    def test_invalid_submission_items_quantity_less_than_or_equal_to_0_po(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_add_item(po_number, 'some catalog', 'some description', -10, -10.99)
        try:
            self.manageuserpo_submit_po(po_number)
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertEqual(session.flash, 'Failed To Submit PO: Subtotal Needs To Be Greater Than 0')
            self.assertTrue(f'manageUserPO/index?po_number={po_number}' in exception.headers["Location"])

    def test_invalid_submission_po_subtotal_less_than_or_equal_to_0(self):
        po_number = self._helper_createpo()
        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_add_item(po_number, 'some catalog', 'some description', 10, -10.99)
        db = self.globals['db']
        db.po_form(po_number).update(subtotal = (10 * -10.99))

        try:
            self.manageuserpo_submit_po(po_number)
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertEqual(session.flash, 'Failed To Submit PO: Subtotal Needs To Be Greater Than 0')
            self.assertTrue(f'manageUserPO/index?po_number={po_number}' in exception.headers["Location"])

    def test_invalid_submission_invalid_user_permission(self):
        po_number = self._helper_createpo()

        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_add_item(po_number, 'some catalog', 'some description', 10, 10.99)

        self.web2py_remove_role(self.USER, self.ROLE, self.DEPARTMENT)
        db = self.globals['db']
        db.po_form(po_number).update(subtotal = (10 * 10.99))

        try:
            self.manageuserpo_submit_po(po_number)
        except HTTP as exception:
            self.assertEqual(exception.status, 303)
            self.assertEqual(session.flash, "Failed to Submit PO: Invalid Permission For Department")
            self.assertTrue(f'manageUserPO/index?po_number={po_number}' in exception.headers["Location"])

    def test_po_submission_subtotal_under_100(self):
        po_number = self._helper_createpo()

        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_add_item(po_number, 'some catalog', 'some description', 1, 10.99)

        db = self.globals['db']
        db(db.po_form.id == po_number).update(subtotal = 10.99)
        po_status = db(db.po_form.id == po_number).select().first().status
        submitted_status = db(db.po_status_reference.status == 'Submitted').select().first().id

        try:
            self.manageuserpo_submit_po(po_number)
        except HTTP as exception:
            # Should be redirecting to auto approval
            # Also check to make sure that the status of the PO is submitted now
            self.assertEqual(exception.status, 303)
            self.assertNotEqual(db.po_form(po_number).status, po_status)
            self.assertEqual(db.po_form(po_number).status, submitted_status)
            self.assertTrue(f'manageUserPO/processApprovePO?approver=Auto+Approver&po_number={po_number}' in exception.headers['Location'])

    def test_po_submission_subtotal_over_100(self):
        po_number = self._helper_createpo()

        self._helper_set_po_department(po_number, self.DEPARTMENT)
        self._helper_add_item(po_number, 'some catalog', 'some description', 1, 100000.00)

        db = self.globals['db']
        db(db.po_form.id == po_number).update(subtotal = 100000.00)
        po_status = db(db.po_form.id == po_number).select().first().status
        submitted_status = db(db.po_status_reference.status == 'Submitted').select().first().id

        try:
            self.manageuserpo_submit_po(po_number)
        except HTTP as exception:
            # Should be redirecting to manage user po
            # Also check to make sure that the status of the PO is submitted now
            self.assertEqual(exception.status, 303)
            self.assertNotEqual(db.po_form(po_number).status, po_status)
            self.assertEqual(db.po_form(po_number).status, submitted_status)
            self.assertTrue(f'viewPOForms/index' in exception.headers["Location"])

test = [
    TestManageUserPO_CheckPONumber('test_check_po_number_not_logged_in'),
    TestManageUserPO_CheckPONumber('test_check_po_number_no_po'),
    TestManageUserPO_CheckPONumber('test_check_po_number_invalid_po'),
    TestManageUserPO_CheckPONumber('test_check_po_number_valid_po'),
    TestManageUserPO_GetPODepartment('test_get_department_from_po_not_logged_in_unauthorized'),
    TestManageUserPO_GetPODepartment('test_get_department_from_po_invalid_po'),
    TestManageUserPO_GetPODepartment('test_get_department_from_po_valid_po_no_department'),
    TestManageUserPO_GetPODepartment('test_get_department_from_po_valid_po_with_department'),
    TestManageUserPO_IsPOEditable('test_is_po_editable_not_logged_in'),
    TestManageUserPO_IsPOEditable('test_is_po_editable_invalid_po'),
    TestManageUserPO_IsPOEditable('test_is_po_editable_invalid_permission'),
    TestManageUserPO_IsPOEditable('test_is_po_editable_po_closed'),
    TestManageUserPO_IsPOEditable('test_is_po_editable_po_ordered_user_outside_department'),
    TestManageUserPO_IsPOEditable('test_is_po_editable_po_state_less_than_ordered_not_origin_submitter_department'),
    TestManageUserPO_IsPOEditable('test_is_po_editable_po_state_ordered_submitter_in_department'),
    TestManageUserPO_IsPOEditable('test_is_po_editable_po_state_less_than_closed_origin_submitter'),
    TestManageUserPO_DeletePO('test_unable_to_delete_po_not_logged_in'),
    TestManageUserPO_DeletePO('test_unable_to_delete_po_invalid_permission'),
    TestManageUserPO_DeletePO('test_unable_to_delete_po_no_po'),
    TestManageUserPO_DeletePO('test_unable_to_delete_po_invalid_po'),
    TestManageUserPO_DeletePO('test_unable_to_delete_po_all_roles'),
    TestManageUserPO_DeletePO('test_unable_to_delete_po_submitter_but_not_created_state'),
    TestManageUserPO_DeletePO('test_delete_po_user_is_submitter_and_po_created_state'),
    TestManageUserPO_AddApproval('test_unable_to_approve_own_po'),
    TestManageUserPO_AddApproval('test_unable_to_approve_po_not_logged_in'),
    TestManageUserPO_AddApproval('test_unable_to_approve_no_po'),
    TestManageUserPO_AddApproval('test_unable_to_approve_invalid_po'),
    TestManageUserPO_AddApproval('test_unable_to_approve_po_po_not_submitted'),
    TestManageUserPO_AddApproval('test_unable_to_approve_po_invalid_permission'),
    TestManageUserPO_AddApproval('test_approve_po_supervisor_or_higher'),
    TestManageUserPO_AddApproval('test_approve_po_auto_approve'),
    TestManageUserPO_RejectPO('test_unable_to_reject_po_not_logged_in'),
    TestManageUserPO_RejectPO('test_unable_to_reject_po_no_po'),
    TestManageUserPO_RejectPO('test_unable_to_reject_invalid_po'),
    TestManageUserPO_RejectPO('test_unable_to_reject_po_invalid_permission_in_department'),
    TestManageUserPO_RejectPO('test_unable_to_reject_po_valid_permission_outside_department'),
    TestManageUserPO_RejectPO('test_unable_to_reject_po_po_not_submitted'),
    TestManageUserPO_RejectPO('test_reject_po_supervisor_role_in_department'),
    TestManageUserPO_OrderPO('test_unable_to_order_po_not_logged_in'),
    TestManageUserPO_OrderPO('test_unable_to_order_po_invalid_permission'),
    TestManageUserPO_OrderPO('test_unable_to_order_po_no_po_number'),
    TestManageUserPO_OrderPO('test_unable_to_order_po_invalid_po'),
    TestManageUserPO_OrderPO('test_unable_to_order_po_po_not_approved'),
    TestManageUserPO_OrderPO('test_unable_to_order_po_user_is_not_submitter'),
    TestManageUserPO_OrderPO('test_order_po_user_is_submitter_po_approved'),
    TestManageUserPO_ClosePO('test_unable_to_close_po_not_logged_in'),
    TestManageUserPO_ClosePO('test_unable_to_close_po_no_po_number'),
    TestManageUserPO_ClosePO('test_unable_to_close_po_invalid_po'),
    TestManageUserPO_ClosePO('test_unable_to_close_po_invalid_minimum_permission'),
    TestManageUserPO_ClosePO('test_unable_to_close_po_supervisor_all_statuses_except_submitted'),
    TestManageUserPO_ClosePO('test_unable_to_close_po_accounting_all_statuses_except_ordered'),
    TestManageUserPO_ClosePO('test_unable_to_close_po_user_is_submitter'),
    TestManageUserPO_ClosePO('test_close_po_accounting_status_ordered'),
    TestManageUserPO_ClosePO('test_close_po_supervisor_status_submitted'),
    TestManageUserPO_ClosePO('test_close_po_superuser'),
    TestManageUserPO_ValidateSubmittedPO('test_invalid_submission_no_po_number'),
    TestManageUserPO_ValidateSubmittedPO('test_invalid_submission_invalid_po_number'),
    TestManageUserPO_ValidateSubmittedPO('test_invalid_submission_no_department_on_po'),
    TestManageUserPO_ValidateSubmittedPO('test_invalid_submission_no_items_on_po'),
    TestManageUserPO_ValidateSubmittedPO('test_invalid_submission_items_quantity_less_than_or_equal_to_0_po'),
    TestManageUserPO_ValidateSubmittedPO('test_invalid_submission_po_subtotal_less_than_or_equal_to_0'),
    TestManageUserPO_ValidateSubmittedPO('test_invalid_submission_invalid_user_permission'),
    TestManageUserPO_ValidateSubmittedPO('test_invalid_submission_not_po_owner'),
    TestManageUserPO_ValidateSubmittedPO('test_submission_po_with_correct_details'),
    TestManageUserPO_MapSubmittedPO('test_invalid_submission_map_no_po'),
    TestManageUserPO_MapSubmittedPO('test_invalid_submission_map_invalid_po'),
    TestManageUserPO_MapSubmittedPO('test_invalid_submission_map_no_department_on_po'),
    TestManageUserPO_MapSubmittedPO('test_invalid_submission_map_no_subtotal_on_po'),
    TestManageUserPO_MapSubmittedPO('test_successful_submission_map_po'),
    TestManageUserPO_SubmitPO('test_invalid_submission_no_po_number'),
    TestManageUserPO_SubmitPO('test_invalid_submission_invalid_po_number'),
    TestManageUserPO_SubmitPO('test_invalid_submission_no_department_on_po'),
    TestManageUserPO_SubmitPO('test_invalid_submission_no_items_on_po'),
    TestManageUserPO_SubmitPO('test_invalid_submission_items_quantity_less_than_or_equal_to_0_po'),
    TestManageUserPO_SubmitPO('test_invalid_submission_test_invalid_submission_po_subtotal_less_than_or_equal_to_0no_items_on_po'),
    TestManageUserPO_SubmitPO('test_invalid_submission_invalid_user_permission'),
    TestManageUserPO_SubmitPO('test_po_submission_subtotal_under_100'),
    TestManageUserPO_SubmitPO('test_po_submission_subtotal_over_100'),
]

suite = unittest.TestSuite()
[suite.addTest(test) for test in tests]

unittest.TextTestRunner(verbosity=2).run(suite)