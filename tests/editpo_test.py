#!/usr/bin/env python

import sys
import os.path

GLUON_PATH = '/opt/web2py'
sys.path.insert(1, os.path.realpath(GLUON_PATH))

from gluon.dal import DAL
from gluon.shell import exec_environment, execfile
from gluon.globals import Request, Response, Session
from gluon import current
from gluon.storage import Storage
from gluon.http import HTTP

original_globals = dict(globals())

import unittest
import json

TEST_DB = 'sqlite://testing.sqlite'

class Web2PyTest(unittest.TestCase):
    # Parent Class
    """
        All tests run against the PO system should inherit this web2pytest class
    """
    CLEAR_TABLES = None
    CONTROLLER = None

    def setUp(self):
        self.setup_web2py()
        self.exec_controller()

    def setup_web2py(self):
        if db._uri != TEST_DB:
            raise Exception('Not using testing database')
        if self.CONTROLLER is None:
            raise NotImplementedError('CONTROLLER must be defined by a subclass of Web2PyTest')
        self.globals = original_globals.copy()
        self.clear_tables()

    def exec_controller(self):
        execfile(self.CONTROLLER, self.globals)

    def tearDown(self):
        self.clear_tables()
        self.web2py_logout()

    def clear_tables(self):
        # Dropping everything in the CLEAR_TABLES array
        if self.CLEAR_TABLES is None:
            raise NotImplementedError('CLEAR_TABLES must be defined by a subclass of Web2PyTest')
        db = self.globals['db']
        db.rollback()
        for table in self.CLEAR_TABLES:
            db[table].truncate()
        db.commit()

    def web2py_register(self, username, password):
        auth = self.globals['auth']
        test_user = dict(username=username, password=password)
        auth.register_bare(**test_user)

    def web2py_login(self, username, password):
        auth = self.globals['auth']
        test_user = dict(username=username, password=password)
        auth.login_bare(**test_user)

    def web2py_set_role(self, username, role_name, department=None):
        auth = self.globals['auth']
        db = self.globals['db']
        if department:
            role = f'{department} {role_name}'
        else:
            role = role_name
        auth.add_membership(
            group_id=auth.id_group(role), 
            user_id=db(db.auth_user.username == username).select().first().id)

    def web2py_remove_role(self, username, role_name, department=None):
        auth = self.globals['auth']
        db = self.globals['db']
        if department:
            role = f'{department} {role_name}'
        else:
            role = role_name
        auth.del_membership(
            group_id=auth.id_group(role), 
            user_id=db(db.auth_user.username == username).select().first().id)

    def web2py_logout(self):
        auth = self.globals['auth']
        auth.logout(next=None)

class EditPOControllerTest(Web2PyTest):
    CLEAR_TABLES = [
        'auth_user',
        'auth_membership',
        'po_form',
        'po_note',
        'po_item',
        'po_rule_map',
        'vendor'
    ]

    CONTROLLER = 'applications/po/controllers/editPO.py'
    # Helper items to add to POs
    # Price is 99.99
    TEST_ITEM_99_99 = dict(
        catalog="test item 1",
        description="test item 1",
        quantity=1,
        price_per_unit=99.99
    )

    # Price is 100.01
    TEST_ITEM_100_01 = dict(
        catalog="test item 1",
        description="test item 1",
        quantity=1,
        price_per_unit=100.01
    )

    # Price is 5000.01
    TEST_ITEM_5000_01 = dict(
        catalog="test item 1",
        description="test item 1",
        quantity=1,
        price_per_unit=5000.01
    )

    # Price is 50000.01
    TEST_ITEM_50000_01 = dict(
        catalog="test item 1",
        description="test item 1",
        quantity=1,
        price_per_unit=50000.01
    )

    USER = None
    PASSWORD = None
    ROLE = None
    DEPARTMENT = None

    def setUp(self):
        self.setup_web2py()

        if self.USER:
            user = dict(username=self.USER, password=self.PASSWORD)

            self.web2py_register(**user)
            self.web2py_login(**user)
            if self.ROLE:
                self.web2py_set_role(user['username'], self.ROLE, self.DEPARTMENT)

        self.exec_controller()

    def _helper_createpo(self):
        copy_globals = original_globals.copy()
        _controller = 'applications/po/controllers/createPO.py'
        execfile(_controller, copy_globals)
        exec('po_number=createNewPo()', copy_globals)
        po_number = copy_globals['po_number']
        self._helper_set_po_departmenet(po_number, self.DEPARTMENT)

        return po_number
    
    def _helper_add_item(self, po_number, catalog, description, quantity, price_per_unit):
        db = self.globals['db']
        id = db.po_item.insert(
            po_form=po_number,
            catalog=catalog,
            description=description,
            quantity=quantity,
            price_per_unit=price_per_unit,
            extended_price=(quantity * price_per_unit)
        )
        return id

    def _helper_set_po_status(self, po_number, po_status):
        db = self.globals['db']
        status = db(db.po_status_reference.status == po_status).select().first().id
        db(db.po_form.id == po_number).update(status=status)

    def _helper_set_po_departmenet(self, po_number, department):
        db = self.globals['db']
        department_id = db(db.department.name == self.DEPARTMENT).select().first().id
        db(db.po_form.id == po_number).update(department=department_id)

    def _helper_add_tax(self, po_number, tax_percent):
        db = self.globals['db']
        po = db(db.po_form.id == po_number).select().first()
        po_subtotal = po.subtotal

        db(db.po_form.id == po_number).update(tax_percent=tax_percent, tax=po_subtotal * tax_percent)
    
    def _helper_add_freight(self, po_number, freight):
        db = self.globals['db']
        db(db.po_form.id == po_number).update(freight=freight)

    def editpo_reset_approval_and_status(self, po_number):
        exec(f'resetApprovalsAndStatus({po_number})', self.globals)

    def editpo_vendor_lookup(self, vendor):
        self.globals['request']['_vars']['vendor'] = vendor
        exec(f'vendor_info_as_json_string=vendorLookup()', self.globals)
        return self.globals['vendor_info_as_json_string']

    def editpo_calculate_new_totals(self, po_number):
        exec(f'new_totals=calculate_new_totals({po_number})', self.globals)
        return self.globals['new_totals']

    def editpo_verify_editability(self, po_number=None):
        if po_number:
            self.globals['request']['_vars']['po_number'] = po_number
        exec('editable, error_string=(verifyEditability())', self.globals)
        return self.globals['editable'], self.globals['error_string']

    def tearDown(self):
        self.globals['request']['_vars']['po_number'] = None
        super().tearDown()

class TestEditOwnPO(EditPOControllerTest):
    USER = 'submitter'
    PASSWORD = 'password'
    ROLE = 'Submitter'
    DEPARTMENT = 'Feeds/Pet Foods'

    def test_unable_to_edit_no_po(self):
        editable, error_string = self.editpo_verify_editability()
        self.assertFalse(editable)
        self.assertTrue(error_string == 'No PO Number Provided' or error_string.startswith('Invalid PO #'))

    def test_unable_to_edit_invalid_po(self):
        editable, error_string = self.editpo_verify_editability()
        self.assertFalse(editable)
        self.assertEqual(error_string, 'No PO Number Provided')

    def test_can_edit_own_po_created_state(self):
        po_number = self._helper_createpo()
        editable, error_string = self.editpo_verify_editability(po_number)
        self.assertTrue(editable)
        self.assertEqual(error_string, '')
    
    def test_can_edit_own_po_created_state(self):
        po_number = self._helper_createpo()

        self._helper_set_po_status(po_number=po_number, po_status='Created')

        editable, error_string = self.editpo_verify_editability(po_number)
        self.assertTrue(editable)
        self.assertEqual(error_string, '')

    def test_can_edit_own_po_submitted_state(self):
        po_number = self._helper_createpo()

        self._helper_set_po_status(po_number=po_number, po_status='Submitted')

        editable, error_string = self.editpo_verify_editability(po_number)
        self.assertTrue(editable)
        self.assertEqual(error_string, '')

    def test_can_edit_own_po_approved_state(self):
        po_number = self._helper_createpo()

        self._helper_set_po_status(po_number=po_number, po_status='Approved')

        editable, error_string = self.editpo_verify_editability(po_number)
        self.assertTrue(editable)
        self.assertEqual(error_string, '')

    def test_can_edit_own_po_ordered_state(self):
        po_number = self._helper_createpo()

        self._helper_set_po_status(po_number=po_number, po_status='Ordered')

        editable, error_string = self.editpo_verify_editability(po_number)
        self.assertTrue(editable)
        self.assertEqual(error_string, '')

    def test_unable_edit_other_users_po_closed_state(self):
        po_number = self._helper_createpo()

        self._helper_set_po_status(po_number=po_number, po_status='Closed')

        editable, error_string = self.editpo_verify_editability(po_number)
        self.assertFalse(editable)
        self.assertTrue('is not editable' in error_string)

    def test_unable_to_edit_other_users_po_rejected_state(self):
        po_number = self._helper_createpo()

        self._helper_set_po_status(po_number=po_number, po_status='Rejected')

        editable, error_string = self.editpo_verify_editability(po_number)
        self.assertTrue(editable)
        self.assertEqual(error_string, '')

class TestEditOtherUsersInDepartmentPO(EditPOControllerTest):
    USER = 'submitter'
    PASSWORD = 'password'
    ROLE = 'Submitter'
    DEPARTMENT = 'Feeds/Pet Foods'

    NEW_USER = 'supervisor'
    NEW_PASSWORD = 'password'
    NEW_ROLE = 'Supervisor'
    NEW_DEPARTMENT = DEPARTMENT

    def _helper_switch_user(self):
        self.web2py_register(self.NEW_USER, self.NEW_PASSWORD)
        self.web2py_remove_role(self.USER, self.ROLE, self.DEPARTMENT)
        self.web2py_set_role(self.NEW_USER, self.NEW_ROLE, self.DEPARTMENT)

        self.web2py_logout()
        self.web2py_login(self.NEW_USER, self.NEW_PASSWORD)

    def test_unable_to_edit_other_users_po_created_state(self):
        po_number = self._helper_createpo()

        self._helper_set_po_status(po_number=po_number, po_status='Created')
        self._helper_switch_user()

        editable, error_string = self.editpo_verify_editability(po_number)
        self.assertFalse(editable)
        self.assertTrue('is not editable' in error_string)

    def test_unable_to_edit_other_users_po_submitted_state(self):
        po_number = self._helper_createpo()

        self._helper_set_po_status(po_number=po_number, po_status='Submitted')
        self._helper_switch_user()

        editable, error_string = self.editpo_verify_editability(po_number)
        self.assertFalse(editable)
        self.assertTrue('is not editable' in error_string)

    def test_unable_to_edit_other_users_po_approved_state(self):
        po_number = self._helper_createpo()

        self._helper_set_po_status(po_number=po_number, po_status='Approved')
        self._helper_switch_user()

        editable, error_string = self.editpo_verify_editability(po_number)
        self.assertFalse(editable)
        self.assertTrue('is not editable' in error_string)

    def test_unable_to_edit_other_users_po_ordered_state(self):
        po_number = self._helper_createpo()

        self._helper_set_po_status(po_number=po_number, po_status='Ordered')
        self._helper_switch_user()

        editable, error_string = self.editpo_verify_editability(po_number)
        self.assertTrue(editable)
        self.assertFalse(error_string)

    def test_unable_to_edit_other_users_po_closed_state(self):
        po_number = self._helper_createpo()

        self._helper_set_po_status(po_number=po_number, po_status='Closed')
        self._helper_switch_user()

        editable, error_string = self.editpo_verify_editability(po_number)
        self.assertFalse(editable)
        self.assertTrue('is not editable' in error_string)

    def test_unable_to_edit_other_users_po_rejected_state(self):
        po_number = self._helper_createpo()

        self._helper_set_po_status(po_number=po_number, po_status='Rejected')
        self._helper_switch_user()

        editable, error_string = self.editpo_verify_editability(po_number)
        self.assertFalse(editable)
        self.assertTrue('is not editable' in error_string)

class TestEditOtherUsersOutsideDepartmentPO(EditPOControllerTest):
    USER = 'submitter'
    PASSWORD = 'password'
    ROLE = 'Submitter'
    DEPARTMENT = 'Feeds/Pet Foods'

    NEW_USER = 'supervisor'
    NEW_PASSWORD = 'password'
    NEW_ROLE = 'Supervisor'
    NEW_DEPARTMENT = 'IT'

    def _helper_switch_user(self):
        self.web2py_register(self.NEW_USER, self.NEW_PASSWORD)
        self.web2py_set_role(self.NEW_USER, self.NEW_ROLE, self.NEW_DEPARTMENT)

        self.web2py_logout()
        self.web2py_login(self.NEW_USER, self.NEW_PASSWORD)

    def test_unable_to_edit_other_users_outside_department_po_created_state(self):
        po_number = self._helper_createpo()

        self._helper_set_po_status(po_number=po_number, po_status='Created')
        self._helper_switch_user()

        editable, error_string = self.editpo_verify_editability(po_number)
        self.assertFalse(editable)
        self.assertTrue('is not editable' in error_string)

    def test_unable_to_edit_other_users_outside_department_po_submitted_state(self):
        po_number = self._helper_createpo()

        self._helper_set_po_status(po_number=po_number, po_status='Submitted')
        self._helper_switch_user()

        editable, error_string = self.editpo_verify_editability(po_number)
        self.assertFalse(editable)
        self.assertTrue('is not editable' in error_string)

    def test_unable_to_edit_other_users_outside_department_po_approved_state(self):
        po_number = self._helper_createpo()

        self._helper_set_po_status(po_number=po_number, po_status='Approved')
        self._helper_switch_user()

        editable, error_string = self.editpo_verify_editability(po_number)
        self.assertFalse(editable)
        self.assertTrue('is not editable' in error_string)

    def test_unable_to_edit_other_users_outside_department_po_ordered_state(self):
        po_number = self._helper_createpo()

        self._helper_set_po_status(po_number=po_number, po_status='Ordered')
        self._helper_switch_user()

        editable, error_string = self.editpo_verify_editability(po_number)
        self.assertFalse(editable)
        self.assertTrue('is not editable' in error_string)

    def test_unable_to_edit_other_users_outside_department_po_closed_state(self):
        po_number = self._helper_createpo()

        self._helper_set_po_status(po_number=po_number, po_status='Closed')
        self._helper_switch_user()

        editable, error_string = self.editpo_verify_editability(po_number)
        self.assertFalse(editable)
        self.assertTrue('is not editable' in error_string)

    def test_unable_to_edit_other_users_outside_department_po_rejected_state(self):
        po_number = self._helper_createpo()

        self._helper_set_po_status(po_number=po_number, po_status='Rejected')
        self._helper_switch_user()

        editable, error_string = self.editpo_verify_editability(po_number)
        self.assertFalse(editable)
        self.assertTrue('is not editable' in error_string)

class TestCalculateNewTotals(EditPOControllerTest):
    USER = 'submitter'
    PASSWORD = 'password'
    ROLE = 'Submitter'
    DEPARTMENT = 'Feeds/Pet Foods'
    TAX_PERCENT = 7
    FREIGHT = 19.99

    def test_verify_new_totals(self):
        po_number = self._helper_createpo()

        item1 = self.TEST_ITEM_99_99
        item2 = self.TEST_ITEM_100_01
        item3 = self.TEST_ITEM_5000_01
        item4 = self.TEST_ITEM_50000_01

        self._helper_add_item(po_number, **item1)
        self._helper_add_item(po_number, **item2)
        self._helper_add_item(po_number, **item2)
        self._helper_add_item(po_number, **item3)
        self._helper_add_item(po_number, **item3)
        self._helper_add_item(po_number, **item3)
        self._helper_add_item(po_number, **item4)
        self._helper_add_item(po_number, **item4)
        self._helper_add_item(po_number, **item4)
        self._helper_add_item(po_number, **item4)

        self._helper_add_tax(po_number, self.TAX_PERCENT)
        self._helper_add_freight(po_number, self.FREIGHT)

        compare_subtotal = \
            (item1['price_per_unit'] * 1) + \
            (item2['price_per_unit'] * 2) + \
            (item3['price_per_unit'] * 3) + \
            (item4['price_per_unit'] * 4)

        compare_tax = compare_subtotal * (self.TAX_PERCENT / 100)

        new_totals = self.editpo_calculate_new_totals(po_number)
        self.assertEqual(new_totals['freight'], self.FREIGHT)
        self.assertEqual(new_totals['subtotal'], compare_subtotal)
        self.assertEqual(new_totals['tax'], compare_tax)
        self.assertEqual(new_totals['total'], self.FREIGHT + compare_subtotal + compare_tax)

class TestResetApprovalAndStatus(EditPOControllerTest):
    # TODO We need to handle editing an ordered po as well
    USER = 'submitter'
    PASSWORD = 'password'
    ROLE = 'Submitter'
    DEPARTMENT = 'Feeds/Pet Foods'

    def setUp(self):
        super().setUp()
        self.PO_NUMBER = self._helper_createpo()
        db = self.globals['db']
        self.CREATED_STATUS_ID = db(db.po_status_reference.status == 'Created').select().first().id
        self.MODIFIED_NOTE = 'Modified After Submission'

    def test_modify_created_po(self):
        self._helper_set_po_status(self.PO_NUMBER, 'Created')
        self.editpo_reset_approval_and_status(self.PO_NUMBER)

        db = self.globals['db']
        new_po_status = db(db.po_form.id == self.PO_NUMBER).select().first().status
        self.assertEqual(new_po_status, self.CREATED_STATUS_ID)

    def test_modify_submitted_po(self):
        self._helper_set_po_status(self.PO_NUMBER, 'Submitted')
        self.editpo_reset_approval_and_status(self.PO_NUMBER)

        db = self.globals['db']
        new_po_status = db(db.po_form.id == self.PO_NUMBER).select().first().status
        most_recent_po_note = db(db.po_note.po_form == self.PO_NUMBER).select(orderby=~db.po_note.id).first()
        approval_counts = db((db.po_note.po_form == self.PO_NUMBER) & (db.po_note.po_approval == True)).count()

        self.assertEqual(new_po_status, self.CREATED_STATUS_ID)
        self.assertEqual(most_recent_po_note.comment, self.MODIFIED_NOTE)
        self.assertTrue(approval_counts == 0)

    def test_modify_approved_po(self):
        self._helper_set_po_status(self.PO_NUMBER, 'Approved')
        # We should make sure there are no approvals on there either. We aren't adding any currently
        self.editpo_reset_approval_and_status(self.PO_NUMBER)

        db = self.globals['db']
        new_po_status = db(db.po_form.id == self.PO_NUMBER).select().first().status
        most_recent_po_note = db(db.po_note.po_form == self.PO_NUMBER).select(orderby=~db.po_note.id).first()
        approval_counts = db((db.po_note.po_form == self.PO_NUMBER) & (db.po_note.po_approval == True)).count()

        self.assertEqual(new_po_status, self.CREATED_STATUS_ID)
        self.assertEqual(most_recent_po_note.comment, self.MODIFIED_NOTE)
        self.assertTrue(approval_counts == 0)


    def test_modify_rejected_po(self):
        self._helper_set_po_status(self.PO_NUMBER, 'Rejected')
        self.editpo_reset_approval_and_status(self.PO_NUMBER)

        db = self.globals['db']
        new_po_status = db(db.po_form.id == self.PO_NUMBER).select().first().status
        most_recent_po_note = db(db.po_note.po_form == self.PO_NUMBER).select(orderby=~db.po_note.id).first()
        approval_counts = db((db.po_note.po_form == self.PO_NUMBER) & (db.po_note.po_approval == True)).count()

        self.assertEqual(new_po_status, self.CREATED_STATUS_ID)
        self.assertEqual(most_recent_po_note.comment, self.MODIFIED_NOTE)
        self.assertTrue(approval_counts == 0)

class TestVendorLookup(EditPOControllerTest):
    def test_existing_vendor_lookup(self):
        db = self.globals['db']
        VENDOR_NAME = 'vendor1'
        PHONE_NUM='18001112222'
        ACCOUNT_NUM=1
        CONTACT_PERSON='some person'
        db.vendor.insert(
            name=VENDOR_NAME,
            phone_num=PHONE_NUM,
            account_num=ACCOUNT_NUM,
            contact_person=CONTACT_PERSON
        )
        vendor_info = json.loads(self.editpo_vendor_lookup(VENDOR_NAME))
        self.assertEqual(vendor_info['vendor_name'], VENDOR_NAME)
        self.assertEqual(vendor_info['phone_num'], PHONE_NUM)
        self.assertEqual(vendor_info['account_num'], ACCOUNT_NUM)
        self.assertEqual(vendor_info['contact_person'], CONTACT_PERSON)

    def test_nonexisting_vendor_lookup(self):
        vendor_info = json.loads(self.editpo_vendor_lookup('vendor1'))
        self.assertEqual(vendor_info['vendor_name'], 'vendor1')
        self.assertEqual(vendor_info['phone_num'], '')
        self.assertEqual(vendor_info['account_num'], '')
        self.assertEqual(vendor_info['contact_person'], '')

tests = [
    TestEditOwnPO('test_unable_to_edit_no_po'),
    TestEditOwnPO('test_unable_to_edit_invalid_po'),
    TestEditOwnPO('test_can_edit_own_po_created_state'),
    TestEditOwnPO('test_can_edit_own_po_created_state'),
    TestEditOwnPO('test_can_edit_own_po_submitted_state'),
    TestEditOwnPO('test_can_edit_own_po_approved_state'),
    TestEditOwnPO('test_can_edit_own_po_ordered_state'),
    TestEditOwnPO('test_unable_edit_other_users_po_closed_state'),
    TestEditOwnPO('test_unable_to_edit_other_users_po_rejected_state'),
    TestEditOtherUsersInDepartmentPO('test_unable_to_edit_other_users_po_created_state'),
    TestEditOtherUsersInDepartmentPO('test_unable_to_edit_other_users_po_submitted_state'),
    TestEditOtherUsersInDepartmentPO('test_unable_to_edit_other_users_po_approved_state'),
    TestEditOtherUsersInDepartmentPO('test_unable_to_edit_other_users_po_ordered_state'),
    TestEditOtherUsersInDepartmentPO('test_unable_to_edit_other_users_po_closed_state'),
    TestEditOtherUsersInDepartmentPO('test_unable_to_edit_other_users_po_rejected_state'),
    TestEditOtherUsersOutsideDepartmentPO('test_unable_to_edit_other_users_outside_department_po_created_state'),
    TestEditOtherUsersOutsideDepartmentPO('test_unable_to_edit_other_users_outside_department_po_submitted_state'),
    TestEditOtherUsersOutsideDepartmentPO('test_unable_to_edit_other_users_outside_department_po_approved_state'),
    TestEditOtherUsersOutsideDepartmentPO('test_unable_to_edit_other_users_outside_department_po_ordered_state'),
    TestEditOtherUsersOutsideDepartmentPO('test_unable_to_edit_other_users_outside_department_po_closed_state'),
    TestEditOtherUsersOutsideDepartmentPO('test_unable_to_edit_other_users_outside_department_po_rejected_state'),
    TestCalculateNewTotals('test_verify_new_totals'),
    TestResetApprovalAndStatus('test_modify_created_po'),
    TestResetApprovalAndStatus('test_modify_submitted_po'),
    TestResetApprovalAndStatus('test_modify_approved_po'),
    TestResetApprovalAndStatus('test_modify_rejected_po'),
    TestVendorLookup('test_existing_vendor_lookup'),
    TestVendorLookup('test_nonexisting_vendor_lookup'),
]

suite = unittest.TestSuite()
[suite.addTest(test) for test in tests]

unittest.TextTestRunner(verbosity=2).run(suite)