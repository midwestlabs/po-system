import sys
import os
import traceback
import glob
import argparse

WEB2PY_PATH = '/opt/web2py/'
APPLICATION_NAME = 'po'
sys.path.insert(1, WEB2PY_PATH)
from gluon import shell

parser = argparse.ArgumentParser('web2py unittest runner')
parser.add_argument('-p', '--web2py_path', help='Path to web2py')
parser.add_argument('-a', '--application', help='Name of the application to test')

def showfeedback():
    exc_type, exc_value, exc_traceback = sys.exc_info()
    print('-'*60)
    for line in traceback.format_exception(exc_type, exc_value, exc_traceback):
        print(line[:-1])
    print('-'*60)

def run_test(test_file):
    if os.name == 'nt':
        errlist = (WindowsError, ValueError, SystemExit)
    else:
        errlist = (OSError, ValueError, SystemExit)

    try:
        # to support imports form current folder in the testfiles
        sys.path.append(os.path.split(test_file)[0])
        # modules are applications/[appname]/modules
        modules_path = os.path.join('applications', APPLICATION_NAME, 'modules')
        # to support imports from modules folder
        sys.path.append(modules_path)
        # support imports from web2py/site-packages
        sys.path.append('site-packages')
        exec(compile(open(test_file,'rb').read(), test_file, 'exec'), globals())

        # PREFERRED
        # Allowing gluon to execute this for us
        # This is currently broken due to the following error on execution
        """
         File "/home/miversen/web2py/gluon/restricted.py", line 219, in restricted
            exec(ccode, environment)
        File "applications/po/models/0_init.py", line 21, in <module>
            if request.global_settings.web2py_version < "2.15.5":
        TypeError: '<' not supported between instances of 'NoneType' and 'str'
        """
        # os.chdir(WEB2PY_PATH)
        # shell.run(
        #     appname=APPLICATION_NAME,
        #     startfile=test_file,
        #     import_models=True
        # )
    except errlist:
        pass  # we know about the rotating logger error...
        # and SystemExit is not useful to detect
    except:
        showfeedback()

path = os.path.join(WEB2PY_PATH, 'applications', APPLICATION_NAME, 'tests', '*.py')
tests = [file for file in glob.glob(path) if file.endswith('test.py') or (file.startswith('test') and file.endswith('.py'))]

path = os.path.join(WEB2PY_PATH, 'applications', APPLICATION_NAME, 'controllers', '*.py')
doc_tests = glob.glob(path)

if not tests:
    print(f'No Unittests found for {APPLICATION_NAME}')
# else:
#     print(f'Found the following unittests: {tests}')
if not doc_tests:
    print(f'No Doctests found for {APPLICATION_NAME}')
# else:
#     print(f'Found the following files to doctest: {doc_tests}')

for test in tests:
    run_test(test)


# if __name__ == '__main__':
#     # Not in use yet, we have to fix the gluon error first
#     args = parser.parse_args()