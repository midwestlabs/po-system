# -*- coding: utf-8 -*-
from button import BUTTON
from generate_attachments_view import UPLOAD_TYPES as ATTACHMENT_KEYS

### required - do no delete
def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires

@auth.requires_login()
def index():
    """
    This is the "landing" page for our manageUserPO controller. This page will handle most of the work flow in a PO. 
    Here is where we do the logic to generate which buttons should be appearing for a user to interact with.

    :usage: https://yourURL/po/manageUserPO/index?po_number=somepo_number

    :requires: user must be logged into the system

    :param po_number: A po number must be provided to view. The PO number doesn't _have_ to be valid, however if an invalid PO number is received, an 
    error is flashed and the user is returned to the default page.

    :returns dict(header, poForm): This function returns a dictionary with a header (page header) and an uneditable SQLFORM with the PO information in it
    """
    poError = checkPoNumber()
    if poError:
        response.flash = poError
        return dict(header="", poForm="")
    
    po_number = request.vars.po_number
    po = db(db.po_form.id == po_number).select().first()
    readablePoState = "View"
    readablePoSubmitter = db(db.auth_user.id == po.submitter).select(db.auth_user.first_name, db.auth_user.last_name).first()

    buttons = get_buttonbar(po)
    poForm = SPAN(
        *buttons,
        LOAD(c="viewSinglePO", f='index.load', vars=dict(po_number=po_number, editable=False))
        ,
    )
    header = f"{readablePoState} PO # {po_number} For {readablePoSubmitter.first_name} {readablePoSubmitter.last_name}"

    return dict(header=header, poForm = poForm)

def get_buttonbar(po):
    """
    A method that returns an array of buttons which can be interacted with by the user. The available buttons vary
    based on user's role and the status of the PO

    :usage get_buttonbar(po):

    :requires: None

    :param po: Parameter should be a row from db. Generally, this is retrieved via db(db.po_form.id == somepo_number).select().first()

    :returns [button1, button2, ... buttonX]: This function returns an array of variable length containing all available buttons for the provided PO
    """

    # TODO organize/clean this
    po_number = po.id
    isEditable = isPoEditable(po_number)

    poState = db(po.status == db.po_status_reference.id).select(db.po_status_reference.id).first().id
    poDepartment = getPoDepartment(po_number)
    
    """
        This should be filled with the buttons relevant to both the user and state of the PO

        For example, this should have the "Submit PO, Edit PO" button, if the user is the owner of the PO, and the PO state is Created
        The buttons list should have "Delete PO" button if the user is owner of the PO, or approver (and the state is submitted), or superuser
        The buttons list should have "Approve PO, Reject PO, Close PO" if user is department approver or superuser
        "Clone PO" should be available to anyone viewing the PO
    """
    buttonArray = []

    DELETE_BUTTON_MESSAGE = f"Are You Sure You Want To Delete PO #{po_number}"
    ORDER_BUTTON_MESSAGE = f"Placing PO #{po_number} In Ordered Status Will Lock The PO. Are You Sure You Want To Do This?"
    RECEIVE_BUTTON_MESSAGE = 'This will finalize the PO, marking it for Accounting to close out. Are you sure you want to do this?'
    CLOSE_BUTTON_MESSAGE = f"Are You Sure You Want To Close PO# {po_number}?"

    deleteButton = BUTTON(text="Delete PO", _onclick=
        f"if(confirm('{DELETE_BUTTON_MESSAGE}')){{" + \
        f"    window.location='{URL(c='manageUserPO', f='processDeletePO', vars=dict(po_number=po_number))}';" + \
        "}else{" + \
        f"    window.location='{URL(c='manageUserPO', f='index', vars=dict(po_number=po_number))}';}}",
        user_signature=True,
    )

    orderedButton = BUTTON(text="Ordered PO", _onclick=
        f"if(confirm('{ORDER_BUTTON_MESSAGE}')){{" + \
        f"    window.location='{URL(c='manageUserPO', f='processOrderPO', vars=dict(po_number=po_number))}';" + \
        "}else{" + \
        f"    window.location='{URL(c='manageUserPO', f='index', vars=dict(po_number=po_number))}';}}",
        user_signature=True,
    )

    closeButton = BUTTON(text="Close PO", _onclick=
        f"if(confirm('{CLOSE_BUTTON_MESSAGE}')){{" + \
        f"    window.location='{URL(c='manageUserPO', f='processClosePO', vars=dict(po_number=po_number))}';" + \
        "}else{" + \
        f"    window.location='{URL(c='manageUserPO', f='index', vars=dict(po_number=po_number))}';}}",
        user_signature=True,
    )

    exportButton = BUTTON(text="Download/Print", _onclick=
        f"""window.location='{URL(c="viewSinglePO", f="index", vars=dict(po_number=po_number, print=True))}'"""
    )

    receivedButton = BUTTON(text="Received PO", _onclick=
        f"if(confirm('{RECEIVE_BUTTON_MESSAGE}')){{" + \
        f"    window.location='{URL(c='manageUserPO', f='processReceivePO', vars=dict(po_number=po_number))}';" + \
        "}else{" + \
        f"    window.location='{URL(c='manageUserPO', f='index', vars=dict(po_number=po_number))}';}}",
        user_signature=True,)

    buttonArray.append(BUTTON(text="Back", _onclick=f"window.history.back();", user_signature=True))
    if poDepartment is not None:
        if auth.has_permission('submitter', 'any'):
            buttonArray.append(BUTTON(text="Clone PO", _onclick=f"""window.location='{URL(c="createPO", f="index", vars=dict(po_number=po_number))}';""", user_signature=True,))

        if (po.submitter != auth.user.id) and auth.has_permission('supervisor', poDepartment) and (poState == db(db.po_status_reference.status == "Submitted").select(db.po_status_reference.id).first().id): 
            buttonArray.append(BUTTON(text="Approve PO", _onclick=f"""window.location='{URL(c="manageUserPO", f="processApprovePO", vars=dict(po_number=po_number))}';""", user_signature=True,))
            buttonArray.append(BUTTON(text="Reject PO", _onclick=f"""window.location='{URL(c="manageUserPO", f="processRejectPO", vars=dict(po_number=po_number))}';""", user_signature=True,))
            buttonArray.append(closeButton)
    
    if isEditable:
        # TODO consider having a confirmation on edit if the PO is already approved
        buttonArray.append(BUTTON(text="Edit PO", _onclick=f"""window.location='{URL(c="editPO", f="index", vars=dict(po_number=po_number))}';""", user_signature=True,))
        if ((po.submitter == auth.user.id) and 
                ((po.status == db((db.po_status_reference.status == "Created") | (db.po_status_reference.status == "Rejected")).select(db.po_status_reference.id).first().id))):
            buttonArray.append(BUTTON(text="Submit PO", _onclick=f"""window.location='{URL(c="manageUserPO", f="processSubmittedPO", vars=dict(po_number=po_number))}';""", user_signature=True,))
            if (auth.user.id == po.submitter) and (po.status <= db(db.po_status_reference.status == "Approved").select(db.po_status_reference.id).first().id):
                buttonArray.append(deleteButton)
        if auth.has_permission('superuser', 'any') and (auth.user.id != po.submitter) and deleteButton not in buttonArray:
            buttonArray.append(deleteButton)
        if (po.submitter == auth.user.id) and (po.status == db(db.po_status_reference.status == "Approved").select(db.po_status_reference.id).first().id):
            buttonArray.append(orderedButton)

    createdState = db(db.po_status_reference.status == "Created").select(db.po_status_reference.id).first().id
    closedState = db(db.po_status_reference.status == "Closed").select(db.po_status_reference.id).first().id

    if not _unable_to_receive_po(po_number):
        buttonArray.append(receivedButton)

    if (poState != createdState and poState != closedState) and (auth.has_permission('accounting', 'any')) and closeButton not in buttonArray:
        buttonArray.append(closeButton)

    if poState == db(db.po_status_reference.status == "Ordered").select(db.po_status_reference.id).first().id:
        # Making the following fields visible because the PO is in an ordered state
        db.po_form.order_confirmation_num.readable=True
        db.po_form.tracking_number.readable=True
        db.po_form.shipping_company.readable=True
    
    buttons = SPAN(_class="row_buttons")
    for button in buttonArray:
        buttons.append(button)
    buttons.append(exportButton)

    return buttons


@auth.requires_login()
def checkPoNumber(displayError=None, redirectLinkOnError=None):
    """
    Performs validity check on po_number. Note that you do not pass po_number as a parameter. That is because we are also checking to 
    see if the po_number is attached to the request.

    :usage checkPoNumber(displayError=None, redirectLinkOnError=URL(c="viewPOForms", f="index")):

    :requires: user must be logged in

    :param displayError (optional): A string error message to be flashed on the screen if the po number fails vthe validity check. If not provided, default strings are used
    based on the type of failure
    :param redirectLinkOnError (optional): A link to redirect the user to upon po validity failure. 
    If not provided, user is redirected to https://yourURL/po/viewPOForms/index

    :returns None: If the PO is valid, None is returned. 
    If the PO is not valid, the user is redirected to the redirectLinkOnError, and the displayError is flashed on the screen 
    """
    validPo = True
    if not redirectLinkOnError:
        redirectLinkOnError = URL(c="viewPOForms", f="index")
    if not request.vars.po_number:
        if displayError is None:
            displayError="No PO Number Provided"
        validPo = False
    if db(db.po_form.id == request.vars.po_number).isempty():
        if displayError is None:
            displayError=f"Invalid PO #{request.vars.po_number}"
        validPo = False

    if not validPo:
        session.flash = displayError
        redirect(redirectLinkOnError)


@auth.requires_login()
def isPoEditable(po_number):
    """
    Checks the state of the PO by the provided po_number, to determine if the PO can be edited or not

    :usage isPoEditable(po_number):

    :requires: user must be logged in

    :param po_number: must be a valid PO number. It is expected that this is ran after checkPoNumber()

    :returns Boolean: If the PO is editable, returns True, else returns False. 
    """
    po = db(db.po_form.id == po_number).select().first()
    current_state = db(db.po_status_reference.id == po.status).select(db.po_status_reference.status).first().status
    if (
            (current_state == 'Created' and po.submitter != auth.user_id)
            or (current_state in ['Closed', 'Received'])
    ):
        return False
    
    department = db(db.department.id == po.department).select(db.department.name).first()
    if not department:
        department = ''
    else:
        department = department.name.replace(' ', '_').lower()

    ordered_state = db(db.po_status_reference.status == 'Ordered').select().first().id
    po = db(db.po_form.id == po_number).select().first()
    if po.status < ordered_state and po.submitter == auth.user_id:
        return True
    user_is_owner_permission = auth.user is None
    user_not_submitter_permission = not auth.has_permission('submitter', 'any')
    user_has_relevant_granular_permission = False
    for key in ATTACHMENT_KEYS:
        if user_has_relevant_granular_permission:
            break
        _k = key.replace(' ', '_').lower()
        _s = current_state.lower()
        base_permission_check = auth.has_permission(f'{_k}_{_s}', 'any')
        base_permission_check_with_department = auth.has_permission(f'{_k}_{_s}', department)
        alt_permission_check = auth.has_permission(f'upload_{_k}_{_s}', 'any')
        alt_permission_check_with_department = auth.has_permission(f'upload_{_k}_{_s}', department)
        if base_permission_check or base_permission_check_with_department or alt_permission_check or alt_permission_check_with_department:
            user_has_relevant_granular_permission = True

    permission_checks = [
        user_is_owner_permission,
        user_not_submitter_permission,
        user_has_relevant_granular_permission
    ]
    failed_check = True
    for permission_check in permission_checks:
        if permission_check:
            failed_check = False
            break
    
    return not failed_check

@auth.requires_login()
def getPoDepartment(po_number):
    """
    Gets the string department for a PO by the provied po_number.

    :usage getPoDepartment(po_number):

    :requires: user must be logged in

    :param po_number: must be a valid PO number. It is expected that this is ran after checkPoNumber()

    :returns department_name: If po has a department name associated with it, it is returned in string form. Otherwise, None is returned

    """
    poDepartment = db((db.po_form.id == po_number) & (db.po_form.department == db.department.id)).select(db.department.name).first()
    if poDepartment is None:
        return None
    return poDepartment.name

@auth.requires(auth.has_permission('submitter', 'any'), requires_login=True)
def processSubmittedPO():
    """
    Handles the submission process of a PO via the following process
    - Checks to ensure the PO is a valid PO
    - Validates the PO can be submitted in general, as well as submitted by the user
    - Maps the PO to its appropriate rule
    - Tells the engine to review the PO and notify the correct approvers

    :usage: https://yourURL/po/manageUserPO/processSubmittedPO?po_number=somepo_number

    :requires: user must be a submitter in any department and logged in

    :param po_number: Can be any po number, valid or not. Method checks to ensure this PO is a legit PO, before processing submission

    :returns None: Upon success or failure, the user is redirected to https://yourURL/po/viewPOForms/index
    """
    checkPoNumber(displayError="Failed To Submit PO: Invalid PO", redirectLinkOnError=URL(c="viewPOForms", f="index"))

    po_number = request.vars.po_number
    error_message = validateSubmittedPO(po_number)

    if error_message:
        session.flash = error_message
        redirect(URL(c="manageUserPO", f="index", vars=dict(po_number=po_number)))

    submittedStatus = db(db.po_status_reference.status == "Submitted").select(db.po_status_reference.id).first().id
    db.po_note.insert(po_form=po_number, made_by=auth.user, po_approval=False, comment='Submitted')
    db(db.po_form.id == po_number).update(status = submittedStatus, last_modified = request.now)

    auto_approval = mapSubmittedPO(po_number)
    if auto_approval:
        redirect(URL(c="manageUserPO", f="processApprovePO", vars=dict(po_number=po_number, approver="Auto Approver")))

    review_po(po_number, notify_approvers=True)
    redirect(URL(c="viewPOForms", f="index"))

def validateSubmittedPO(po_number):
    """
    Validates that the po_number provided is able to be submitted

    :usage: validateSubmittedPO(po_number)

    :requires: A valid PO number. We are assuming that that validity of the physical number has already been handled, as we are not verifying the PO exists and the number is appropriate. We are verifying the PO itself is acceptable to be submitted. To verify the number, please use the checkPoNumber method

    :param po_number: See requires for more details on the PO number requirements

    :returns error_message: Returns an error message (if we find the submission is not valid) or None
    """
    po = db(db.po_form.id == po_number).select().first()

    if po is None:
        return "Failed To Submit PO: Invalid PO"

    row_department = db(db.department.id == po.department).select().first()
    department = row_department.name if row_department is not None else ''

    if not department or not row_department:
        return "Failed To Submit PO: No Department Selected"

    if not auth.has_permission('submitter', row_department.name):
        return "Failed to Submit PO: Invalid Permission For Department"

    if db(db.po_item.po_form == po_number).count() < 1:
        return "Failed To Submit PO: No PO Items Found"

    if db(db.po_form.id == po_number).select(db.po_form.subtotal).first().subtotal <= 0:
        return "Failed To Submit PO: Subtotal Needs To Be Greater Than 0"


    if auth.user.id != po.submitter:
        return f"Failed to Submit PO: {auth.user.first_name} {auth.user.last_name} is not the owner of this PO"

def mapSubmittedPO(po_number):
    """
    Establishes the rule set for the PO

    :usage: mapSubmittedPO(po_number)

    :requires: A valid PO number

    :param po_number:

    :returns auto_approve: map_response will be either True if the rule is an "auto approval", and False otherwise

    :raises: An exception is raised if we are passed an invalid PO or we are unable to find a rule for the PO
    """
    if db(db.po_form.id == po_number).count() == 0:
        raise Exception('No PO Provided')

    db(db.po_rule_map.po_form == po_number).delete()

    # TODO Verify if max_role_heirarchy is still needed
    rule = db(
        (db.po_form.id == po_number) &
        (db.po_form.department == db.po_rule.department) &
        (db.po_rule.min_subtotal <= db.po_form.subtotal) &
        ((db.po_rule.max_subtotal >= db.po_form.subtotal) | (db.po_rule.max_subtotal == 0))
        ).select(db.po_rule.id, db.po_rule.min_subtotal).first()
    if not rule:
        raise Exception(f'No Rule Found For PO #{po_number}')
    db.po_rule_map.update_or_insert(po_form=po_number, po_rule=rule.id)
    # We should convert this to check if there are no required approvals instead of a 0 subtotal, as this allows
    # more flexibility
    return True if rule.min_subtotal == 0 else False

@auth.requires(auth.has_permission('submitter', 'any'), requires_login=True)
def processDeletePO():
    """
    Handles PO deletion process

    :usage: https://yourURL/po/manageUserPO/processDeletePO?po_number=somepo_number

    :requires: user is a submitter in any department and user is logged in

    :param po_number: Can be any po number, valid or not. Method checks to ensure this PO is a legit PO, before processing Deletion

    :returns None: Upon success or failure, the user is redirected to https://yourURL/po/viewPOForms/index
    """
    checkPoNumber(displayError="Failed To Delete PO: Invalid PO", redirectLinkOnError=URL(c="viewPOForms", f="index"))

    po_number = request.vars.po_number
    po = db(db.po_form.id == po_number).select().first()

    department = getPoDepartment(po_number)
    createdStatus = db(db.po_status_reference.status == "Created").select(db.po_status_reference.id).first().id
    approvedStatus = db(db.po_status_reference.status == "Approved").select(db.po_status_reference.id).first().id
    if ((auth.user.id == po.submitter and po.status == createdStatus) or (auth.has_permission('superuser', 'any'))):
        db(db.po_form.id == po_number).delete()
        session.flash = f"Deleted PO #{po_number}"
        redirect(URL(c="viewPOForms", f="index"))
    else:
        session.flash = f"You Do Not Have Permission To Delete PO #{po_number}"
        redirect(URL(c="manageUserPO", f="index", vars=dict(po_number=po_number)))

@auth.requires(auth.has_permission('submitter', 'any'), requires_login=True)
def processApprovePO():
    """
    Handles PO approval process via the following steps
    - Checks to ensure po_number is valid
    - Checks to ensure user has minimum supervisor permission on the department of the po being approved
    - Reviews the PO to determine if the PO is actually approved, and if the submitter should be informed of a state change

    :usage: https://yourURL/po/manageUserPO/processApprovePO?po_number=somepo_number or https://yourURL/po/manageUserPO/processApprovePO?po_number=somepo_number&approver="Auto Approver"

    :requires: user is a submitter in any department and user is logged in. The reason this requires submitter and 
    not supervisor or higher is because the users credentials are passed during an autoapproval. If the user is just
    a submitter, their PO is not auto approved as they are denied the ability to enter this method

    :param po_number: Can be any po number, valid or not. Method checks to ensure this PO is a legit PO, before processing Approval
    :param approver (optional): An optional param that can be passed to tell the system the specific approver of this PO. Currently only used for auto approval

    :returns None: Upon success or failure, the user is redirected to https://yourURL/po/viewPOForms/index
    """

    checkPoNumber(displayError="Failed To Approve PO: Invalid PO", redirectLinkOnError=URL(c="viewPOForms", f="index"))

    po_number = request.vars.po_number
    po = db.po_form(po_number)
    department = db.department(po.department)
    submitted_status = db(db.po_status_reference.status == 'Submitted').select(db.po_status_reference.id).first().id

    if po.status != submitted_status:
        session.flash = f"You Do Not Have Permission To Approve PO #{po_number}"
        redirect(URL(c="manageUserPO", f="index", vars=dict(po_number=po_number)))

    approver = request.vars.approver
    if approver is None and not auth.has_permission('supervisor', department.name):
        session.flash = f"You Do Not Have Permission To Approve PO #{po_number}"
        redirect(URL(c="manageUserPO", f="index", vars=dict(po_number=po_number)))

    if approver is not None:
        auto_approver = db(db.auth_user.id == 0).select(db.auth_user.id)
        if len(auto_approver) == 0:
            raise Exception('Auto Approver Not Found!')
        approver = auto_approver.first().id
    else:
        # TODO We should probably put a check here to make sure we should be using the auto approver
        approver = auth.user.id
    if approver == po.submitter:
        session.flash = f"You Do Not Have Permission To Approve PO #{po_number}"
        redirect(URL(c="manageUserPO", f="index", vars=dict(po_number=po_number)))

    approvedStatus = db(db.po_status_reference.status == "Approved").select(db.po_status_reference.id).first().id
    db.po_note.insert(po_form=po_number, made_by=approver, po_approval=True, comment='Approved')
    review_po(po_number, notify_approvers=True)
    redirect(URL(c="viewPOForms", f="index"))

@auth.requires(auth.has_permission('supervisor', 'any'), requires_login=True)
def processRejectPO():
    """
    Handles PO reject process

    :usage: https://yourURL/po/manageUserPO/processRejectPO?po_number=somepo_number

    :requires: user is a supervisor in any department and user is logged in

    :param po_number: Can be any po number, valid or not. Method checks to ensure this PO is a legit PO, before processing Approval

    :returns None: Upon success or failure, the user is redirected to https://yourURL/po/viewPOForms/index
    """
    checkPoNumber(displayError="Failed To Reject PO: Invalid PO", redirectLinkOnError=URL(c="viewPOForms", f="index"))

    po_number = request.vars.po_number

    po = db.po_form(po_number)
    department = db.department(po.department)
    if not auth.has_permission('supervisor', department.name):
        session.flash = f'You Do Not Have Permission To Reject PO #{po_number}'
        redirect(URL(c="manageUserPO", f="index", vars=dict(po_number=po_number)))

    rejectedStatus = db(db.po_status_reference.status == "Rejected").select(db.po_status_reference.id).first().id
    submittedStatus = db(db.po_status_reference.status == 'Submitted').select(db.po_status_reference.id).first().id
    if po.status != submittedStatus:
        session.flash = f'PO #{po_number} cannot be rejected as it is not currently submitted'
        redirect(URL(c="manageUserPO", f="index", vars=dict(po_number=po_number)))

    db(db.po_form._id == po_number).update(status = rejectedStatus, last_modified = request.now)
    db.po_note.insert(po_form=po_number, po_approval=False, comment='Rejected', made_by=auth.user.id)
    review_po(po_number, notify_approvers=False)
    redirect(URL(c="viewPOForms", f="index"))

@auth.requires(auth.has_permission('submitter', 'any'), requires_login=True)
def processOrderPO():
    """
    Handles PO ordered process

    :usage: https://yourURL/po/manageUserPO/processOrderPO?po_number=somepo_number

    :requires: user is a submitter in any department and user is logged in

    :param po_number: Can be any po number, valid or not. Method checks to ensure this PO is a legit PO, before processing Ordering State

    :returns None: Upon success or failure, the user is redirected to https://yourURL/po/viewPOForms/index
    """
    checkPoNumber(displayError="Failed To Order PO: Invalid PO", redirectLinkOnError=URL(c="viewPOForms", f="index"))

    po_number = request.vars.po_number
    po = db.po_form(po_number)
    department = db.department(po.department)

    if not auth.has_permission('submitter', department.name) or auth.user.id != po.submitter:
        session.flash = f'You Do Not Have Permission To Place PO #{po_number} Into An Ordered State'
        redirect(URL(c="manageUserPO", f="index", vars=dict(po_number=po_number)))

    approvedStatus = db(db.po_status_reference.status == 'Approved').select(db.po_status_reference.id).first().id
    if po.status != approvedStatus:
        session.flash = f'PO #{po_number} cannot be ordered as it has not been approved'
        redirect(URL(c="manageUserPO", f="index", vars=dict(po_number=po_number)))

    orderedStatus = db(db.po_status_reference.status == "Ordered").select(db.po_status_reference.id).first().id
    db(db.po_form._id == po_number).update(status = orderedStatus, last_modified = request.now)
    db.po_note.insert(po_form=po_number, po_approval=False, comment='Ordered', made_by=auth.user.id)
    redirect(URL(c="viewPOForms", f="index"))

@auth.requires_login()
def processReceivePO():
    '''
    Handles Marking PO as received

    :usage: https://yourURL/po/manageUserPO/processReceivePO?po_number=somepo_number

    :param po_number: Can be any po number, valid or not. Method checks to ensure this PO is a legit PO, before moving state

    :returns None: Upon success or failure, the user is redirected to https://yourURL/po/viewPOForms/index
    '''
    checkPoNumber(displayError="Failed To Receive PO: Invalid PO", redirectLinkOnError=URL(c="viewPOForms", f="index"))

    po_number = request.vars.po_number
    receivedStatus = db(db.po_status_reference.status == "Received").select(db.po_status_reference.id).first().id

    error = _unable_to_receive_po(po_number)
    if error:
        session.flash = error
        redirect(URL(c="manageUserPO", f="index", vars=dict(po_number=po_number)))

    db(db.po_form._id == po_number).update(status = receivedStatus, last_modified = request.now)
    db.po_note.insert(po_form=po_number, po_approval=False, comment='Received', made_by=auth.user.id)
    session.flash = f'Successfully Marked PO {po_number} As Received!'
    redirect(URL(c="viewPOForms", f="index"))

# This seems like a reasonable (ish?) design pattern? Maybe make a wrapper that calls these based on the state?
def _unable_to_receive_po(po_number) -> str:
    '''
    Performs validation check to see if we can NOT mark the PO as received

    :usage: _unable_to_receive_po(po_number)

    :param po_number: Must be a valid PO number, though the state of the PO doesn't matter

    :returns str: Returns an error to be displayed if the po CANNOT be put into received state. This seems a bit backwards
    but given how the rest of the code is, this fits how we do stuff
    '''
    po = db.po_form(po_number)
    response = None
    orderedStatus = db(db.po_status_reference.status == "Ordered").select(db.po_status_reference.id).first().id
    if po.department:
        department = db.department(po.department).name

        if not auth.has_permission('submitter', department) and auth.user.id != po.submitter:
            response = f'You Do Not Have Permission To Place PO #{po_number} Into An Received State'

    if po.status != orderedStatus:
        response = f'PO #{po_number} cannot be received as it has not been approved'

    return response

@auth.requires_login()
def processClosePO():
    """
    Handles PO Closure process

    :usage: https://yourURL/po/manageUserPO/processClosePO?po_number=somepo_number

    :requires: user is a submitter in any department and user is logged in

    :param po_number: Can be any po number, valid or not. Method checks to ensure this PO is a legit PO, before processing Closure

    :returns None: Upon success or failure, the user is redirected to https://yourURL/po/viewPOForms/index
    """
    checkPoNumber(displayError="Failed To Close PO: Invalid PO", redirectLinkOnError=URL(c="viewPOForms", f="index"))

    po_number = request.vars.po_number
    po = db.po_form(po_number)
    department = db.department(po.department).name
    receivedStatus = db(db.po_status_reference.status == "Received").select(db.po_status_reference.id).first().id
    submittedStatus = db(db.po_status_reference.status == "Submitted").select(db.po_status_reference.id).first().id
    closedStatus = db(db.po_status_reference.status == "Closed").select(db.po_status_reference.id).first().id

    if (
        (po.submitter != auth.user.id) and 
        (
            (auth.has_permission('superuser', 'any')) or 
            (auth.has_permission('supervisor', department) and po.status == submittedStatus) or 
            (auth.has_permission('accounting', 'any') and po.status == receivedStatus)
        )
    ):
        db(db.po_form._id == po_number).update(status = closedStatus, last_modified = request.now)
        db.po_note.insert(po_form=po_number, po_approval=False, comment='Closed', made_by=auth.user.id)
        session.flash = f'Closed PO #{po_number}'
        redirect(URL(c="viewPOForms", f="index"))
    else:
        session.flash = f'You Do Not Have Permission To Close PO #{po_number}'
        redirect(URL(c="manageUserPO", f="index", vars=dict(po_number=po_number)))
