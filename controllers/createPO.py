# -*- coding: utf-8 -*-

### required - do no delete
def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires

@auth.requires(auth.has_permission('submitter', 'any'), requires_login=True)
def index():
    """
    This is the "landing" page for our createPO controller. Simply navigating to this page will create a new PO for us, providing 
    us with a PO number. Additionally, you can pass a PO number to this controller and we will clone that PO to a new PO (ie, we will
    copy the provided PO's contents into a new PO)

    :usage: https://yourURL/po/createPO/ or https://yourURL/po/createPO/index?po_number=somepo_number

    :requires submitter permission in any department

    :params po_number (optional): A po number can be provided for use to clone/template from.

    :returns None: This function redirects the user to manageUserPO after it has successfully created a new PO
    """
    clonePo = request.vars.po_number
    poNum = cloneExistingPo(clonePo) if clonePo is not None else createNewPo()

    redirect(URL(c="editPO", f="index", vars=dict(po_number=poNum)))

@auth.requires(auth.has_permission('submitter', 'any'), requires_login=True)
def createNewPo(addCreationNote=True):
    """
    This method will create a new PO. Among the things that are done here, are the following
        - New PO state set to Created
        - Adds note to system that a new PO was created (if addCreationNote == True)

    :usage createNewPo() or createNewPo(addCreationNote=False):

    :requires submitter permission in any department

    :params addCreationNote (optional): A boolean that can be provided that adds a note about the POs creation. Setting this to false
    forgoes the note adding to the Database. You would set this to false if you are cloning a PO, since cloning the PO will
    already add a note to the database documenting which PO was cloned

    :returns po_number: Returns the newly created PO number, after the PO has been written to the database
    """
    poNumber = db.po_form.insert(
        status=db(db.po_status_reference.status == "Created").select().first().id,
        submitter=auth.user.id,
        last_modified=request.now,
    )
    if addCreationNote:
        db.po_note.insert(po_form=poNumber, made_by=auth.user.id, po_approval=False, comment="Created PO")
    return poNumber

@auth.requires(auth.has_permission('submitter', 'any'), requires_login=True)
def cloneExistingPo(poNumber):
    """
    Clones most of the contents of the provided PO to a new PO. Items not copied
    - Uploads (Contacts, quotes, etc)
    - Notes (We are creating a new PO so the notes do not transition over)
    :usage cloneExistingPo(po_number):

    :requires submitter permission in any department

    :params po_number: This should be the PO number we are looking to clone from

    :returns po_number: We return the PO number for the new PO we have created
    """
    existingPo = db(db.po_form.id == poNumber).select().first()
    existingPoItems = db(db.po_item.po_form == existingPo.id).select()
    newPo = db(db.po_form.id == createNewPo(addCreationNote=False)).select().first()
    poNumber = newPo.id

    # Cloning PO Items
    subtotal = existingPo.freight
    for item in existingPoItems:
        db.po_item.insert(po_form=poNumber, catalog=item.catalog, description=item.description, quantity=item.quantity, price_per_unit=item.price_per_unit, extended_price=item.extended_price)
        subtotal += item.extended_price

    tax = subtotal * (existingPo.tax_percent/100)
    total = subtotal + tax

    # Cloning PO
    db(db.po_form.id == poNumber).update(
        department=existingPo.department,
        vendor=existingPo.vendor,
        phone_num=existingPo.phone_num,
        account_num=existingPo.account_num,
        contact_person=existingPo.contact_person,
        project=existingPo.project,
        tax_percent=existingPo.tax_percent,
        on_vendor_approval_list=existingPo.on_vendor_approval_list,
        sole_source_purchase=existingPo.sole_source_purchase,
        subtotal=subtotal,
        tax=tax,
        freight=existingPo.freight,
        total=total
    )

    db.po_note.insert(po_form=poNumber, po_approval=False, made_by=auth.user.id, comment=f"Cloned PO From PO #{existingPo.id}")

    return poNumber