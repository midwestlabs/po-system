# -*- coding: utf-8 -*-

import json

import generate_attachments_view
from generate_attachments_view import get_uneditable_views, get_editable_views, get_attachments

### required - do no delete
def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires

@auth.requires_login()
def index():
    """
    Landing Page for the "View Single PO" page. Clicking "View" on any PO will land you here.

    :usage: https://yourURL/po/viewSinglePO/index?po_number=somepo_number

    :requires: user is logged in

    :param po_number: Can be any po number, valid or not. Method checks to ensure this PO is a legit PO

    :returns dict(po_number, print_po (optional), po_state, po_form, po_items, po_notes (optional)):
    If we receive a po_number that is not valid, we will return only the required items in the dictionary (IE, all optional fields are 
    left out). Additionally, an error will be flashed to the user informing them of their sins.

    If however the user submits a valid po_number, the optional items are returned as well.
    - po_number: the provided PO number
    - print_po: a boolean of whether to render a print screen of the PO or not
    - po_state: the state of the po (created, submitted, ordered, etc)
    - po_form: an HTML compatible string representation of our po information in the database
    - po_items: an HTML compatible string representation of the items attached to our po
    - po_notes: an HTML compatible string representation of the notes attached to our po
    - po_attachments: an HTML compatible string representation of the attachments on our po
    """
    poError = checkPoNumber()
    if poError:
        response.flash = poError
        return dict(po_number = "", po_state = "", po_form = "", po_items = "")

    poView,poState = createPOView(request.vars.po_number, request.vars.editable)
    poState = "View" if poState else "Edit"
    po_form = poView["po_form"]
    po_items = poView["po_items"]
    po_notes = poView['po_notes']
    po_attachments = poView['po_attachments']
    print_po = True if request.vars.print else False
    if print_po:
        return createPrintablePO(request.vars.po_number)
    return dict(po_number = request.vars.po_number, print_po=print_po, po_state = poState, po_form = po_form, po_items = po_items, po_notes = po_notes, po_attachments = po_attachments, add_notes = _can_add_note(request.vars.po_number))

# Put any validation of po numbers here.
# Currently we check to make sure we got a PO
# and to make sure that the PO exists
def checkPoNumber():
    """
    Helper method that ensures the provided po_number exists

    :usage checkPoNumber():

    :requires None:

    :param None: Technically the po_number needs to be a param, but to ensure it even exists, we do not require it be 
    passed to us. Instead, we check request.vars.po_number to see if it exists, before checking the validity of it

    :returns message/None: If there is an error with the provided po_number, an error message is returned. If all is well
    nothing is returned.
    """
    if not request.vars.po_number and not request.post_vars.po_number:
        return "No PO Number Provided"
    if db((db.po_form.id == request.vars.po_number) | (db.po_form.id == request.post_vars.po_number)).isempty():
        return f"Invalid PO #{request.vars.po_number}"

def createPOView(po_number, editPO=False):
    """
    Helper method that creates an HTML compatible version of our PO

    :usage createPOView(po_number, editPO):

    :requires None:

    :param po_number: po_number is expected to be a valid PO number. 
    :param editPO: editPO is a boolean informing us if we need to ensure the PO is editable or not

    :returns dict(po_form, po_items, po_notes):
    - po_form: an HTML compatible string representation of our po information in the database
    - po_items: an HTML compatible string representation of the items attached to our po
    - po_notes: an HTML compatible string representation of the notes attached to our po
    - po_attachments: an HTML compatible string representation of all attachments for our po
    """
    readonly = not isPoEditable(po_number, editPO)

    po = db(db.po_form.id == po_number).select().first()
    if po.submitter != auth.user.id:
        db.po_form.submitter.readable = True

    db.po_form.id.readable = False
    po_form = SQLFORM(db.po_form, record=po_number, readonly=readonly, submit_button="Save Changes To Form")
    db.po_item.po_form.readable = False
    db.po_item.id.readable = False
    po_items = []

    for row in db(db.po_item.po_form == po_number).select(db.po_item.id):
        po_items.append(SQLFORM(db.po_item, record=row.id, readonly=readonly, submit_button="Save PO Item Changes"))

    po_notes = [
        f'At {note.made_time}, {note.made_by.first_name} {note.made_by.last_name} {"approved the PO" if note.po_approval else f"added the note {note.comment}"}'
        for note in 
        db(db.po_note.po_form == po_number).select(orderby=~db.po_note.made_time)
    ]

    po_attachments = createPoAttachmentsView(po_number, editPO)

    return dict(po_form=po_form, po_items=po_items, po_notes=po_notes, po_attachments=po_attachments), readonly

def isPoEditable(po_number, editPO=None):
    """
    Helper method that verifies if a PO is editable or not based on the state of the PO. You can force a po to be editable
    by passing editPO=True

    :usage isPoEditable(po_number, editPO):

    :requires None:

    :param po_number: po_number needs to be attached to an existing PO.
    :param editPO (optional): You can override the results here by passing editPO=True (or literally anything except 
    None, "False", or False)

    :returns isEditable: Returns a boolean True/False informing you if the PO is indeed editable
    """
    po = db(
        (db.po_form.id == po_number) & 
        (db.po_status_reference.id == db.po_form.status)
    ).select(db.po_status_reference.status, db.po_form.submitter).first()
    status = po.po_status_reference.status
    submitter = po.po_form.submitter

    # TODO Clean this
    if editPO is not None and (editPO == "False" or not editPO):
        return False
    elif auth.has_permission('submitter', 'any', user_id=auth.user.id) and ((submitter == auth.user.id) and (status == 'Created' or status == 'Rejected')):
        return True
    else:
        return False

def createPoAttachmentsView(po_number, editable):
    if editable:
        po_attachments = get_editable_views(po_number, get_attachments(db, po_number, get_file))
    else:
        po_attachments = get_uneditable_views(po_number, get_attachments(db, po_number, get_file))

    container = DIV()
    for view in po_attachments.values():
        container.append(view)

    return container

@auth.requires_login()
def addNote():
    '''
    Controller exposed endpoint, allowing users to submit new notes to a PO

    :usage: https://yourURL/po/viewSinglePO/addNote

    :requires: Logged in user
    :requires: valid PO number
    :requires: User with edit permissions on po

    Note: Params should be provided via POST request
    :param po_number: PO Number must be a valid PO number
    :param note: String note to be saved

    :returns: 
    Successful note save
    {
        "status": 200,
        "message": ""
    }

    Unsuccessful note save (not all requirements met)
    {
        "status": 400,
        "message": "Invalid request, request requirements not met"
    }

    Unsuccessful note save (note entry fail)
    {
        "status": 500,
        "message": "An error occurred while saving the note. Please try again"
    }
    '''
    _response_200 = dict(
        status=200,
        message=""
    )
    _error_400 = dict(
        status=400,
        message="Invalid request, request requirements not met"
    )
    _error_500 = dict(
        status=500,
        message="An error occurred while saving the note. Please try again"
    )
    poError = checkPoNumber()
    if poError or not _can_add_note(request.post_vars.po_number) or request.post_vars.note is None:
        if request.post_vars.note is None:
            response.flash = T('Please provide a note')
            _error_400['message'] = 'Please provide a note'
        elif poError:
            response.flash = T('Invalid PO Number Selected')
        else:
            response.flash = T('Unable to add note to not-editable PO')
        return json.dumps(_error_400)

    try:
        note_id = db.po_note.insert(po_form=request.post_vars.po_number, made_by=auth.user_id, po_approval=False, comment=request.post_vars.note)
    except Exception:
        response.flash = T('An error occurred while saving the note. Please try again')
        return json.dumps(_error_500)

    note = db.po_note[note_id]

    _response_200['note'] = f'At {note.made_time}, {note.made_by.first_name} {note.made_by.last_name} {"approved the PO and " if note.po_approval else f"added the note {note.comment}"}'

    response.flash = T('Note Added')
    return json.dumps(_response_200)

def _can_add_note(po_number):
    closed_status = db(db.po_status_reference.status == 'Closed').select().first().id
    return not db((db.po_form.id == po_number) & (db.po_form.status == closed_status)).select().first()

def createPrintablePO(po_number):
    """
    Helper method that gathers all the information needed to render a printer friendly version of a PO.

    :usage createPrintablePO(po_number):

    :requires None:

    :param po_number: po_number needs to be attached to an existing PO.

    :returns dict(
        print_po, 
        vendor,
        confirmation,
        contact_number,
        contact_person,
        account,
        department,
        date,
        approved_vendor,
        sole_source,
        items,
        notes,
        attachments,
        po_number,
        tax,
        subtotal,
        freight,
        total,
        submitter,
        approvers
    ):
    - print_po: a boolean letting us know that the PO does indeed need to be printed
    - vendor: a string representation of the vendor name
    - confirmation: po confirmation number
    - contact_number: vendors contact number (including any extensions if provided)
    - contact_person: our contact with the vendor
    - account: our account number with the vendor
    - department: the department the PO is submitted for
    - date: the date the PO was submitted
    - approved_vendor: a boolean of whether the vendor is an approved vendor (speak with accounting/QA for more details on approved vendors)
    - sole_source: a boolean of whether the po is being purchased from the only vendor that provides the the product on the PO
    - items: an array of dictionary object representation of each individual item on the PO. Each dictionary contains the following keys.
        - catalog
        - description
        - quantity
        - price_per_unit
        - extended_price
    - notes: Currently not used
    - attachments: Currently not used
    - po_number: the po_number
    - tax: the amount (total) of tax on the PO. Already properly currency formatted
    - subtotal: the subtotal of the PO. Already properly currency formatted
    - freight: the freight (special handling cost) of the PO. Already properly currency formatted
    - total: the total of the PO. Already properly currency formatted
    - submitter: the string representation of the submitter
    - approvers: an array of string representations of the approvers on the PO
    """
    # TODO we need to handle NoneTypes on our returned info...
    po = db(db.po_form.id == po_number).select().first()
    vendor = po.vendor
    confirmation = po.order_confirmation_num
    contact_number = po.phone_num
    contact_person = po.contact_person
    account = po.account_num
    department = db(db.department.id == po.department).select(db.department.name).first().name
    date = db(
                (db.po_note.po_form == po_number) &
                (db.po_note.comment == "Submitted")
            ).select(db.po_note.made_time, orderby=~db.po_note.id, limitby=(0,1)).first()
    try:
        date = date.made_time.strftime("%Y-%m-%d")
    except AttributeError:
        date = ""
    approved_vendor = po.on_vendor_approval_list
    sole_source = po.sole_source_purchase
    items = [
        dict(catalog=item.catalog, description=item.description, quantity=item.quantity, price_per_unit='${:,.2f}'.format(item.price_per_unit), extended_price='${:,.2f}'.format(item.extended_price)) 
        for item in db(
            (db.po_item.po_form == po_number)).select(
                db.po_item.catalog, db.po_item.description, db.po_item.quantity, db.po_item.price_per_unit, db.po_item.extended_price)
            ]
    notes = "NA FOR NOW"
    attachments = "NA FOR NOW"
    subtotal = '${:,.2f}'.format(po.subtotal)
    tax = '${:,.2f}'.format(po.tax)
    freight = '${:,.2f}'.format(po.freight)
    total = '${:,.2f}'.format(po.total)
    submitter = db(db.auth_user.id == po.submitter).select(db.auth_user.first_name, db.auth_user.last_name).first()
    submitter = f'{submitter.first_name} {submitter.last_name}'
    approvers = [
            f'Approved By {approver.auth_user.first_name} {approver.auth_user.last_name} on {approver.po_note.made_time.strftime("%Y-%m-%d")}'
            for approver in db(
                (db.po_note.po_form == po_number) &
                (db.po_note.po_approval == True) &
                (db.auth_user.id == db.po_note.made_by) 
            ).select(db.auth_user.first_name, db.auth_user.last_name, db.po_note.made_time, orderby=~db.po_note.made_time)
        ]

    return dict(
        print_po=True, 
        vendor=vendor,
        confirmation=confirmation,
        contact_number=contact_number,
        contact_person=contact_person,
        account=account,
        department=department,
        date=date,
        approved_vendor=approved_vendor,
        sole_source=sole_source,
        items=items,
        notes=notes,
        attachments=attachments,
        po_number=po_number,
        tax=tax,
        subtotal=subtotal,
        freight=freight,
        total=total,
        submitter=submitter,
        approvers=approvers
    )