# -*- coding: utf-8 -*-

# TODO(Mike) FIX ME!
# TODO REFACTOR ME SO JS IS IN JS LAND AND WE ARE JUST BACKEND STUFF
@auth.requires_login()
@auth.requires_permission('supervisor', "any")
def index():
    """
    This is the landing page for the userAdmin page. Anyone in any form of management should have access to this page. It is used to administer 
    submitters/approvers in any department, based on the role of the viewing user.

    :usage: https://yourURL/po/userAdmin

    :requires: user is a supervisor in any department and user is logged in

    :param None:

    :returns dict(userAdminForm): The return is a dictionary containing a custom FORM object which when reduced to a string, is an html string
    """
    departments = get_department_permissions()
    if departments is None or len(departments) < 1:
        session.flash = f"{auth.user.username} Is Not Approved To Modify Other Users Permissions"
        redirect(URL(c="default", f="index", user_signature=True))

    output = FORM(
        DIV(
            SPAN(
                SELECT(
                    OPTION('Choose a department', _value=''),
                    *departments,
                    _name='department',
                    _onchange=("ajax('%s', ['department'], 'user_container',"
                            "{error:function(xhr,textStatus,errorThrown)"
                            "{if(xhr.status==401){window.location.reload(true)};}})" %
                            (URL(f='load_users'))
                    )
                ),
            ),
            SPAN(_id='user_container'),
            _class="departmentContainer"
        ),
        _id="userAdminForm")
    return dict(userAdminForm = output)

def get_department_permissions():
    """
    This method returns a list of all departments that the user has supervisor or higher permissions in. This is for 
    determining which departments the user has access to modifying users in

    :usage get_department_permissions():

    :requires None:

    :params None:

    :returns [department1, department2, ... departmentX]: Returns an array of department names (strings)
    """
    return [department for department in auth.departments if auth.has_permission('supervisor', department)]

@auth.requires(auth.has_permission('supervisor', request.vars.department) or not request.vars.department,
               requires_login=True)
def load_users():
    """
    This method is called when a department is selected. We use this function to get a list of all users (baring the auto complete, and viewer)

    :usage: https://yourURL/po/userAdmin/load_users or https://yourURL/po/userAdmin/load_users?department=some_department

    :requires: user must be a supervisor of the department provided (if a department is provided), or there must be no department provided. Additionally, the user must be logged in.

    :params department (optional): You can provide a department to filter users by.

    :returns users: Users are returned as an HTML string (IE, the string is html objects). If no department is specified, we return an empty string instead
    """
    if request.vars.department:
        userQuery = db((db.auth_user.id != 0) & (db.auth_user.id != auth.user_id)).select(db.auth_user.ALL, orderby=db.auth_user.first_name)
        users = SPAN(
                    SELECT(
                        OPTION('Choose a user', _value='', _selected=True),
                        *[OPTION(u.first_name, " ", u.last_name, _value=u.id) for u in userQuery],
                        _name="user",
                        _onchange=("ajax('%s',['department','user'],'permissions',"
                                "{error:function(xhr,textStatus,errorThrown)"
                                "{if(xhr.status==401){window.location.reload(true)};}})" %
                                (URL(f='permissions_form'), )),
                        _style="margin-left: 5px;"),
                    BR(),
                    BR(),
                    SPAN(_id='permissions'))
        return users
    return ''

@auth.requires(auth.has_permission('supervisor', request.vars.department) or not request.vars.department,
               requires_login=True)
def permissions_form():
    """
    Once a user and department are selected, we need to load the users permissions. This method will load the users relevant
    permissions, disabling permissions that need to be disabled as well.

    :usage: https://yourURL/po/userAdmin/permissions_form or https://yourURL/po/userAdmin/permissions_form?department=some_department&user=some_user

    :requires: user (viewer in this case) must be at minimum a supervisor for the department they are viewing, or there must not be a department provided. User (viewer in this case) must be logged in

    :params department (optional): if department is passed, a user needs to be passed as well. This will use the department to filter the users permissions down based both on the users role and the viewers role
    :params user (optional): if user is passed, a department needs to be passed as well. This will use the user to determine granular level permissions. Note, user is not viewer in this case.

    :returns permissions: Permissions is an HTML compatible string (IE, the permissions are wrapped in HTML text).
    """
    if request.vars.department and request.vars.user:
        department = request.vars.department
        user = request.vars.user

        viewer_query = (
            (db.auth_user.id == auth.user.id) &
            (db.auth_user.id == db.auth_membership.user_id) &
            (db.auth_permission.group_id == db.auth_membership.group_id) &
            ((db.department.name == db.auth_permission.table_name) & (db.department.name == department)) &
            (db.role_heirarchy.name == db.auth_permission.name) 
        )

        user_query = (
            (db.auth_user.id == user) &
            (db.auth_user.id == db.auth_membership.user_id) &
            (db.auth_permission.group_id == db.auth_membership.group_id) &
            ((db.department.name == db.auth_permission.table_name) & (db.department.name == department)) &
            (db.role_heirarchy.name == db.auth_permission.name) 
        )

        viewer_max = db(viewer_query).select(db.role_heirarchy.level, orderby=~db.role_heirarchy.level, limitby=(0,1)).first().level
        user_max = db(user_query).select(db.role_heirarchy.level, orderby=~db.role_heirarchy.level, limitby=(0,1)).first()

        # Making sure superuser has actual superuser permissions. IE, superuser should be able to set permissions for anyone in any department, including other superusers
        if auth.has_membership(auth.id_group('superuser'), user_id=auth.user_id):
            viewer_max = db(db.role_heirarchy.name == 'superuser').select(db.role_heirarchy.level).first().level + 1
        try:
            user_max = user_max.level
        except AttributeError:
            user_max = 0
        disabled = (user == str(auth.user.id))

        labels = []
        for row in db(db.role_heirarchy.id > 0).select(db.role_heirarchy.name, db.role_heirarchy.level, orderby=db.role_heirarchy.level):
            permission = row.name
            readablePermission = permission.replace("_", " ").title()
            level = row.level
            if permission in ['superuser', 'superapprover', 'accounting', 'packing_slip_uploader']:
                value = auth.has_membership(auth.id_group(permission), user_id=user)
            else:
                value = auth.has_membership(auth.id_group(department + " " + readablePermission), user_id=user)
            if level >= viewer_max:
                continue
            labels.append(
                SPAN(
                    LABEL(
                        readablePermission,
                        INPUT(
                            _name=permission,
                            _type="checkbox",
                            value=value,
                            _disabled=disabled or viewer_max <= user_max,
                            _style="margin-left: 5px;"
                        )
                    ),
                    BR()
                )
            )
        
        return SPAN(
                    *labels, 
                    BR(), 
                    BR(),
                    INPUT(
                        _value='Set Roles', 
                        _type='submit', 
                        _disabled=(viewer_max <= user_max)
                    ), 
                    _title="Cannot Change User's Permissions" if viewer_max <= user_max else ''
                )

    return ''

@auth.requires(auth.has_permission('supervisor', request.vars.department) or not request.vars.department,
               requires_login=True)
def change_permissions():
    """
    Viewer has selected the permission level for a user in a department and selected save. We need to save those changes to the database for that user

    :usage: https://yourURL/po/userAdmin/change_permissions or https://yourURL/po/userAdmin/change_permissions?department=some_department&user=some_user

    :requires: user (viewer in this case) must be at minimum a supervisor for the department they are viewing, or there must not be a department provided. User (viewer in this case) must be logged in

    :params department (optional): If department is passed, a user needs to be passed as well. This will use the department to filter the users permissions down based both on the users role and the viewers role
    :params user (optional): If user is passed, a department needs to be passed as well. This will use the user to determine granular level permissions. Note, user is not viewer in this case.
    :params role (optional): If a role is passed, a department and user are required. This applies the role to the user in the department provided.
    If a role is not passed, it is considered removed from the user. (Meaning if you dont pass the submitter role, 
    for example, the user is removed from the submitter role in this group).

    :returns None: Technically nothing is returned by this method. However we do put a notification in the browser informing the viewer of the success/failure to set the permissions for the selected user
    """
    now = request.now.strftime('%m-%d %H:%M:%S')
    flash_message = 'Failed to set permissions at %s' % (now, )
    user = db.auth_user(request.vars.user)

    if request.vars.department and user is not None:
        username = '%s %s' % (user.first_name, user.last_name)
        for row in db(db.role_heirarchy.name).select(db.role_heirarchy.name):
            role = f'{request.vars.department} {row.name.replace("_", " ").title()}'
            group = db(db.auth_group.role == role).select(db.auth_group.id).first()
            # There may be a chance that when we query the group based on the provided role, we get nothing back even though the role exists. This is likely to happen for
            # department_heads. To handle that, we do a secondary query, swapping out an underscore for a space. This corrects that issue in the database
            if group is None:
                role = row.name.replace("_", " ")
                group = db(db.auth_group.role == role).select(db.auth_group.id).first()
            if group is None:
                role = row.name
                group = db(db.auth_group.role == role).select(db.auth_group.id).first()

            group = group.id

            if request.vars[row.name]:
                auth.add_membership(group, user.id)
            else:
                auth.del_membership(group, user.id)
        flash_message = 'Successfully set permissions for %s at %s' % (username, now)
    response.headers['web2py-component-flash'] = flash_message
    return ''