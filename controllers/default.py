# -*- coding: utf-8 -*-
import datetime

### required - do no delete
def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires
@auth.requires_login()
def index():
    """
    TODO Documentation Maybe?
    """
    # TODO if we want a true homepage, this is where it will be
    if not auth.user:
        redirect(URL(c="default",f="user", args=["login"], user_signature=True))
    else:
    # TODO Its worth having a bit of logic here to see if the logged in user has any POs pending action.
    # If not, they should be directed to the new "Search PO" page?
        redirect(URL(c="viewPOForms", f="index", user_signature=True))
