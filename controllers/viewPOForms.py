# -*- coding: utf-8 -*-

from poFilter import UserLevelFilter, SupervisorLevelFilter, MaxLevelFilter, AccountingFilter, MaxLevelFilter_WithClosedPOs

VALID_FILTERS = ['user', 'supervisor', 'all', 'accounting']
### required - do no delete
def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires

@auth.requires_login()
def index():
    """
    This is the landing page for "View Your PO Forms", "View Actionable POs" and "View All POs" pages. 
    We handle all the role level filtering on this page

    :usage: https://yourURL/po/viewPOForms/index or https://yourURL/po/viewPOForms/index?filter=somefilter

    :requires: user must be logged in

    :params filter (optional): Filter can be provided as an optional param that tells the method what exactly you want to see on the page.
    If a filter is not provided, the system will attempt to "intellingently" guess what you want to see. Computer's are not smart. Tell it 
    what you want.

    :params view (optional): View can be provided to tell the method you want to view a particular PO. If view is passed, the user is redirected
    to "manageUserPO" with the po number attached

    :params edit (optional): Edit can be provided to tell the method you want to edit a particular PO. If edit is passed, the user is redirected
    to "editPO" with the po number attached

    :returns dict(poForm, header): returns a dictionary with the po information compressed into an SQLFORM, as the poForm, and a string header
    for the page, as the header
    """
    if request.args(0) in ('view',):
        redirect(URL(c="manageUserPO", f="index", vars=dict(po_number=request.args(-1))))

    if request.args(0) in ('edit',):
        redirect(URL(c="editPO", f="index", vars=dict(po_number=request.args(-1))))
    filter = request.vars.filter
    poFilter = None
    # If none is passed, we assume the request is for user level
    if filter is not None and filter.lower() not in VALID_FILTERS:
        raise Exception("Invalid PO Filter")

    if filter is None or filter.lower() == "user":
        filter = 'user'
        poFilter = getUserFilter()
    elif filter.lower() in ("supervisor"):
        poFilter = getApproverFilter()
    elif filter.lower() in ('accounting'):
        poFilter = getAccountingFilter()
    elif filter.lower() in ("all",):
        poFilter = getAllFilter()

    grid_vars = dict(
        query=poFilter.query,
        deletable=False,
        fields=poFilter.fields,
        create=False,
        editable=poFilter.editable,
        advanced_search=True,
        csv=None)
    if request.vars.pagination:
        grid_vars['paginate'] = int(request.vars.pagination)
        if grid_vars['paginate'] < 0:
            grid_vars['paginate'] = 20
        if grid_vars['paginate'] > 100:
            grid_vars['paginate'] = 100
    if auth.has_permission('accounting', 'any') and filter.lower() == 'accounting' :
        grid_vars['selectable'] = lambda ids: redirect(URL(f='generateReportCSV', vars=dict(ids=ids)))
        grid_vars['selectable_submit_button'] = 'Export Records To CSV'
    po = SQLFORM.grid(**grid_vars)

    return dict(poForm=po, header=poFilter.header, filter=filter.lower())

def getUserFilter():
    """
    This method will return a filter designed to only show the user their own submitted POs

    :usage getUserFilter():

    :requires None:

    :params None:

    :returns dict(query, fields, header, editable): 
    """
    query, fields = UserLevelFilter(db, auth.user.id)
    header = f"Pending POs For {auth.user.first_name} {auth.user.last_name}"
    editable = \
        lambda row: \
            db(db.po_form.id == row.id).select(db.po_form.submitter).first().submitter == auth.user.id and db(db.po_form.id == row.id).select(db.po_form.status).first().status <= db(db.po_status_reference.status == "Ordered").select(db.po_status_reference.id).first().id
    
    return Storage(dict(
        query=query,
        fields=fields,
        header=header,
        editable=editable
    ))

def getApproverFilter():
    query, fields = SupervisorLevelFilter(db, auth.user.id)
    header = f"Pending Actionable POs For {auth.user.first_name} {auth.user.last_name}"
    editable = None

    return Storage(dict(
        query=query,
        fields=fields,
        header=header,
        editable=editable
    ))

def getAllFilter():
    if auth.has_permission('accounting', 'any'):
        query, fields = MaxLevelFilter_WithClosedPOs(db, auth.user.id)
    else:
        query, fields = MaxLevelFilter(db, auth.user.id)

    header = "View All POs"
    editable = None

    return Storage(dict(
        query=query,
        fields=fields,
        header=header,
        editable=editable
    ))

def getAccountingFilter():
    query, fields = AccountingFilter(db, auth.user.id)
    header = "View Ordered POs"
    editable = None

    return Storage(dict(
        query=query,
        fields=fields,
        header=header,
        editable=editable
    ))

@auth.requires_login()
def generateReportCSV():
    response.headers['Content-Type'] = 'text/csv'

    ids = request.vars.ids
    if ids is None:
        return
    if isinstance(ids, str):
        ids = [ids]
    try:
        ids = [int(id) for id in ids]
    except Exception as exception:
        print(repr(exception))
        return

    import io
    class StringToByteIO(io.BytesIO):
        def write(self, s):
            super().write(s.encode('utf-8'))
    import csv
    f = StringToByteIO()
    writer = csv.writer(f, delimiter=',', quoting=csv.QUOTE_MINIMAL)
    headers = ['Entry Date', 'PO Number', 'Submitter', 'Approver', 'Department', 'Vendor', 'Total', 'Project', 'PO Item 1', 'PO Item 2', 'PO Item 3']
    writer.writerow(headers)
    for row in db((db.po_form.id.belongs(ids))).select():
        items = [row.description for row in db(db.po_item.po_form == row.id).select(limitby=(0, 3))]
        approver = db((db.po_note.po_form == row.id) & (db.po_note.po_approval == True)).select(orderby=db.po_note.made_time).first()
        approver = db.auth_user.format(approver.made_by) if approver is not None else ''
        submitter = f'{db.auth_user[row.submitter].first_name} {db.auth_user[row.submitter].last_name}'
        department = db.department[row.department]
        department = department.name if department is not None else ''
        total = round(row.total, 2)
        total = f'${total}' if total > 1 else f'$00.{total}'
        writer.writerow([
            row.po_entry_date,
            row.id, 
            submitter, 
            approver,
            department, 
            row.vendor, 
            total,
            row.project, 
            items[0] if len(items) >= 1 else '',
            items[1] if len(items) >= 2 else '',
            items[2] if len(items) >= 3 else '',
        ])
    f.seek(0)
    filename = 'Weekly Received Report.csv'
    return response.stream(f, request=request, attachment=True, filename=filename)
