#deprecated
from gluon import rewrite

# Should probably be able to load a department list instead of doing this. 
DEPARTMENTS = [
    "Accounting",
    "Account Services",
    "Construction",
    "Costco",
    "Engineering",
    "FCC",
    "Feeds/Pet Foods",
    "Feeds Receiving",
    "Foods",
    "Fuels",
    "General Lab",
    "HPLC",
    "HR",
    "IT",
    "LCMS",
    "Maintenance",
    "Metals",
    "Methods Development",
    "Microbiology",
    "Office",
    "Officers",
    "Organics",
    "QA",
    "QC",
    "Safety",
    "Sales Marketing",
    "Sampling",
    "Shipping",
    "Soils",
    "Soils Receiving",
    "Specials Receiving",
    "Technical",
    "Wet Chem Central",
    "Wet Chem North",
]

employees = ["Kluthe, AJ", "Musfeldt, Aaron", "Holliday, Aaron", "Mobry, Aaron", "Musfeldt, Aaron", "Robinson, Aaron", "Schumacher, Aaron", "Sears, Aaron", "Nutt, Abigail", "Christensen, Adam", "Felber, Adam", "Freeman, Adam", "Johnson, Adam", "Johnson, Adam", "OHara, Adam", "Matthews, Adam", "Melius, Adam", "OHara, Adam", "Hunter, Addie", "Hunter, Addie", "Joyce, Adrian", "Keegan, Adrian", "Keck, Adrienne", "Rockwell, Adrienne", "Beckwith, ALaina", "Beckwith, ALaina", "Timilsina, Alan", "Paul, Alanna", "Foster-Rettig, Aldwin", "Donovan, Alex", "Hook, Alex", "Johnson, Alex", "Kallenbach, Alex", "Lippold, Alex", "Praus, Alex", "Prideaux, Alex", "Tietz, Alex", "Kellett, Alexa", "Brown, Alexander", "DeLeon, Alexander", "Prado, Alfred", "Schiemann, Alfred", "Asanaenyi, Alice", "Clough, Alicia", "Smith, Alison", "Chavez, Allen", "Pike, Alli", "Fees, Allison", "Hermann, Allison", "Hansen, Alysha", "Szymczak, Alyson", "Elwood, Alyssa", "Hangman, Alyssa", "Mita, Alyssa", "Hill, Amanda", "Austin-Mafilika, Amanda", "Brostad, Amanda", "Jondle, Amanda", "Sanders, Amanda", "Thaler, Amanda", "Motley, Amanda", "Sitter, Amanda", "Reese, Amanda", "Sanders, Amanda", "Thaler, Amanda", "Brown, Amber", "Mausbach, Amber", "Monroe, Amber", "Mundt, Amber", "Quamme, Amber", "Loudon, AmberLea", "Alvarez, Amelia", "Speck, Amelia", "Mohr, Amelia", "Glock, Amy", "Bosak, Amy", "Braaten, Amy", "Buckley, Amy", "Gizinski, Amy", "Handbury, Amy", "Hemje, Amy", "Hester, Amy", "Joy, Amy", "Krasne, Amy", "Smouse, Amy", "Locher, Amy", "Meyers, Amy", "Militti, Amy", "Bosak, Amy", "Smouse, Amy", "Wells, Amy", "Ortiz-Morales, Ana", "Nelson, Ana", "Bendig, Andrea", "Brockman, Andrea", "Heilig, Andrea", "Johnson, Andrea", "Milne, Andrea", "Vlach, Andrea", "Albright, Andrew", "DeBoer, Andrew", "Bond, Andrew", "Linn, Andrew", "Gross, Andrew", "Jensen, Andrew", "Jipp, Andrew", "Lee, Andrew", "Linn, Andrew", "Rudersdorf, Andrew", "Sachs, Andrew", "Spencer, Andrew", "Jipp, Andrew", "Waltke, Andrew", "Booth, Andy", "Matz, Andy", "Rashid, Andy", "Alexander, Angela", "Brown, Angela", "Ervin, Angela", "Gerlach, Angela", "Miller, Angela", "Miller, Angela", "Moore, Angela", "Prescher, Angela", "Johnson, Angela", "Moore, Angela", "Vermeer, Angela", "Francis, Angelia", "Smith, Angelia", "Franklin, Anita", "Doerfel-Parker, Anke", "Kumm, Ann", "Wenzl, Ann", "Butera, Anna", "Rong, Anna", "Major, Annabel", "Hoins, Annette", "Nelsen, Annette", "Antenor, Anson", "Bieck, Anthony", "Dowers, Anthony", "Gennrich, Anthony", "Leapley, Anthony", "Marion, Anthony", "Leapley, Anthony", "Sumpter, Anthony", "Yates, Anthony", "Dall, Anton", "Mangaonkar, Anusuia", "Evans, April", "Halver, April", "Rakosky, Arden", "Feldmiller, Ari", "Miller, Ariana", "Peters, Ariele", "Herrera, Arixa", "Colorado, Armando", "Murshed, Ashiqur", "Gilliam, Ashlee", "Harvey, Ashleigh", "Arnold, Ashley", "Avalos, Ashley", "Bohac, Ashley", "Chatham, Ashley", "DiMauro, Ashley", "Jessick, Ashley", "Jones, Ashley", "Kappas, Ashley", "Avalos, Ashley", "Babl, Ashley", "Moore, Ashley", "Vrbka, Ashley", "Himan, Ashlyn", "Nanninga, Ashton", "Aasgaard, Audrey", "Sherrod, Audrey", "Blake, Auriel", "Tamegnon, Auriole", "Bailey, Austin", "Martin, Austin", "Nelson, Austin", "Sims, Austin", "Haq, Azeem", "Eloge, Barb", "Vavrick, Barb", "Frazier, Barbara", "McGrath, Barbara", "Blessing, Barry", "Webber, Barry", "Brandenburg, Bart", "Cox, Bart", "Andrle, Becky", "House, Becky", "Hrdy, Becky", "Kadel, Becky", "Andrle, Becky", "Stock, Becky", "Zimmerman, Becky", "Caldwell, Ben", "Johnson, Ben", "Landers, Ben", "Merritt, Ben", "Merritt, Ben", "Pedersen, Ben", "Piper, Ben", "Ertz, Benjamin", "Marcussen, Benjamin", "Vazquez, Benjamin", "Riggers, Bernice", "Kemp, Beth", "Pohlman, Beverley", "Aerts, Beverly", "Rael, Bianca", "Manning, Bill", "Steinbach, Bill", "Winchester, Billy", "Debor, Blaine", "Luebbert, Blaine", "Elder, Blake", "Philbrick, Blake", "Philbrick, Blake", "Baumgart, Bob", "Musil, Bobby", "Thomas, Bobby", "Ervin, Boyd", "Carlson, Brad", "Jauron, Brad", "Pruess, Brad", "Jauron, Brad", "Strandberg, Brad", "Stukenholtz, Brad", "Berg, Bradley", "Fickenscher, Brady", "Becvar, Brandi", "Barrett, Brandon", "Beethe, Brandon", "Brown, Brandon", "Griger, Brandon", "Hillman, Brandon", "Scholting, Brandon", "Kahnk, Brandon", "Newland, Brandon", "Scholting, Brandon", "Stennis, Brandon", "Bayles, Breanna", "Byrnes, Breanna", "Bayles, Breanna", "Bell, Brenda", "Coulter, Brendan", "Kelley, Brendan", "Aoki, Brendy", "Rudolph, Brenna", "Flemmer, Brent", "Pohlman, Brent", "Pentecost, Brent", "VanPatten, Bret", "Kelly, Brett", "Pietig, Brett", "Hodges, Brian", "Derby, Brian", "Drewel, Brian", "Hansen, Brian", "Hodges, Brian", "Jensen, Brian", "Kalasek, Brian", "Lawson, Brian", "McGloin, Brian", "Pawloski, Brian", "Pirnie, Brian", "Schulte, Brian", "Callahan, Brianna", "Ebel, Brianna", "Holmes, Brianna", "Johnson, Brianna", "Broyles, Briar", "Komasincki, Bridgette", "Schulz-Rhodes, Britt", "Rosendahl, Brittan", "Bedor, Brittany", "Cooper, Brittany", "Navrkal, Brittany", "Martinez, Brittany", "Navrkal, Brittany", "Cline, Brittny", "Livingston, Brooke", "Erhart, Brooks", "Manoukian, Bryan", "Heckman, Bryan", "Manoukian, Bryan", "Skillstad, Bryan", "McCullough, Bryce", "McFayden, Bryce", "Gillespie, Bud", "Reichert, Byron", "Vaughn, Byron", "Johnson, Caitlin", "Baber, Caleb", "Sterkel-Colombo, Calvin", "Lyncook, Calvin", "Sterkel-Colombo, Calvin", "Clemen, Cameron", "Moore, Cami", "Almaguer, Candace", "Mills, Candice", "Pond, Candice", "Stock, Candy", "Altizer, Capitola", "Franey, Cara", "Kucirek, Carey", "Nelson, Carl", "Bobier, Carla", "Goodrich, Carla", "Stephenson, Carla", "Thompson, Carlina", "Angulo, Carlos", "Trampe, Carly", "Hart, Carman", "Pellish, Carmen", "Petsch, Carmen", "Barnes, Carol", "Kamper, Carole", "Maher, Caroline", "Schwalbach, Carson", "Hall, Carter", "Carrell, Casey", "Schleicher, Casey", "Stevens, Casey", "Weber, Casey", "Wilson, Cassandra", "Steiner, Cassie", "Liebgold, Cathy", "Matuella, Cathy", "Watson, Cathy", "Caskey, Chad", "Hoins, Chad", "Juszyk, Chad", "Rengo, Chad", "Steele, Chad", "Keopanya, Chance", "Thompson, Channing", "Thompson, Channing", "Coultas, Chareise", "Totten, Charissa", "Getz, Charles", "Harper, Charles", "Wasserburger, Charles", "Miller, Charles", "Mountjoy, Charles", "Shavers, Charles", "Ware, Charles", "Dawson, Charlie", "Holub, Charlotte", "Kellar, Chauncey", "Tuimauga, Chavar", "Carlson, Chelsea", "Carlson, Chelsea", "Pontious, Chelsea", "Meyer, Chelsey", "Nath, Cherie", "Atkinson, Cheryl", "Parker, Cheyenne", "Hough, Chris", "Kramer, Chris", "Dougherty, Chris", "Frey, Chris", "Fullmer, Chris", "Hamling, Chris", "Jordan, Chris", "Leach, Chris", "LeFlore, Chris", "McDaniel, Chris", "Mieras, Chris", "Ouren, Chris", "Russell, Chris", "Schwartz, Chris", "Shellito, Chris", "Shellito, Chris", ", Chris", "Sinnott, Chrissy", "Donaldson, Christian", "Gunder, Christian", "Vogler, Christian", "Gilchrist, Christiann", "Imus, Christina", "Lee, Christina", "Kudym, Christina", "Wilson, Christine", "Calabro, Christopher", "Effken, Christopher", "Kellogg, Christopher", "McDaniel, Christopher", "Hamling, Christopher", "Schidler, Christopher", "Semler, Christopher", "McCoy, Cinda", "Rein, Cindy", "Brown, Clarence", "Smith, Clarence", "Wolf, Clay", "Jones, Clenita", "Gregory, Clint", "Haney, Cody", "Janssen, Cody", "Quine, Cody", "Parsons, Cole", "Parsons, Cole", "Watts, Cole", "Nahkunst, Colin", "Nahkunst, Colin", "Sand, Colin", "Hardick, Colleen", "ORouke, Colleen", "Rosso, Colleen", "Brennan, Collin", "Mann, Collin", "Garvin, Colton", "Nelsen, Colton", "Tracy, Colton", "Goduto, Connor", "Phipps, Connor", "Rush, Cora", "Gruber, Coral", "Gregory, Cordell", "Harold, Corey", "Gershon, Corinne", "Gershon, Corinne", "Valeika, Corry", "Amill, Cory", "Benes, Cory", "Livingston, Cory", "Runnells, Cory", "Brozek, Courtney", "Oltman, Courtney", "Dehm, Courtney", "Chambers, Craig", "Ebel, Craig", "Ebel, Craig", "Martin, Craig", "Hermann, Craig", "Jewell, Craig", "Wetterberg, Craig", "Turpin, Craig", "Wetterberg, Craig", "Katen, Curt", "Lemrick, Curt", "Lemrick, Curtis", "Huenefeld, Curtis", "Flatt, Cynthia", "Mitchell, Dale", "Sillik, Dale", "Youssef, Dalia", "Boltz, Dallas", "Bichlmeier, Dalton", "Rossbach, Dan", "Kottich, Dana", "Catlett, Dana", "Burkey, Dana", "Moyer, Dana", "Theisen, Dana", "Pedersen, Dane", "LeGros, Dani", "Baker, Daniel", "Baumann, Daniel", "Bopp, Daniel", "Brown, Daniel", "Buechter, Daniel", "Jett, Daniel", "Ossowski, Daniel", "Chaney, Daniel", "Davis, Daniel", "Deever, Daniel", "DeVetter, Daniel", "Jabs, Daniel", "Gonzalez, Daniel", "Harvey, Daniel", "Jabs, Daniel", "Jett, Daniel", "Johnson, Daniel", "Ossowski, Daniel", "Pelletier, Daniel", "Primi, Daniel", "Deever, Daniel", "Ahrens, Danielle", "Schaal, Danielle", "Schaal, Danielle", "Brown, Danna", "Duskin, Danny", "McMahon, Darbie", "Merrill, Darnell", "Armstrong, Darrick", "Drews, Darrin", "Drews, Darrin", "Reed, Dart", "Guthrie, Dave", "Huneke, Dave", "Schonlau, Dave", "Budka, David", "Burchell, David", "Coleman, David", "Coyle, David", "Creswell, David", "Dempsey, David", "Huneke, David", "Forsyth, David", "Keller, David", "King, David", "Loyall, David", "Greene, David", "Mosqueda, David", "Nauman, David", "Stephens, David", "Sykora, David", "VanKlaveren, David", "Warneke, David", "Tarbox, Dawn", "Tarbox, Dawn", "Brame, Dean", "Grilli, Deanna", "Cooper, Deb", "Kaluza, Debbie", "Willard, Debbie", "Willard, Debora", "Summerside, Deborah", "Harner, Debra", "Glover, Demera", "Glover, Demera", "Fetherkile, Derek", "Lemonds, Derek", "Mimms, Derek", "Morris, Derek", "Kendrick, Derrick", "Kendrick, Derrick", "Stover, Derry", "Prasanth, Devika", "Caballero, Devin", "Smith, Devin", "Anderson, Devon", "Anderson, Devon", "Delgado, Devon", "Dillon, Devon", "Jensen, Dex", "Lindloff, Diana", "Jurgens, Diane", "Northrup, Dillon", "Caniglia, Dominick", "Coash, Don", "Mummey, Don", "Gebers, Donald", "Steckelberg, Dou", "Dinning, Doug", "Gerdes, Doug", "Tosoni, Doug", "Starks, Douglesha", "Swenson, Drew", "Deibert, Duane", "Mayot, Duol", "Ball, Dustin", "Corman, Dustin", "Deibert, Dwayne", "McCombs, Dylan", "Richards, Dylan", "Workman, Dylan", "Murphy, E", "Maloney, Eamon", "Georgen, Ed", "Aguayo, Eddie", "Lopez, Edgar", "Richardson, Edward", "Abotsi, Efoui", "Guerrero, Efrain", "DeBoer, Elaine", "Boggs, Eli", "Caldwell, Elicia", "Hannon, Elijah", "Tracey, Elisabeth", "Ristow, Elisha", "Scheiber, Elisha", "Dussault, Elizabeth", "Jacobsen, Elizabeth", "Rhodes, Elizabeth", "Moore, Elizabeth", "Riesenberg, Elizabeth", "Worrall, Elizabeth", "Yapo, Ella", "Kusey, Ellen", "Eichner, Elliott", "Kanchewere, Elliott", "Black, Elston", "Buford, Emily", "Roll, Emily", "Burger, Emily", "Camacho, Emily", "Nessen, Emily", "Wagaman, Emily", "Wendover, Emily", "Williams, Emily", "Deats, Emma", "Mirch, Emma", "Mirch, Emma", "Spencer, Emma", "Stewart, Emma", "Adler, Eric", "Chickris, Eric", "Clausen, Eric", "Clausen, Eric", "Fjare, Eric", "Francoeur, Eric", "Hall, Eric", "Heuton, Eric", "Hubbert, Eric", "Klein, Eric", "Mitchell, Eric", "Peitz, Eric", "Hall, Eric", "Sobczak, Eric", "Steele, Eric", "Tyler, Eric", "Wetterberg, Eric", "Wolf, Eric", "Hall, Erica", "Pribil, Erica", "Ryan, Erica", "Ozuna, Erick", "Pina, Erick", "Grove, Erik", "Guetersloh, Erika", "Buri, Erin", "Cada, Erin", "Doherty, Erin", "Schnase, Erin", "Hubbard, Erin", "Kurtz, Erin", "Ramos, Erin", "Smith, Erin", "McIntyre, Erin", "Merrell, Erin", "Sorensen, Erin", "Sorenson, Erin", "Yelverton, Erin", "Mockelstrom, Ethan", "Stark, Ethan", "Lanning, Eugene", "Bredeweg, Evan", "Harpster, Evan", "Bredeweg, Evan", "Harpster, Evan", "Rajpar, Fatima", "Martinez, Florensio", "Noonan, Francis", "Demelle, Frank", "Little, Gabby", "Garaycochea, Gabriela", "Potter, Gabrielle", "Bunge, Gail", "Haug, Gail", "Haug, Gail", "Lawton, Gale", "Medlin, Garett", "Behrends, Garrett", "Hamby, Gary", "Shore, Gary", "White, Gary", "Montalvo, Gaven", "Wartick, Gavin", "Harris, Gemma", "Napon, Geoffroy", "Nelsen, George", "Rolfe, Gillian", "Howard, Gina", "Lee, Glen", "Alexander, Gracey", "Hebel, Grant", "Kavan, Grant", "Hebel, Grant", "Brezina, Greg", "Jarding, Greg", "Jarding, Greg", "Styduhar, Greg", "Willcockson, Greg", "Etter, Gregory", "Ervin, Gregory", "Brezina, Gregory", "Hudson, Griffin", "Ivener, Guadalupe", "Klug, Gwen", "Salsbury, Haleigh", "Nguyen, Hanna", "Nguyen, Hanna", "Smith, Hannah", "Smith, Hannah", "Sorensen, Hannah", "Andersen, Hans", "Little, Harvey", "Derksen, Heather", "Ramig, Heather", "McIntire, Heather", "Morehouse, Heather", "Pickrell, Heather", "Woodward, Heather", "Hulshizer, Heaven", "Hulshizer, Heaven", "Bruce, Heidi", "Horstman, Heidi", "Hottor, Henry", "McNeil, Henry", "Nelson, Henry", "Galatas, Hillary", "Hawkins, Hillary", "Galatas, Hillary", "Blum, Holli", "Mello, Hollie", "Mello, Hollie", "Garvin, Holly", "Schlickbernd, Holly", "Kloucek, Holly", "Schlickbernd, Holly", "Linder, Howard", "ODonnell, Hugh", "Resources, Human", "Bargar, Ian", "Braune, Irene", "Saner, Irene", "Agarkova, Irina", "Servellon, Iris", "Brown, Irvin", "Holm, Irving", "Salal, Ismael", "Kenter, Ivan", "Howell, Jace", "Dehner, Jack", "Little, Jack", "Love, Jack", "Dahl, Jacki", "Jordan, Jackson", "Gomez, Jaclyn", "Clancy, Jacob", "Grothe, Jacob", "Laack, Jacob", "Halverson, Jacob", "Nelsen, Jacob", "Pearson, Jacob", "White, Jacob", "Andresen, Jacqueline", "Cameron, Jade", "Hair, Jagger", "Pospisil, Jaimen", "Granger, Jaimie", "Gutierrez, Jaisa", "Arneson, Jake", "Carlson, Jake", "Johnson, Jake", "McGowan, Jake", "Stokes, Jake", "Wilson, Jake", "Blackburn, Jamar", "Fasching, James", "Grunkemeyer, James", "Beebe, James", "Cannon, James", "Crom, James", "Crye, James", "Grilliot, James", "Wurtz, James", "Davis, James", "Janzen, James", "Kopecky, James", "Needham, James", "Pedersen, James", "Porter, James", "Shearer, James", "McCall, James", "Shearer, James", "Shores, James", "Tardy, James", "Frieze, Jami", "Mitchell, Jami", "Wood, Jamie", "Bechtel, Jamie", "Hackenberg, Jamie", "Hamm, Jamie", "DeBoer, Jamie", "Hackenberg, Jamie", "McFarlin, Jamie", "Wood, Jamie", "White, Jamison", "Wolterman, Jan", "Davis, Janale", "Caldwell, Jane", "Anderson, Janelle", "Musil, Janette", "Rozmiarek, Janette", "Parker, Janice", "Pittman, Janice", "Lichtenberg, Janice", "Parker, Janice", "Jones, January", "Rutledge, Jared", "Goddard, Jared", "Graham, Jared", "Grauer, Jared", "Patton, Jared", "Rutledge, Jared", "Seipel, Jared", "Strate, Jared", "Wilken, Jarryd", "Williams, Jasmine", "Stark, Jason", "Bladt, Jason", "Crumrine, Jason", "Bladt, Jason", "Rothfuss, Jason", "Hawkins, Jason", "Knudton, Jason", "Salek, Jason", "McIntosh, Jason", "Merrill, Jason", "Metz, Jason", "Nickla, Jason", "Torpy, Jason", "Salek, Jason", "Schmidt, Jason", "Vanek, Jason", "Weigel, Jason", "White, Jason", "Williams, Jason", "Sanchez, Javier", "Bryant, Jay", "Bushon, Jay", "Bryant, Jay", "Harris, Jay", "Northrup, Jay", "Petersen, Jay", "Woods, Jay", "Johnson, Jean", "Abbott, Jeff", "Blomstedt, Jeff", "Bradley, Jeff", "Franco, Jeff", "Gonzales, Jeff", "Herr, Jeff", "Hulscher, Jeff", "Kalar, Jeff", "Machal, Jeff", "Pooley, Jeff", "Post, Jeff", "Salazar, Jeff", "Sunders, Jeff", "Blomstedt, Jeff", "Hacker, Jeffrey", "Waszgis, Jeffrey", "Scheid, Jen", "Kelley, Jenna", "Winnike, Jennifer", "Anit, Jennifer", "Price, Jennifer", "Geranis, Jennifer", "Reiter, Jennifer", "LaRue, Jennifer", "List, Jennifer", "Barry, Jennifer", "Reiter, Jennifer", "Olguin, Jennifer", "Pierce, Jennifer", "Ponec, Jennifer", "Price, Jennifer", "Anit, Jennifer", "Steele, Jennifer", "Struble, Jennifer", "VanEvera, Jennifer", "Waltke, Jennifer", "Deters, Jenny", "Petersen, Jenny", "Birdsall, Jeremiah", "Grell, Jeremiah", "Birdsall, Jeremiah", "Trevino, Jeremiah", "Gross, Jeremy", "Holmes, Jeremy", "Rector, Jeremy", "Rose, Jeremy", "Rutherford, Jeremy", "Campbell, Jeris", "Patzner, Jerod", "King, Jerome", "Wallace, Jerome", "Cribbs, Jerry", "Smith, Jerry", "Walker, Jess", "Carty, Jesse", "Krause, Jesse", "Murphy, Jesse", "Krause, Jesse", "Trombla, Jesse", "Tuttle, Jesse", "White, Jesse", "Douglas, Jessica", "Bell, Jessica", "Beyer, Jessica", "Bruce, Jessica", "Douglas, Jessica", "Foster, Jessica", "Hosler, Jessica", "Wurtz, Jessica", "Shortino, Jessica", "York, Jessie", "Leahy, Jessika", "Karl, Jessika", "Ganser, Jill", "Becker, Jim", "Dalen, Jim", "Fleissner, Jim", "Habegger, Jim", "Halbersleben, Jim", "Olson, Jim", "Waddell, Jim", "Roman, Jimmy", "Talaska, Jimmy", "Tharnish, Jo", "Mormann, Jocelyn", "Rock, Joe", "Bost, Joe", "Christensen, Joe", "Davis, Joe", "Eggers, Joe", "Hammeke, Joe", "Jones, Joe", "Sinnott, Joe", "Stathos, Joe", "Trader, Joe", "Depue, Joel", "Ficek, Joel", "Johnson, Joel", "Meng, Joel", "Oliver, Joel", "Stradinger, Joel", "Ficek, Joel", "Lewandowski, Joey", "Anderson, Johanna", "Kelley, John", "Bazis, John", "Christenson, John", "Coonfare, John", "Danforth, John", "Elsaesser, John", "Ernst, John", "Fascianella, John", "Guthmiller, John", "Hanson, John", "Henthorn, John", "Keith, John", "Kilday, John", "Coonfare, John", "Tiedemann, John", "Miller, John", "Olley, John", "DeBoer, John", "Menghini, John", "Torpy, John", "Patrick, John", "McManis, John", "Sershen, John", "Shannon, John", "DeBoer, John", "Vaughan, John", "Welch, John", "Scissom, Johnathan", "Haynes, Johnnie", "Morelock, Jolin", "Demuth, Jon", "Hoagboon, Jon", "Rabe, Jon", "Shonebarger, Jon", "Hammond, Jonathan", "Kazhila, Jonathan", "Meyer, Jonathan", "Renstrom, Jonathan", "Zuniga, Jonathan", "Kirwan, Joni", "Boerma, Jordan", "Davis, Jordan", "Gunn, Jordan", "Willers, Jordan", "Sabez, Jordan", "Reznikov, Jordan", "Sedlacek, Jordan", "Uttecht, Jordan", "Willers, Jordan", "Klein, Jordyn", "Reyes, Jose", "Gaube, Joseph", "Sisco, Joseph", "Black, Joseph", "Davis, Joseph", "Doyen, Joseph", "Duffer, Joseph", "Thelen, Joseph", "Kirui, Joseph", "Walker, Joseph", "McGinnis, Joseph", "Paletta, Joseph", "Ulphani, Joseph", "Eggers, Joseph", "Walker, Joseph", "Nicholson, Josh", "Coxbill, Josh", "Krejci, Josh", "Geist, Josh", "Krajewski, Josh", "Krejci, Josh", "Leach, Josh", "Murphy, Josh", "Rodenburg, Josh", "Shirley, Josh", "Avram, Joshua", "Battazzi, Joshua", "Brown, Joshua", "Idachaba, Joshua", "Pospisil, Joshua", "Pospisil, Joshua", "Thomas, Joshua", "Valenti, Joshua", "VanDerslice, Joslynn", "Murry, Jovan", "Ortiz, Juan", "Bouma, Judith", "Juhl, Judy", "Caldwell, Julian", "Menghini, Julie", "Owen, Julie", "Kowal, Julie", "Krupicka, Julie", "Price, Julie", "Nelson, Justin", "Boerma, Justin", "Ching, Justin", "Holmes, Justin", "Kearns, Justin", "Boerma, Justin", "Mueller, Justin", "Nelson, Justin", "Palmer, Justin", "Pollock, Justin", "Sears, Justin", "Ussery, Justin", "Gregory, Justina", "Pogge, Justine", "Pogge, Justine", "Hughes, Justus", "Hughes, Justus", "Scott, Kahlea", "Parr, Kailey", "Johnson, Kaitlin", "Ashley, Kaitlyn", "Eastman, Kaitlyn", "Eastman, Kaitlyn", "Abioye, Kamar", "Brown, Kamau", "Pope, Kambi", "Altman, Kara", "Rivera, Kara", "Andresen, Kari", "Sjostrom, Karissa", "Ostrand, Karl", "Eby, Karla", "Qualheim, Karlee", "Goforth, Kassandra", "Houdek, Kassandra", "Wenstrand, Kassandra", "Klumper, Katarina", "Clark, Kate", "Womack, Kate", "Behounek, Katelyn", "Prager, Katelyn", "Bartusiak, Kathleen", "Boylan, Kathleen", "Madsen, Kathleen", "Smith, Kathleen", "Johnson, Kathryn", "Kovac, Kathryn", "Cornwell, Kathryn", "Forke, Kathryn", "Campbell, Kathy", "Kruzan, Kathy", "Griess, Katie", "Herringa, Katie", "Griess, Katie", "Larson, Katie", "Larson, Katie", "Lincoln, Katie", "Reeve, Katie", "Reeve, Katie", "Spanger, Katie", "Spencer, Katie", "Svoboda, Katricia", "Burchett, Katrina", "Wilcox, Katrina", "Booth-Malnack, Katryna", "Cornwell, Katy", "Kovac, Katy", "Krause, Katy", "Bolte, Kay", "Simms, Kayla", "Parr, Keegan", "Copeland, Kegan", "Mitchell, Keish", "Firman, Keith", "Herrington, Keith", "Tooley, Keith", "Arnett, Kelli", "Kincaid, Kellie", "Daberkow, Kelly", "Berney, Kelly", "Cunningham, Kelly", "Duckert, Kelly", "Hallagan, Kelly", "Hallagan, Kelly", "Langenegger, Kelly", "Daberkow, Kelly", "Kruse, Kelsey", "Abbott, Kelty", "Kenkel, Kelty", "Emesih, Kemy", "Smith, Kendall", "Lymon, Kendra", "Hale, Kendra", "Pohlman, Kennard", "Johnson, Kenneth", "Garrison, Kenneth", "Mann, Kenneth", "White, Kenneth", "Sunderman, Kent", "Carleton, Kerri", "Aldrich, Kevin", "Byers, Kevin", "Chytil, Kevin", "Denny, Kevin", "Heck, Kevin", "Flores, Kevin", "Free, Kevin", "Joerger, Kevin", "Kroeger, Kevin", "McCann, Kevin", "Meyer, Kevin", "Morgan, Kevin", "OFlynn, Kevin", "Quinn, Kevin", "Quinn, Kevin", "Voyles, Kevin", "Wilson, Kevin", "Algya, Kiley", "Algya, Kiley", "Vandenburg, Kiley", "Westergaard, Kiley", "Blizzard, Kim", "Phan, Kim", "Porter, Kim", "Blizzard, Kimberly", "Butts, Kimberly", "Bynote, Kimberly", "Edwards, Kimberly", "Meints, Kimberly", "McKee, Kimberly", "Meints, Kimberly", "Zehnder, Kirill", "Didamo, Kirsten", "Didamo, Kirsten", "Irwin, Kody", "Womas, Koko", "Vang, Kongmeng", "Ritter-Butz, Korbin", "Haugen, Koren", "Shea, Kori", "Rokahr, Kori", "Rokahr, Kori", "Kica, Kortnee", "Patera, Kory", "Patera, Kory", "Seipel, Kris", "Bowlin, Krissi", "Anders, Krista", "Niezwaag, Krista", "Sacry, Krista", "Aylward, Kristen", "Emanuel, Kristin", "Caron, Kristina", "Slater, Kristina", "Springmeyer, Kristine", "Moulton, Kristopher", "Crumm, Kristy", "Schaaf, Kristy", "Schaaf, Kristy", "Ward, Krys", "Hamik, Krystal", "Hamik, Krystal", "Edwards, Kurt", "Olmer, Kurt", "Olmer, Kurt", "Lammers, Kurtis", "Hinkel, Kurtis", "Lammers, Kurtis", "Kern, Kyla", "Aldana, Kyle", "Copple, Kyle", "Bernstrauch, Kyle", "Estwick, Kyle", "Fahey, Kyle", "Kloewer, Kyle", "Lowery, Kyle", "McAndrews, Kyle", "Morton, Kyle", "Rupp, Kyle", "French, Kylee", "Kemp, Kylee", "Tiller, Lamayzio", "Peterson, Landon", "Peterson, Landon", "Mikels, Lara", "Mikels, Lara", "Ross, LaRell", "Adams, Larry", "Alexander, Larry", "Altadonna, Larry", "Hinrichs, Larry", "Jones, Lateesha", "Stevens, Latricia", "Adler, Laura", "Conant, Laura", "Deck, Laura", "Fox, Laura", "Fuhrman, Laura", "Conant, Laura", "Moore, Laura", "Zadow, Laura", "Honeycutt, Laura", "Moore, Laura", "Schmitt, Laura", "Skold, Laura", "Tierney, Laura", "Tracey, Laura", "Sellers, Laurel", "Suchan, Lauren", "Albert, Lauren", "Rutledge, Lauren", "Suchan, Lauren", "Uhlig, Lauren", "Mlejnek, Lauri", "Wilson, LaVon", "Mueggenberg, Leah", "Jaixen, Leanna", "Kalvelage, Leanna", "Wigington, LeeAnn", "Acosta, Leo", "Marks, Leo", "Ogomo, Leonard", "Stohlmann, Leonard", "Falter, Lesley", "Martinson, Leslie", "Ring, Levi", "Mills, Libby", "Arndt, Linda", "Wendel, Linda", "Santos, Linda", "McDaniel, Lindsay", "ONeill, Lindsay", "McCormick, Lindsay", "Schnackenberg, Lindsay", "Thomsen, Lindsay", "Frye, Lindsey", "Kawahakui, Lindsey", "Sanchez, Lindzey", "Pitz, Lisa", "Avila, Lisa", "Cornelius, Lisa", "Gerjevic, Lisa", "Grantham, Lisa", "Avila, Lisa", "Offutt, Lisa", "Pitz, Lisa", "Rasmussen, Lisa", "Riesberg, Lisa", "Sempek, Lisa", "Weissenburger-Moser, Lisa", "Becker, Liz", "Holmgren, Liz", "Larson, Lizabeth", "Rhodes, Lizzie", "Falt, Logan", "Wright, Logan", "Wright, Logan", "Haupt, Lois", "Trinh, Long", "Tharnish, Loretta", "Thompson, Lori", "Gelder, Lorri", "Kozisek, Lucas", "Poete, Ludovic", "Vazquez, Luis", "Aeilts, Luke", "Westphal, Luke", "Ivener, Lupita", "Madison, Lynde", "Stednitz, Lyndsay", "Hinman, Lyndsay", "Kriegler, Lynette", "Mullen, Lynette", "Jenkins, Lynn", "Carlson, Lynsey", "Potts, Macy", "Warren, Madelyn", "Lange, Madison", "Day, Maggie", "Sontag, Magi", "Urbanovsky, Makaela", "Willett, Malcolm", "Willett, Malcolm", "Huggins, Mallory", "Frans, Mandy", "Michaud, Mandy", "Villaverde, Marc", "Jacobsen, Marc", "Villaverde, Marc", "Barnes, Marcus", "Rowe, Marcy", "Forget, Margaret", "Pearson, Margaret", "Pena, Margarito", "Bertelsen, Margie", "Ortiz, Maria", "Reynolds, Maria", "Cornish, Marissa", "Warner, Mark", "Berlett, Mark", "Doyle, Mark", "Pankers, Mark", "Wiedel, Mark", "Radmacher, Mark", "Seman, Mark", "Pankers, Mark", "Warner, Mark", "Wiedel, Mark", "Wulff, Mark", "Cunningham, Marlon", "Ortega, Marlon", "McGill, Marshall", "Galvin, Martha", "Evans, Martina", "Evans, Martina", "Gizinski, Marty", "Timmerwilke, Marty", "Barone, Mary", "Bartek, Mary", "Kraft, Mary", "Piper, Mary", "Piper, Mary", "Sothan, Mary", "Stumbaugh, Mary", "Tadros, Mary", "Zeigler, Mary", "Escalante, Maryellen", "Menke-Wright, MaryJo", "Barton, Matt", "Benson, Matt", "Blair, Matt", "Borman, Matt", "Martin, Matt", "Diem, Matt", "Hetzler, Matt", "Goodlett, Matt", "Hennen, Matt", "Hetzler, Matt", "Stukenholtz, Matt", "Jones, Matt", "Kueker, Matt", "Martin, Matt", "Nissen, Matt", "Peterson, Matt", "Schnase, Matt", "Schwabe, Matt", "Snowdon, Matt", "Starr, Matt", "Stolzenburg, Matt", "Stukenholtz, Matt", "Vanicek, Matt", "Wetjen, Matt", "Will, Matt", "Williams, Matt", "Arnett, Matthew", "Brandes, Matthew", "Schnase, Matthew", "DeBoer, Matthew", "Hennen, Matthew", "Latner, Matthew", "Schultz, Matthew", "DeBoer, Matthew", "McDermott, Matthew", "Schmidt, Matthew", "Shellhase, Matthew", "Stastny, Matthew", "Tierney, Maureen", "OConnor, Maureen", "Tierney, Maureen", "Mansaray, Maurisa", "Trejo-Hernandez, Maximilliano", "Coffee, Maxine", "Young, Meagan", "Bruce, Megan", "Chelle, Megan", "Coleman, Megan", "Fonfara, Megan", "Kuehl, Megan", "Okeson, Megan", "Sheppard, Megan", "Skold, Megan", "Wilson, Megan", "Prosser, Meghan", "Prosser, Meghan", "Aust, Melanie", "Lang, Melanie", "Lang, Melanie", "Wojtkiewicz, Melinda", "Armistead, Melissa", "Arnold, Melissa", "Bourgeois, Melissa", "Chadwell, Melissa", "Howe, Melissa", "Bourgeois, Melissa", "Milner, Melissa", "Moore, Melissa", "Parpart, Melissa", "Salerno, Melissa", "Holliday, Mell", "Baxter, Melvin", "Chambers, Meredith", "Shellito, Meredith", "VanPatten, Merrill", "Logan, Mia", "Molinari, Mianna", "Arwood, Michael", "Belling, Michael", "Bloom, Michael", "Bohnhoff, Michael", "Boyle, Michael", "Brodie, Michael", "Crum, Michael", "Cruse, Michael", "Cuevas, Michael", "Bloom, Michael", "Feulner, Michael", "Fischer, Michael", "Neman, Michael", "Gillilan, Michael", "Hartnett, Michael", "Hensley, Michael", "Iversen, Michael", "Belling, Michael", "Henrichs, Michael", "Johnson, Michael", "Bohnhoff, Michael", "Labs, Michael", "LaRandeau, Michael", "Madsen, Michael", "Menconi, Michael", "Neman, Michael", "Neubauer, Michael", "Paz, Michael", "LaRandeau, Michael", "Williams, Michael", "Torres, Michael", "Richardson, Michael", "Wheeldon, Michael", "Wolf, Michael", "Woods, Michael", "Hume, Michaela", "Stricklin, Michala", "Julian, Michele", "Blecha, Michelle", "Cantu, Michelle", "Schuster, Michelle", "Maher, Michelle", "Pereira, Michelle", "Roeder, Michelle", "Schuster, Michelle", "Denaeyer, Mike", "Gnarra, Mike", "Greene, Mike", "Henrichs, Mike", "Houston, Mike", "Menghini, Mike", "Lewis, Mike", "Lotspeich, Mike", "McGinty, Mike", "Tiedemann, Mike", "Wilson, Mike", "Volz, Mikel", "Fok, Ming", "Wang, Ming", "Wang, Ming", "Bierbower, Mitch", "Thiel, Mitch", "Charron, Mitchell", "Abudamir, Mohamed", "Halhouli, Mohammed", "Halhouli, Mohammed", "Khanal, Mohit", "Hadan, Molly", "Gaston, Molly", "McGinn, Molly", "McMilian, Molly", "Al-Mugotir, Mona", "Marasco, Monica", "Michalik, Monnie", "Balabanoff, Morgan", "Gleason, Morgan", "Kaiser, Morgan", "Shaffer, Morgan", "Julian, Nancy", "Tan, Nancy", "Brummel, Nancy", "Brummel, Nancy", "Prout, Nancy", "Matz, Natalie", "Holtz, Natalie", "Junge, Natalie", "Matz, Natalie", "Fittje, Nate", "Kounkel, Nate", "Larson, Nate", "Slachetka, Nate", "Svoboda, Nate", "Halzen, Nathan", "Hannemann, Nathan", "Heaton, Nathan", "Hoffman, Nathan", "Johnson, Nathan", "Lyman, Nathan", "Mongan, Nathan", "Oerman, Nathan", "Smith, Nathan", "Oerman, Nathan", "Thomas, Nathan", "Wecker, Nathan", "Lombardo, Nathaniel", "Nooruddin, Naveed", "Sabata, Neil", "Jones, Neil", "Sabata, Neil", "Baker, Nicholas", "Giacoppo, Nicholas", "Otten, Nicholas", "Freer, Nicholas", "Hanson, Nicholas", "Hopkins, Nicholas", "Seminara, Nicholas", "Wiehl, Nicholas", "Easton, Nichole", "Kern, Nichole", "Goffney, Nichole", "Kern, Nichole", "Barrientos, Nick", "Clark, Nick", "Floreani, Nick", "Giacoppo, Nick", "Girard, Nick", "Ochako, Nick", "Seminara, Nick", "Wages, Nick", "Wiehl, Nick", "Wilde, Nick", "Hogan, Nicki", "Kern, Nickolus", "Torres, Nicolas", "Pfeffer, Nicole", "Brown, Nicole", "Horn, Nicole", "Jaspersen, Nicole", "Wallace, Nicole", "Andersen, Nicole", "Apfelbeck, Nicole", "Nieves, Nicole", "Rother, Nicole", "Starns, Nicole", "Wallace, Nicole", "Krejci, Nicolette", "Tyack, Nik", "Neagu, Nikki", "Prasanth, Nita", "Peters, Noah", "Hytrek, Nolan", "Olsen, Ole", "Olsen, Ole", "Bekale, Olivia", "Robertson, Oreta", "Barrera, Oscar", ", Outsource", "Derr, Paden", "VanCleve, Paige", "Ameta, Pallavi", "Bohac, Pam", "Hiemer, Pam", "Hiemer, Pam", "Kelly, Parker", "Pombrio, Parker", "Scott, Parker", "Jones, Parris", "Delaware, Patricia", "Adamski, Patrick", "Anderl, Patrick", "Weller, Patrick", "Bresnahan, Patrick", "Castle, Patrick", "Luger, Patrick", "Nitchals, Patrick", "Weller, Patrick", "Wewel, Patrick", "Delaware, Patty", "Gough, Patty", "Kimberling, Patty", "Brown, Paul", "Signore, Paul", "Westenburg, Paul", "Holm, Paul", "Johnson, Paul", "Kay, Paul", "Duran, Paul", "Lonn, Paul", "Prososki, Paul", "Staab, Paul", "Guastello, Paula", "Doeschot, Paula", "Vore, Paula", "Morris, Philip", "Lind, Philip", "Morris, Philip", "Rarig, Phillip", "Halsted, Phillip", "Rarig, Phillip", "Hoffman, Piper", "Arora, Prem", "Khan, Qadir", "Marion, Quantaus", "Damgaard, Quinn", "Pracheil, Quinn", "McAdams, Quintin", "Green, Rachael", "Steier, Rachael", "Benzoni, Rachel", "Drapela, Rachel", "Durham, Rachel", "Corell, Rachel", "Smith, Rachel", "Gering, Raime", "Elia, Raina", "Viola, Rainer", "Forke, Ralph", "Pilcher, Ranae", "Rogers, Randall", "Lambert, Randy", "Moncrief, Randy", "Telfer, Raquel", "Castillo, Raul", "Rael, Ray", "Urwin, Ray", "Morten, RB", "Tafoya, Reagan", "Zimmerman, Rebecca", "Perry, Rebecca", "Davis, Reid", "Meier, Rene", "Corley, Rene", "Ferris, Renee", "Lohaus, Renee", "Pitt, Rhonda", "Pankowski, Richard", "Badger, Richard", "Goff, Richard", "Foster, Richard", "Johnson, Richard", "Morhardt, Richard", "Siemer, Richard", "Leahy, Rick", "Jarzynka, Riley", "Shenk, Riley", "Petersen, Rita", "Ferris, Rob", "Burks, Robert", "Ferris, Robert", "Orsi, Robert", "Burks, Robert", "Hecht, Robert", "Distefano, Robert", "Baumgart, Robert", "Heineman, Robert", "Speck, Robert", "Madison, Robert", "Mangiameli, Robert", "Murphy, Robert", "Musil, Robert", "Distefano, Robert", "Walters, Robert", "Weber, Robert", "Gonzalez, Roberto", "Lopez, Roberto", "Colter, Robin", "Sheldrake, Robin", "McGrath, Robyn", "Thomsen, Robyn", "Stout, Rodney", "Barratt, Roger", "Jerrick, Roger", "Nance, Roland", "Zielinski, Roman", "Bruening, Ron", "Spier, Ron", "Rodgers, Rose", "Quinones, Rosely", "Barragan, Roxie", "Paden, Roy", "Hadan, Russell", "Ruffcorn, Russell", "Waterfield, Ruth", "Tinant, Ruthie", "Staub, Ryan", "Bawek, Ryan", "Brown, Ryan", "Bruce, Ryan", "Cannon, Ryan", "Cook, Ryan", "Evers, Ryan", "Maitland, Ryan", "Muldoon, Ryan", "Norman, Ryan", "Ogata, Ryan", "Anderson, Ryan", "Staub, Ryan", "Tunnell, Ryan", "Pickerill, Ryan", "Weber, Ryan", "Blessing, Sam", "Ford, Sam", "Morris, Sam", "Muiu, Sam", "Swartz, Sam", "Strickland, Samantha", "Uttecht, Samantha", "Becker, Sandy", "Becker, Sandy", "Rome, Sandy", "Watkins, Sanford", "Marrufo, Santa", "Iamelli, Sara", "Campbell, Sara", "Lange, Sara", "Moore, Sara", "Sterkel-Colombo, Sara", "Rupp-Moody, Sara", "Ryan, Sara", "Stepan, Sara", "Veloso, Sara", "Purcell, Sarah", "Bjorkman, Sarah", "Campbell, Sarah", "Cornell, Sarah", "Currie, Sarah", "Currie, Sarah", "Campbell, Sarah", "Medeck, Sarah", "Welch, Sarah", "Lockhorn, Scott", "Anderson, Scott", "Bailey, Scott", "Bruschwein, Scott", "Ewer, Scott", "Holtslander, Scott", "Evans, Scott", "Foresman, Scott", "Harris, Scott", "Henderson, Scott", "Holtslander, Scott", "King, Scott", "McGrath, Scott", "Peterson, Scott", "Ronshaugen, Scott", "Rucker, Scott", "Smith, Scott", "Smith, Scott", "Groff, Scottie", "Awakuni, Sean", "King, Sean", "Taylor, Sean", "Thiemann, Sean", "Royer, Sean", "Souders, Sean", "Taylor, Sean", "Edwards, Sebastian", "Haroon, Sehr", "Alberts, Seth", "Frishman, Seth", "Grigsby, Shakeitha", "Hertz, Shandi", "Hertz, Shandi", "Gundersen, Shane", "Harland, Shane", "Havens, Shane", "Kinnison, Shane", "Sisel, Shane", "Sisel, Shane", "Hotchkiss, Shannon", "Hunter, Shannon", "Ramos, Shannon", "Bloom, Sharon", "Pflanz, Sharon", "Erickson, Shawn", "Halverson, Shawn", "Phifer, Shawn", "Rahaman, Shawn", "Mai, Shawnette", "Slater, Shea", "Slater, Shea", "Morales, Sheila", "Bloomquist, Shelby", "Davis, Shellayne", "Morisetti, Shilpa", "Poitier, Sidney", "Keiser, Sierra", "Hernandez, Silvestre", "Young, Sineah", "Sanders, Skyler", "Brown, Spencer", "Sharp, Spencer", "Tibbetts, Spencer", "Boehm, Stacey", "Godek, Stacey", "Gutchewsky, Stacey", "Sigmon, Stacey", "Nelson, Stacie", "Nelson, Stacie", "Coleman, Stacy", "Gillman, Stacy", "Crockett, Stefanie", "Jones, Stefanie", "Jones, Stefanie", "Rath, Stefanie", "Eulie, Stephanie", "Hoffman, Stephanie", "Kassera, Stephanie", "Adams, Stephanie", "Peterson, Stephanie", "Westergren, Stephanie", "Benish, Stephen", "Dak, Stephen", "Hampton, Stephen", "Vlock, Stephen", "Leeds, Stephen", "Sueda, Stephen", "Whiteman, Stephen", "Gleason, Steve", "Vlock, Steve", "Wieczorek, Steve", "Eckert, Steven", "Jr, Steven", "Fox, Steven", "Hansen, Steven", "Hawbaker, Steven", "Lancaster, Steven", "Mullen, Steven", "Samson, Steven", "Schumacher, Steven", "Schwanke, Sue", "Seitz, Sue", "Anantapalli, Suneeta", "Sharma, Sunil", "Smith, Susan", "Kielion, Susan", "Massih, Susan", "Wertheim, Susan", "Willis, Susan", "DeCoux, Suzanna", "Spiehs, Suzanne", "Eggert, Tamara", "Grage, Tanner", "York, Tanner", "Humphrey, Tanya", "Humphrey, Tanya", "Wright, Tara", "Wulfekoetter, Tara", "Wright, Tara", "Wulfekoetter, Tara", "Dorn, Taryn", "Reynolds, Taulima", "VanOtterloo, Taylor", "Washburn, Taylor", "Grote, Taylor", "Kavan, Taylor", "Mosley, Taylor", "Raymond, Taylor", "Baker, Ted", "Cemper, Ted", "Stocking, Ted", "Goodwin, Teia", "Bottcher, Terra", "Bottcher, Terra", "Luckett, Terrance", "Tonsil, Terrence", "Henriksen, Terri", "Henriksen, Terri", "Thompson, Terry", "Langston, Thea", "Honeycutt, Theresa", "Noah, Theresa", "Parys, Therese", "Flanigan, Thomas", "Jr, Thomas", "Paper, Thomas", "Harris, Thomas", "Janing, Thomas", "Korte, Thomas", "Markeson, Thomas", "Schultz, Thomas", "Riffle, Tiffany", "Rubush, Tiffany", "Baruth, Tim", "McIntyre, Tim", "Radmacher, Tim", "Spencer, Tim", "Steil, Tim", "Sullivan, Tim", "Sveeggen, Tim", "Szalewski, Tim", "White, Tim", "Alexander, Timothy", "Sullivan, Timothy", "Penne, Timothy", "Radmacher, Timothy", "Mowry, Timothy", "Thompson, Timothy", "Mundorf, Timothy", "Woldt, Timothy", "Larson, Tina", "Pridell, TJ", "Mcclinton-Wilson, Tobias", "Anderson, Todd", "Belgum, Todd", "Christensen, Todd", "Lundgren, Todd", "Anderson, Todd", "Rieper, Todd", "Armknecht, Tom", "Cooper, Tom", "Dao, Tom", "Hyde, Tom", "Miller, Tom", "Nguyen, Tom", "Barnes, Tony", "Hubbard, Tony", "Koch, Tony", "Lewis, Tony", "Sierra, Tony", "Baxley, Tonya", "Moore, Tonya", "Crum, Traci", "Grummert, Traci", "Schaubert, Traci", "Kaiser, Tracy", "Peitz, Tracy", "Scalf, Tracy", "Timm, Travis", "Bradley, Travis", "Culpepper, Travis", "Hanson, Travis", "Hedlund, Travis", "Kozak, Travis", "Meyer, Travis", "Timm, Travis", "Hedlund, Travis", "Zimmerman, Trent", "Bruneau, Trevor", "Corry, Trevor", "Stec, Trevor", "Meisinger, Trevor", "Morris, Trevor", "Parks, Trevor", "Stec, Trevor", "Corby, Trina", "Garrison, Troy", "Panning, Troy", "Bystrom, Trudi", "Barrett, Ty", "Moreno, Ty", "Fellows, Tyler", "Hasenauer, Tyler", "Hiipakka, Tyler", "Martin, Tyler", "Robeson, Tyler", "Sanders, Tyler", "Shaw, Tyler", "Sudbeck, Tyler", "Wakefield, Tyler", "Tate, Tyshawn", "Gilroy, Tyson", "Peterson, Val", "Manske, Valerie", "Manske, Valerie", "Brown, Ver-Anita", "McBride, Vernon", "Liu, Vicki", "Liu, Vicki", "Garcia, Victor", "Farwell, Victoria", "Stuckey, Victoria", "Ravipati, Vijay", "Berigan, Vince", "Harwan, Vincent", "Garren, Wade", "Veland, Wade", "Iskandr, Wahid", "Tembo, Walter", "Ritchie, Wayne", "Park, Wendee", "Collins, Wendy", "DeBoer, Wendy", "Johansen, Wendy", "Lewis, Wendy", "Pope, Wendy", "Vienneau, Wendy", "Bryan, Whitney", "Prokupek, Whitney", "Briley, William", "Diederich, William", "Kazlow, William", "Marquardt, William", "Norskov, William", "Peters, William", "Steinbach, William", "Jr, William", "Huggins, Wilmarie", "Beck, Winston", "Processing, Word", "Kirwan, Wyatt", "Martinez, Yaelli", "Egr, Zac", "Malsam, Zac", "Kelley, Zach", "Kroenke, Zach", "Ring, Zach", "VanRoy, Zach", "Fugitt, Zachary", "Gomez, Zachary", "Hartman, Zachary", "Hernbloom, Zachary", "Lanham, Zachary", "McGargill, Zachary", "Metz, Zachary", "Osborne, Zachary", "Reid, Zachary", "Sorensen, Zachary", "Kick, Zack", "Young, Zelany", "Goldberg, Zina"]

def index():
    # Alot of this should be done via front end stuff instead of like this.
    # Its probably worth making a true GUI controller for superusers to manage the system (outside of database manipulation)
    # Absolutely should be able to load a vendor list instead of this
    """
    This is the current home for any population/reseting scripts

    :usage: See individual methods for correct usage/params

    :requires: user must be in superuser group

    :params: See individual methods for correct usage/params

    :returns: no
    """
    if auth.has_membership('superuser'):
        parseInput()
    else:
        handleInvalidPermission()

def handleInvalidPermission():
    """
    This is the method called whenever a user calls our population scripts and doesn't have appropriate permissions.
    We display a generic 404 to the user, and then log whatever the user was attempting to do

    :usage handleInvalidPermission():

    :requires None:

    :params None:

    :returns None: Technically we are raising an HTTP exception as that is how web2py displays webpages to the user. This HTTP
    exception simply displays a default 404 saying whatever the user attempted to access does not exist. 
    """
    # TODO we should probably both log out and throw an email err to programming if someone attempted to blow away the database on production
    printError = ""
    command = request.vars.command if request.vars.command is not None else "make changes"
    table = f"{request.vars.table} table" if request.vars.table is not None else ""
    user = f"{auth.user.first_name} {auth.user.last_name}" if auth.user is not None else "Invalid User"
    # if not configuration.get('app.debug'):
        # printError = f"{user} from {request.client} attempted to {command} {table} database ON PRODUCTION."
    # else:
    printError = f"{user} from {request.client} attempted to {command} {table} database."
    print(printError)
    raise HTTP(
        404,
        rewrite.THREAD_LOCAL.routes.error_message % 'invalid controller (nukeOrPopulateDB/index)', web2py_error='invalid function (nukeOrPopulateDB/index)'
    )

def parseInput():
    """
    This will attempt to figure out what the user wants to do. Also logs out what actions is being performed,
    what IP is performing it, and who is performing it

    :usage: https://yourURL/po/nukeOrPopulateDB?command=some_command&table=some_table

    :params command: Valid commands are either reset or populate.
    - Reset
        The reset command will wipe away all information in a particular table. This does not drop the table,
        instead deleting every index in the table that is greater than 0
    - Populate
        This will simply perform a "fill" on whatever table was provided. Each table has a specific series of commands
        that are run to "populate" a table. If reset is called, it will call populate afterwards. If populate is ran
        on a table with existing data in it, populate will simply add the population data to the existing data.

    :params table: A database table that you wish to perform changes on. Valid tables are
    - department
    - vendor
    - po_status_reference
    - po_form
    - po_note
    - po_item
    - po_rule
    - role_heirarchy
    - po_rule_map

    :requires None: 

    :returns None:
    """
    stringCommand = request.vars.command if request.vars.command is not None else "make changes"
    stringTable = f"{request.vars.table} table" if request.vars.table is not None else ""
    user = f"{auth.user.first_name} {auth.user.last_name}" if auth.user is not None else "Invalid User"
    print(f"{user} from {request.client} is executing command:{stringCommand} on table:{stringTable}")
    
    command = request.vars.command
    table = request.vars.table

    if command.lower() == "reset":
        resetCommand(table)
    elif command.lower() == "populate":
        populateCommand(table)
    
    else:
        print(f"{command} is not a valid command. Nothing will happen. Sorry about your luck bub")

def resetCommand(table):
    """
    Performs a reset on the provided table. After the reset is performed, we repopulate it. 

    :usage resetCommand(table):

    :params table: A database table that you wish to reset. Valid tables are
    - department
    - vendor
    - po_status_reference
    - po_form
    - po_note
    - po_item
    - po_rule
    - role_heirarchy
    - po_rule_map

    :requires None:

    :returns None:
    """
    table = str(table).lower()
    if (table[0] == "_") or \
       (table[0].startswith("auth")) or \
       (table[0].startswith("scheduler")):
        handleInvalidPermission()
        return
    try:
        db[table]
    except:
        print(f"Invalid Table {table}")
        handleInvalidPermission()

    print(f"Dropping Table {table}!")
    db(db[table].id >= 0).delete()
    populateCommand(table)

def populateCommand(table):
    """
    Populates the provided table.

    :usage populateCommand(table):

    :params table: A database table that you wish to populate. Valid tables are
    - department
    - vendor
    - po_status_reference
    - po_form
    - po_note
    - po_item
    - po_rule
    - role_heirarchy
    - po_rule_map

    :requires None:

    :returns None:
    """
    print(f"Attempting To Repopulate {table}")
    if table == "department":
        populateDepartment()
    elif table == 'vendor':
        populateVendor()
    elif table == 'po_status_reference':
        populateStatusReference()
    elif table == 'po_form':
        populatePOForm()
    elif table == 'po_note':
        populatePONote()
    elif table == 'po_item':
        populatePOItem()
    elif table == 'po_rule':
        populatePORules()
    elif table == 'role_heirarchy':
        populateRoleHeirarchy()
    elif table == 'po_rule_map':
        populatePORuleMap()
    else:
        print(f"Table {table} doesn't exist. I have no idea how in the world you got here. Here's a star ⭐")
        return
    print("We have finished populating everything")

def populateDepartment():
    """
    Manually addes all departments to the department table in the database. The source of truth for the department list is found
    at the top of this file. Eventually, we will want a better way to find all departments, but this will for for now. Additionally,
    this will add permissions on each department (submitter -> superuser)

    :usage populateDepartment():

    :requires None:

    :params None:

    :returns None:
    """
    if db(db.auth_group.role == "superapprover").isempty():
        groupId = auth.add_group("superapprover", "Approve All The Things!")
        auth.add_permission(groupId, "superapprover", 'any')

    if db(db.auth_group.role == 'accounting').isempty():
        groupId = auth.add_group("accounting")
        auth.add_permission(groupId, "accounting", 'any')

    if db(db.auth_group.role == "superuser").isempty():
        groupId = auth.add_group("superuser")
        auth.add_permission(groupId, "superuser", 'any')

    for department in DEPARTMENTS:
        db.department.update_or_insert(name=department)
        groupId = auth.add_group(department + " Submitter", "Submitter Group For " + department)
        auth.add_permission(groupId, "submitter", department)
        auth.add_permission(groupId, "submitter", "any")
        groupId = auth.add_group(department + " Supervisor", "Supervisor Group For " + department)
        auth.add_permission(groupId, "supervisor", department)
        auth.add_permission(groupId, "supervisor", 'any')
        copyPermissions(department + " Submitter", department + " Supervisor", "supervisor", department)
        groupId = auth.add_group(department + " Department Head", "Department Head Group For " + department)
        auth.add_permission(groupId, "department_head", department)
        auth.add_permission(groupId, "department_head", 'any')
        copyPermissions(department + ' Supervisor', department + ' Department Head', 'department_head', department)
        copyPermissions(department + ' Department Head', 'superapprover', 'superapprover', department)
        copyPermissions('superapprover', 'superuser')
        copyPermissions('accounting', 'superuser')


def populateVendor():
    """
    Manually addes all the vendors to the vendor table in the database. The source of truth for the vendors is found in /private/vendorlist.csv. Addtionally, there is an array of users at the top that are to be removed from the vendor list.

    :usage populateVendor():

    :requires None:

    :params None:

    :returns None:
    """
    import csv
    import os
    from os import path
    # We expect to find a vendor list in here. Eventually we should make this query some
    # mythical database, but for how this works
    inputFile = os.path.normpath(os.getcwd() + "/applications/po/private/vendorlist.csv")
    class Row:
        def __init__(self, rowHeaders, rowDetails):
            for key, value in zip(rowHeaders, rowDetails):
                super().__setattr__(key.lower().replace(" ","_"), value)

        def __getattr__(self, attr):
            try:
                return super().__getattr__(attr)
            except:
                return None
            
        def __str__(self):
            return str(self.__dict__.keys())

    class Vendor:
        def __init__(self, name, phone_num="", account_num=None, contact_person=""):
            self.name = name
            self.phone_num = phone_num
            self.account_num = account_num
            self.contact_person = contact_person

    if not path.exists(inputFile):
        print(f"Theres no file at {inputFile}. Failed vendor population")
        return

    with open(inputFile, 'r') as vendorlist:
        reader = csv.reader(vendorlist)
        
        count = 0
        rowHeader = []
        for row in reader:
            if count == 0:
                rowHeader = row
            else:
                nRow = Row(rowHeaders=rowHeader, rowDetails=row)
                vendor = Vendor(nRow.vendor, phone_num=nRow.main_phone)
                if nRow.first_name is not None and nRow.last_name is not None:
                    vendor.contact_person = nRow.first_name + " "+ nRow.last_name
                if vendor.contact_person is None and nRow.primary_contact is not None:
                    vendor.contact_person = nRow.primary_contact
                if vendor.contact_person not in employees:
                    db.vendor.update_or_insert(name=vendor.name, phone_num=vendor.phone_num, account_num=vendor.account_num, contact_person=vendor.contact_person)
            count += 1

def populateStatusReference():
    """
    Adds all the status to the status reference table

    :usage populateStatusReference():

    :requires None:

    :params None:

    :returns None:
    """
    db.po_status_reference.update_or_insert(status="Created")
    db.po_status_reference.update_or_insert(status="Rejected")
    db.po_status_reference.update_or_insert(status="Submitted")
    db.po_status_reference.update_or_insert(status="Approved")
    db.po_status_reference.update_or_insert(status="Ordered")
    db.po_status_reference.update_or_insert(status="Received")
    db.po_status_reference.update_or_insert(status="Closed")

def populatePOForm():
    # We should not populate anything here
    pass

def populatePONote():
    # We should not populate anything here
    pass

def populatePOItem():
    # We should not populate anything here
    pass

def populatePORuleMap():
    """
    This method loops over every PO that is in a submitted state and maps the po to the appropriate rule. These rule mappings
    are stored in a table called po_rule_map.

    :usage populatePORuleMap():

    :requires None:

    :params None:

    :returns None:
    """
    for poNumber in db((db.po_form.status == db.po_status_reference.id) & (db.po_status_reference.status == "Submitted")).select(db.po_form.id):
        rule_query = (
            (db.po_form.id == poNumber) & 
            (db.po_form.department == db.po_rule.department) &
            (db.po_rule.min_subtotal <= db.po_form.subtotal) & 
            ((db.po_rule.max_subtotal >= db.po_form.subtotal) | (db.po_rule.max_subtotal == 0))
        )
        
        for rule in db(rule_query).select(
            db.po_rule.id, 
            db.po_rule.min_subtotal, 
            db.po_rule.supervisor_approvals, 
            db.po_rule.department_head_approvals, 
            db.po_rule.superapprover_approvals):
            
            if rule.min_subtotal == 0:
                # Auto approve po
                redirect(URL(c="manageUserPO", f="processApprovePO", vars=dict(po_number=poNumber, approver="Auto Approver")))
            role_heirarchy_id = 0
            
            if rule.supervisor_approvals > 0:
                role_heirarchy_id = db(db.role_heirarchy.name == "supervisor").select(db.role_heirarchy.id).first().id
            if rule.department_head_approvals > 0:
                role_heirarchy_id = db(db.role_heirarchy.name == "department_head").select(db.role_heirarchy.id).first().id
            if rule.superapprover_approvals > 0:
                role_heirarchy_id = db(db.role_heirarchy.name == "superapprover").select(db.role_heirarchy.id).first().id
            
            if role_heirarchy_id > 0:
                    db.po_rule_map.update_or_insert(po_form=poNumber, po_rule=rule.id)

def populatePORules():
    """
    Helper method that populates all the basic rules templates per department. Note, these will likely not be accurate
    for every department. You will have to make manual changes in the database on a per department basis to match
    what each department needs, if their ruleset is not the generic rule set.

    :usage populatePORules():

    :requires None:

    :params None:

    :returns None:
    """
    for department in DEPARTMENTS:
        departmentId = db(db.department.name == department).select("id").first().id
        db.po_rule.update_or_insert(department=departmentId, min_subtotal=0, max_subtotal=100, supervisor_approvals=0, department_head_approvals=0, superapprover_approvals=0, invoice=1, bids_recommended=0, contract_recommended=0, contract_required=0)
        db.po_rule.update_or_insert(department=departmentId, min_subtotal=100, max_subtotal=5000, supervisor_approvals=1, department_head_approvals=0, superapprover_approvals=0, invoice=1, bids_recommended=0, contract_recommended=0, contract_required=0)
        db.po_rule.update_or_insert(department=departmentId, min_subtotal=5000, max_subtotal=50000, supervisor_approvals=1, department_head_approvals=1, superapprover_approvals=0, invoice=1, bids_recommended=3, contract_recommended=1, contract_required=0)
        db.po_rule.update_or_insert(department=departmentId, min_subtotal=50000, max_subtotal=0, supervisor_approvals=1, department_head_approvals=1, superapprover_approvals=1, invoice=1, bids_recommended=3, contract_recommended=0, contract_required=1)

def populateRoleHeirarchy():
    """
    Helper method that populates each role for the system.

    :usage populateRoleHeirarchy():

    :requires None:

    :params None:

    :returns None:
    """
    db.role_heirarchy.update_or_insert(name="submitter", level=0)
    db.role_heirarchy.update_or_insert(name="supervisor", level=1)
    db.role_heirarchy.update_or_insert(name="department_head", level=2)
    db.role_heirarchy.update_or_insert(name='accounting', level=3)
    db.role_heirarchy.update_or_insert(name="superapprover", level=4)
    # THEIR POWER LEVEL IS OVER 9000!
    db.role_heirarchy.update_or_insert(name="superuser",level=9001)
