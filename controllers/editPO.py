# -*- coding: utf-8 -*-
import custom_formstyles
import json
import generate_attachments_view
from generate_attachments_view import get_editable_views as get_attachment_views
from generate_attachments_view import get_attachments, sort_po_attachments, get_po_attachments_query
from generate_attachments_view import UPLOAD_TYPES as ATTACHMENT_KEYS
from injectable_sqlform import InjectableSQLFORM
from datetime import datetime

PO_FORM_INJECTION_KEYS = ATTACHMENT_KEYS
# Also need to add po_items to injection keys

### required - do not delete
def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires

try:
    BASE_METHOD_PERMISSION_ACCESS = not db(db.auth_membership.user_id == auth.user.id).isempty()
except AttributeError:
    BASE_METHOD_PERMISSION_ACCESS = False

@auth.requires(BASE_METHOD_PERMISSION_ACCESS, requires_login=True)
def index():
    """
    This is the "landing" page for our editPO controller.

    :usage: https://yourURL/po/editPO/index?po_number=somepo_number

    :requires: submitter permission in any department and user must be logged in

    :params po_number: A po number must be provided for us to pull the po information to edit.

    :returns dict(po_number, po_form, po_items): This function returns a dictionary with 3 keys
    - po_number: the provided po_number is put into the dictionary for the view
    - po_form: an editable SQLFORM version of the PO data
    - po_items: an editable SQLFORM version of the items on the PO
    """
    poForm = ""
    poItems = ""

    editable, failureMessage = verifyEditability()
    if not editable:
        session.flash = failureMessage
        redirect(URL(c="viewPOForms", f="index"))

    po_number = request.vars.po_number
    setFieldsWritableStatus(po_number)
    
    forms = createPOForm(po_number)
    poForm = forms['poForm']
    poItems = forms['poItem']
    editable_attachment_fields = get_attachments_writable_status(po_number)
    poAttachments = get_attachments(db, po_number, get_file, editable_attachment_fields)

    if poForm.process().accepted:
        previous_last_modified = datetime.strptime(request.vars.last_modified, '%Y-%m-%d %H:%M:%S')
        last_modified = db(db.po_form.id == po_number).select(db.po_form.last_modified).first().last_modified
        changed = poForm.changed(request)
        if previous_last_modified != last_modified or changed:
            resetApprovalsAndStatus(po_number)
        _handle_injected_info(poForm.get_injected_info(request))
        # Probably worth having the injected sqlform figure out if it was modified
        # in a way we care about
        if not is_po_in_order_state(po_number) and changed:
            po_items_modified(po_number)
        redirect(URL(c="manageUserPO", f="index", vars=dict(po_number=po_number)))

    return dict(po_number=po_number, po_form=poForm, po_items=poItems, po_attachments=poAttachments, editable_attachment_fields=editable_attachment_fields)

@auth.requires(BASE_METHOD_PERMISSION_ACCESS, requires_login=True)
def createPOForm(po_number):
    """
    Converts the PO into an SQLFORM that can be rendered by a view

    :usage createPOForm(po_number):

    :requires: user is a submitter in any department, and the user is logged in

    :params po_number: po_number must be tied to an existing PO.

    :returns dict(poForm, poItems): returns a dictionary with the poForm and poItems as separate SQLFORMS
    """
    monitor_fields = None
    current_status = db((db.po_form.id == po_number) & (db.po_status_reference.id == db.po_form.status)).select(db.po_status_reference.status).first().status
    if current_status == 'Ordered':
        monitor_fields = []
    po_form = InjectableSQLFORM(
        db.po_form, 
        record=po_number, 
        formstyle=custom_formstyles.custom_bootstrap4_inline(submit_button_text="Save PO Changes"),
        _id="po_form",
        monitor_fields = monitor_fields
    )

    attachments_injection_start_point = 'sole_source_purchase'
    items_injection_point = 'contact_person'
    injection_points = dict()
    attachment_views = get_attachment_views(po_number, editable_fields = get_attachments_writable_status(po_number))
    for key, attachment_view in attachment_views.items():
        if len(injection_points) == 0:
            injection_points[attachments_injection_start_point] = dict(
                location="after",
                label=attachment_view,
                widget='',
                key=key
            )
        else:
            injection_points[previous_key] = dict(
                location="after",
                label=attachment_view,
                widget='',
                key=key
            )
        previous_key = key

    poItemForm = SPAN(
        LOAD(f='po_item.load', vars=dict(po_number=po_number), ajax=False),
        # This links to :func: po_item
        _class='po_item'
    )

    # This doesn't quite work and will break the current injection data handling (note, not the form injection system, but how we in editPO handle the injected data later)
    # injection_points[items_injection_point] = dict(
    #     location="after",
    #     label='Items',
    #     widget=poItemForm,
    #     key="po_items"
    # )

    po_form.inject_items(injection_points)
    return dict(poForm=po_form, poItem=poItemForm)

@auth.requires(BASE_METHOD_PERMISSION_ACCESS, requires_login=True)
def po_item():
    """
    Converts PO Items list into SQLFORM that can be rendered by a view

    :usage po_item():

    :requires: user is a submitter in any department, and the user is logged in

    :params po_number: po_number must be tied to an existing PO.

    :returns dict(subtotal, poEntry): returns a dictionary with the po subtotal and the po items. The po items are an SQLFORM
    """
    po_number = request.vars.po_number
    po = db(db.po_form.id == po_number).select().first()
    approvedStatus = db(db.po_status_reference.status == "Approved").select(db.po_status_reference.id).first().id
    # editable = (po.submitter == auth.user.id)
    # Remove the below line in favor of the above line, if we opt for the "better" solution for the add button being visible when the PO is in an ordered state
    editable = (po.submitter == auth.user.id) and (po.status <= approvedStatus)
    poDisplayFields = []
    db.po_item.po_form.default = po_number
    db.po_item.extended_price.readable = False
    db.po_item.extended_price.writable = False

    for field in db.po_item:
        if field is db.po_item.id:
            continue
        poDisplayFields.append(field)
    response.replace_add_record_text = 'Add PO Item'

    poEntry = SQLFORM.grid(
        db(db.po_item.po_form == po_number),
        formname='po_item',
        fields=poDisplayFields,
        searchable=False,
        # This is the current in place fix for the PO system. It simply hides the add button when the PO is deemed not in an editable state
        create=editable,
        editable=editable,
        deletable=editable,
        oncreate=poItemsUpdatedOrCreated,
        onupdate=poItemsUpdatedOrCreated,
        ondelete=poItemsDeleted,
        formstyle=custom_formstyles.custom_bootstrap4_inline(submit_button_text="Add Item"),
        csv=False,
    )

    # We are not reloaded on item deletion, only on create/update. This is by design of web2py, not what we want. :(
    return dict(
        subtotal=(db.po_form(po_number).subtotal or 0.0),
        poEntry=poEntry)

# ------------------------Renders UI Components /\-----------------------

# ------------------------Server Side Methods \/------------------------

def _handle_injected_info(injected_info):
    _handle_uploaded_attachments(injected_info)
    # Also might need to handle the po_items?

def _handle_uploaded_attachments(injected_info):
    po_number = request.vars.po_number
    current_attachments = {}
    for attachment in db(db.po_form_attachments.po_form == po_number).select():
        try:
            current_attachments[attachment.upload_type]
        except:
            current_attachments[attachment.upload_type] = {}

        filename = db.po_form_attachments.file.retrieve(attachment.file)[0]
        id = attachment.id
        current_attachments[attachment.upload_type][filename] = id
    update_attachments = []
    keep_attachments = []
    
    for attachment in injected_info.values():
        if attachment == None:
            continue
        try:
            for a in attachment:
                if _is_file_empty(a.file):
                    keep_attachments.append(current_attachments[a.name][a.filename])
                else:
                    update_attachments.append(a)
        except TypeError:
            if _is_file_empty(attachment.file):
                keep_attachments.append(current_attachments[attachment.name][attachment.filename])
            else:
                update_attachments.append(attachment)
    db((db.po_form_attachments.po_form == po_number) & (~db.po_form_attachments.id.belongs(keep_attachments))).delete()
    for attachment in update_attachments:
        db.po_form_attachments.insert(
            po_form=po_number,
            upload_date=request.now,
            upload_type=attachment.name,
            file=attachment
        )
        db.po_note.insert(
            po_form=po_number,
            made_by=auth.user,
            made_time=request.now,
            comment=f"Added {attachment.name.replace('_', ' ').title()}: {attachment.filename}",
            po_approval=False
        )

def _is_file_empty(file):
    file.seek(0)
    try:
        first_char = file.read(1)
        if first_char:
            file.seek(0)
            return False
        else:
            return True
    except:
        return True

@auth.requires(auth.has_permission('submitter', 'any'), requires_login=True)
def poItemsUpdatedOrCreated(form):
    """
    A callback method that is called when an item on a PO is added or updated

    :usage poItemsUpdatedOrCreated(form):

    :requires: user is a submitter in any department, and the user is logged in

    :params form: form is an automatically attached param by SQLFORM.grid when calling oncreate or onupdate. The 'form' object is the literal form 
    on the page in its current state (after item addition/update)

    :returns None: This is a callback method, designed by the framework to allow us to interact server side when
    an item is updated/created. In order to "circumvent" the lack of return results, we append javascript to the response, which
    is executed when the response is rendered
    """
    po_items_modified(request.vars.po_number)

@auth.requires(auth.has_permission('submitter', 'any'), requires_login=True)
def poItemsDeleted(table, id):
    """
    A callback method that is called when an item on a PO is deleted

    :usage poItemsDelete(table, item_id):

    :requires: user is a submitter in any department, and the user is logged in

    :params table: table is an automatically attached param by SQLFORM.grid when calling ondelete. The 'table' is the SQL table that we are
    removing an item from
    :params id: id is the individual id in the table that is being removed

    :returns None: This is a callback method, designed by the framework to allow us to interact server side when
    an item is removed. In order to "circumvent" the lack of return results, we append javascript to the response, which
    is executed when the response is rendered
    """
    if id is not None:
        db.po_item(id).delete_record()
    po_items_modified(request.vars.po_number, id)
    subtotal = db.po_form(request.vars.po_number).subtotal
    # Its worth having this hide our po bar if the number of items we have is 0 now
    response.js = f"updateSubTotal('{subtotal}');"

def po_items_modified(po_number, po_item=None):
    """
    A method called to when any item on a PO has been modified. This method will clear all approvals on the parent PO.
    Additionally, it recalculates the subtotal and tax for the PO.

    :usage po_items_modified(po_number):

    :requires None:

    :params po_number: po_number must be a valid number and associated with a valid PO. 

    :returns None:
    """
    totals = calculate_new_totals(po_number)
    po_form = db.po_form(po_number)
    po_form.update_record(last_modified=request.now, freight=totals['freight'], subtotal=totals['subtotal'], tax=totals['tax'], total=totals['total'])
    # resetApprovalsAndStatus(po_number)

@auth.requires(auth.has_permission('submitter', 'any'), requires_login=True)
def resetApprovalsAndStatus(po_number):
    """
    A method called to wipe all approvals for this PO off the face of the planet. Additionally we reset the PO back to a "created" state.

    :usage resetApprovalAndStatus(po_number):

    :requires: user is a submitter in any department and user is logged in

    :params po_number: po_number must be a valid number and associated with a valid PO.

    :returns None:
    """
    poForm = db.po_form(po_number)
    createdStatus = db(db.po_status_reference.status == "Created").select(db.po_status_reference.id).first().id
    if poForm.status != createdStatus:
        db.po_note.insert(po_form=po_number, po_approval=False, comment='Modified After Submission', made_by=auth.user.id)
    db((db.po_note.po_form == po_number) & (db.po_note.po_approval == True)).update(po_approval=False)
    poForm.update_record(status=createdStatus)
    poForm.po_approval_date = None
    # TODO We need to clear the po_rule_map table of the po as well

def vendorLookup():
    """
    A method that is called via ajax in order to fetch all possible information about a vendor

    :usage: https://yourURL/po/editPO/vendorLookup?vendor=some_vendor

    :requires None:

    :params vendor: vendor should be a string of the name of the vendor you are looking up

    :returns dict(vendor_name, phone_num, account_num, contact_person): returns a dictionary with the vendors information associated with 
    the appropriate keys

    """
    vendor = request.vars.vendor
    if not request.vars.vendor:
        return ''
    vInfo = db(db.vendor.name == vendor).select()
    if len(vInfo) == 0:
        vendorInfo = dict(vendor_name=vendor, phone_num='', account_num='', contact_person='')
    else:
        vInfo = vInfo.first()
        vendorInfo = dict(vendor_name=vendor, phone_num=vInfo.phone_num, account_num=vInfo.account_num, contact_person=vInfo.contact_person)
    return json.dumps(vendorInfo)

def calculate_new_totals(po_number):
    """
    Iterates through a PO and calculates all numbers that need calculating.
    
    :usage calculate_new_totals(po_number):

    :requires None:

    :params po_number: po_number must be a valid number and associated with a valid PO.

    :returns dict(freight, subtotal, tax, total):
    """
    poForm = db.po_form(po_number)
    freight = poForm.freight
    subtotal = 0
    for r in db(db.po_item.po_form == poForm.id).select():
        subtotal += r.extended_price
    tax = subtotal * (poForm.tax_percent/100)
    return dict(
        freight=freight,
        subtotal = subtotal,
        tax = tax,
        total = subtotal + tax + freight
    )

@auth.requires_login()
def verifyEditability():
    """
    Verifies that we received a po_number and we can indeed edit it

    :usage verifyEditability():

    :requires: user must be logged in

    :params None:

    :returns Boolean, error_string: Returns a boolean True/False. If true, there is no error string. If false, returns an error string to be
    displayed to the user indicating why the PO is not editable
    """
    if not request.vars.po_number:
        return False, "No PO Number Provided"
    elif db(db.po_form.id == request.vars.po_number).isempty():
        return False, f"Invalid PO #{request.vars.po_number}"

    po = db(db.po_form.id == request.vars.po_number).select().first()
    ordered_status_id = db(db.po_status_reference.status == "Ordered").select(db.po_status_reference.id).first().id
    closed_status_id = db(db.po_status_reference.status == "Closed").select(db.po_status_reference.id).first().id
    current_state = db(db.po_status_reference.id == po.status).select(db.po_status_reference.status).first().status
    department = db(db.department.id == po.department).select(db.department.name).first()

    status = f"PO #{request.vars.po_number} is not editable"

    if current_state.lower() in ['closed', 'received']:
        return False, status

    try:
        department = department.name.replace(' ','_').lower()
    except AttributeError:
        department = ""
    editable = False
    ordered_state_and_user_in_department = (po.status == ordered_status_id) and ((auth.has_permission('submitter', department)))
    user_is_accounting = auth.has_permission('accounting', 'any')
    user_owns_po = ((po.status < ordered_status_id) and (po.submitter == auth.user.id))

    user_has_relevant_granular_permission = False
    for key in ATTACHMENT_KEYS:
        if user_has_relevant_granular_permission:
            break
        _k = key.replace(' ', '_').lower()
        _s = current_state.lower()
        base_permission_check = auth.has_permission(f'{_k}_{_s}', 'any')
        base_permission_check_with_department = auth.has_permission(f'{_k}_{_s}', department)
        alt_permission_check = auth.has_permission(f'upload_{_k}_{_s}', 'any')
        alt_permission_check_with_department = auth.has_permission(f'upload_{_k}_{_s}', department)
        if base_permission_check or base_permission_check_with_department or alt_permission_check or alt_permission_check_with_department:
            user_has_relevant_granular_permission = True

    permission_checks = [
        ordered_state_and_user_in_department,
        user_is_accounting,
        user_owns_po,
        user_has_relevant_granular_permission
    ]
    failed_check = True
    for permission_check in permission_checks:
        if permission_check:
            failed_check = False
            break
    
    if not failed_check:
        editable = True
        status = ''
    else:
        editable = False
        status = "Unknown Permission Error Occurred. Please Contact System Administrator"
    return editable,status


def setFieldsWritableStatus(po_number):
    """
    TODO(Mike) Rewrite this.
    Sets relevant fields to a writable status if the PO is in an ordered state

    :usage setFieldsWritableStatus(po_number):

    :requires None:

    :params po_number: po_number must be tied to an existing PO.

    :returns Boolean: If the PO has fields that need to be modified (IE, the PO is in an ordered state), we return False. This is
    to inform the parent that the PO is in an ordered state. The assumption is, if the PO is in an ordered state and we are editing it,
    we should not wipe the approvals. Kinda hacky, but it works
    """
    po_submitter = db(db.po_form.id == po_number).select(db.po_form.submitter).first().submitter
    # if not is_po_in_order_state(po_number) or po_submitter != auth.user_id:
    po_is_ordered = is_po_in_order_state(po_number)
    if po_submitter == auth.user_id and not po_is_ordered:
        return 

    for key in db.po_form:
        key.readable=True
        key.writable=False
    if po_is_ordered:
        db.po_form.order_confirmation_num.writable=True
        db.po_form.tracking_number.writable=True
        db.po_form.shipping_company.writable=True
    else:
        db.po_form.order_confirmation_num.readable=False
        db.po_form.tracking_number.readable=False
        db.po_form.shipping_company.readable=False

def is_po_in_order_state(po_number):
    return not \
        db(
            (db.po_form.id == po_number)
          & (db.po_form.status == db.po_status_reference.id)
          & (db.po_status_reference.status == "Ordered")
        ).isempty()

def get_attachments_writable_status(po_number):
    current_state = db((db.po_form.id == po_number) & (db.po_status_reference.id == db.po_form.status)).select(db.po_status_reference.status).first().status.replace(' ', '_').lower()
    department = db((db.po_form.id == po_number) & (db.department.id == db.po_form.department)).select(db.department.name).first()
    ordered_state = db(db.po_status_reference.status == 'Ordered').select().first().id
    po = db(db.po_form.id == po_number).select().first()
    if po.status < ordered_state and po.submitter == auth.user_id:
        return [key for key in ATTACHMENT_KEYS]

    if department is None:
        department = ''
    else:
        department = department.name.replace(' ', '_').lower()

    if current_state == 'ordered':
        writable_attachments = []
    else:
        writable_attachments = None

    for key in ATTACHMENT_KEYS:
        _k = key.replace(' ', '_').lower()
        base_permission_check = auth.has_permission(f'{_k}_{current_state}', 'any')
        base_permission_check_with_department = auth.has_permission(f'{_k}_{current_state}', department)
        alt_permission_check = auth.has_permission(f'upload_{_k}_{current_state}', 'any')
        alt_permission_check_with_department = auth.has_permission(f'upload_{_k}_{current_state}', department)
        if(base_permission_check or base_permission_check_with_department or alt_permission_check or alt_permission_check_with_department
        ):
            if writable_attachments == None:
                writable_attachments = []
            writable_attachments.append(key)
    return writable_attachments
